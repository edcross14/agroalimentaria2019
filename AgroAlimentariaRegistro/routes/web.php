<?php
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/******************************* 
para la aplicacion de los participantes
********************************/
Route::get('/registro','ParticipantesController@registro')->name('participante.registro');
Route::get('/registroStaff','ParticipantesController@registroStaff')->name('participante.registroStaff');
Route::get('/registroExpositor','ParticipantesController@registroExpositor')->name('participante.registroExpositor');
Route::post('/store','ParticipantesController@store')->name('participante.store');
/******************************* 
para los combos de ocupaciones
********************************/
Route::get('/show/{id}','ParticipantesController@show')->name('participante.show');
Route::get('/show2/{id}','ParticipantesController@show2')->name('participante.show2');
/******************************* 
para las impresiones
********************************/
Route::get('/imprimir/{id}/{ocupa}/{nombre}/{AP}/{AM}/{empresa}', 'GeneradorController@imprimir')->name('imprimir');
Route::get('/consultarmail','ParticipantesController@consultarmail')->name('participante.consultarmail');
/******************************* 
para el sistema de registrar
********************************/
Route::get('/index/consulta','ParticipantesController@index')->name('participante.index');
Route::get('/index/reportes','ParticipantesController@reportes')->name('participante.reportes');
Route::get('/index/consultaReimp','ParticipantesController@consulta')->name('participante.consulta');

Route::get('/registroAdmin','ParticipantesController@registroAdmin')->name('participante.registroAdmin');
Route::get('/registroStaffAdmin','ParticipantesController@registroStaffAdmin')->name('participante.registroStaffAdmin');