<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\ParticipantesRequest;

use DB;
class ParticipantesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $participantes = $users = DB::table('participantes')
                                ->join('subocupaciones', 'participantes.idOcupacion', '=', 'subocupaciones.idSubocupacion')
                                ->join('estado', 'participantes.idEstado', '=', 'estado.id')
                                ->join('pais', 'estado.ubicacionpaisid', '=', 'pais.id')                               
                                ->select('nombre','apellido_pat', 'apellido_mat', 'subocupaciones.subOcupacion' , 'estado.estadonombre', 'pais.paisnombre', 'email', 'participantes.id', 'subocupaciones.idOcupaciones', 'nombreEmpresa')
                                ->paginate(20);
        return view('consulta',compact('participantes'));
    }

    public function consultarmail(Request $request)
    {         
        $email=$request->mail;   
        $participantes = $users = DB::table('participantes')
                                ->join('subocupaciones', 'participantes.idOcupacion', '=', 'subocupaciones.idSubocupacion')
                                ->join('estado', 'participantes.idEstado', '=', 'estado.id')
                                ->join('pais', 'estado.ubicacionpaisid', '=', 'pais.id')   
                                ->Where('email', $email)
                                ->select('nombre','apellido_pat', 'apellido_mat', 'subocupaciones.subOcupacion' , 'estado.estadonombre', 'pais.paisnombre', 'email', 'participantes.id', 'subocupaciones.idOcupaciones', 'nombreEmpresa')
                                ->paginate(20);
        return view('consultamail',compact('participantes'));        
    }

    public function reportes()
    {
        
        $participantes = $users = DB::table('ocupaciones')                                
                    ->select(DB::raw('ocupacion,  
                      IFNULL((SELECT SUM(1) FROM participantes INNER JOIN subocupaciones ON 
                      participantes.idOcupacion=subocupaciones.idSubocupacion 
                      WHERE subocupaciones.idOcupaciones=ocupaciones.idOcupaciones),0) as total'))
                    ->orderBy('ocupacion')
                    ->paginate(11);
        return view('reportes',compact('participantes'));
    }

    public function consulta(Request $request)
    {
        //$nombre=$request->nombre;
        $mail=$request->mail;                
        
        $participantes = $users = DB::table('participantes')
                            ->join('subocupaciones', 'participantes.idOcupacion', '=', 'subocupaciones.idSubocupacion')
                            ->join('estado', 'participantes.idEstado', '=', 'estado.id')
                            ->join('pais', 'estado.ubicacionpaisid', '=', 'pais.id')                                
                            ->where('email', 'like', '%' . $mail . '%')
                            ->select('nombre','apellido_pat', 'apellido_mat', 'subocupaciones.subOcupacion' , 'estado.estadonombre', 'pais.paisnombre', 'email', 'participantes.id', 'subocupaciones.idOcupaciones', 'nombreEmpresa')
                            ->paginate(20);
                      
        return view('consulta',compact('participantes'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function registro()
        {        
        $paises = DB::table('pais')->orderBy('paisnombre')->get();        
        $ocupaciones = DB::table('ocupaciones')->where('staff', '0')->orderBy('ocupacion')->get();        
        return view('principal',compact('paises', 'ocupaciones'));        
        }
    
    public function registroAdmin()
        {        
        $paises = DB::table('pais')->orderBy('paisnombre')->get();        
        $ocupaciones = DB::table('ocupaciones')->where('staff', '0')->orderBy('ocupacion')->get();        
        return view('principal2',compact('paises', 'ocupaciones'));        
        }

    public function registroStaff()
        {
        $paises = DB::table('pais')->orderBy('paisnombre')->get();        
        $ocupaciones = DB::table('ocupaciones')->where('staff', '1')->orderBy('ocupacion')->get();        
        return view('principal',compact('paises', 'ocupaciones'));
        }

    public function registroStaffAdmin()
        {
        $paises = DB::table('pais')->orderBy('paisnombre')->get();        
        $ocupaciones = DB::table('ocupaciones')->where('staff', '1')->orderBy('ocupacion')->get();        
        return view('principal2',compact('paises', 'ocupaciones'));
        }
    
    public function registroExpositor()
        {        
        $paises = DB::table('pais')->orderBy('paisnombre')->get();        
        $ocupaciones = DB::table('ocupaciones')->where('staff', '2')->orderBy('ocupacion')->get();        
        return view('principal',compact('paises', 'ocupaciones'));        
        }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ParticipantesRequest $request)
    {
        $empresa=$request->input('txtNombreEmpresa');
        if ($empresa=='')
        {
            $empresa='S-E';
        }        
        $id = DB::table('participantes')->insertGetId([
            'nombre' => strtoupper($request->input('txtNombre')),
            'apellido_pat' => strtoupper($request->input('txtAP')),
            'apellido_mat' => strtoupper($request->input('txtAM')),
            'email' => $request->input('email'),
            'idSexo' => $request->input('cboSexo'),
            'idEdad' => $request->input('cboEdad'),
            'celular' => $request->input('txtCel'),
            'idOcupacion' => $request->input('cboSubOcupaciones'),
            'idEstado' => $request->input('cboEst'),
            'ciudad' => strtoupper($request->input('txtCd')),
            'cp' => $request->input('txtCP'),
            'direccion' => strtoupper($request->input('txtDir')),            
            'nombreEmpresa' => strtoupper($empresa),
            'aceptaAviso' => $request->input('chkAviso'),
            'fechaRegistro' => date('Y-m-d H:i:s'),
        ]);
        //return redirect('/imprimir');
        $cboOcupa=$request->input('cboOcupa');
        $nombreSolo= strtoupper($request->input('txtNombre'));
        $AP=strtoupper($request->input('txtAP'));
        $AM=strtoupper($request->input('txtAM'));        
        return view('imprime', compact("id", "cboOcupa", "nombreSolo", "AP", "AM", "empresa"));  
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, $id)
    {
        if ($request->ajax()){
            $estados = DB::table('estado')->where('ubicacionpaisid', $id)->get();
            return response()->json($estados);
        }      
        
    }

    /**
     * Display the specified resource.
     *
     * @param  int   $idOcupa
     * @return \Illuminate\Http\Response
     */

    public function show2(Request $request, $idOcupa)
    {        
        if ($request->ajax()){
            $subOcupaciones = DB::table('subocupaciones')->where('idOcupaciones', $idOcupa)->get();                                                         
            return response()->json($subOcupaciones);
        }      
        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
