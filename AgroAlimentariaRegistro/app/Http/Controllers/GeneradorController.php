<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class GeneradorController extends Controller
{
    //   
    public function imprimir($id, $cboOcupa, $nombre, $AP, $AM, $empresa){        
        $pdf = \PDF::loadView('ejemplo2', compact('id', 'cboOcupa', 'nombre', 'AP', 'AM', 'empresa'));
        return $pdf->download('MI_GAFETE_AGROALIMENTARIA_ZACATECAS_2019.pdf');
      }

}