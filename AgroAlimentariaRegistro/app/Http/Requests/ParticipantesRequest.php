<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ParticipantesRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {        
        return [            
            'txtNombre' => 'required',
            'txtAP' => 'required',
            'txtAM' => 'required',
            'email' => 'email|required|unique:participantes',
            'txtCel' => 'required|numeric',
            'txtCd' => 'required',
            'txtCP' => 'required|numeric',
            'txtDir' => 'required',
                        
            'cboSexo' => 'required|integer|not_in:0',
            'cboEdad' => 'required|integer|not_in:0',
            'cboEst' => 'integer|not_in:0',
            'cboSubOcupaciones' => 'required|integer|not_in:0',            
            'chkAviso' => 'required',                       
        ];
    }

    public function messages()
    {
        return [
            'txtNombre.required' => 'El Campo Nombre es Obligatorio',
            'txtAP.required' => 'El Campo Apellido Paterno es Obligatorio',
            'txtAM.required' => 'El Campo Apellido Materno es Obligatorio',
            'txtEmail.required' => 'El Campo Correo Electrónico es Obligatorio',
            'email.email' => 'El Campo Correo Electrónico no es Válido',
            'txtCel.required' => 'El Campo No. Celular es Obligatorio',
            'txtCel.numeric' => 'El Campo No. Celular debe ser un Número Válido',
            'txtCd.required' => 'El Campo Ciudad es Obligatorio',
            'txtCP.required' => 'El Campo Código Postal es Obligatorio',
            'txtCP.numeric' => 'El Campo Código Postal debe ser un Número Válido',
            'txtDir.required' => 'El Campo Direccion es Obligatorio',                        
            'cboSexo.integer' => 'El Campo Sexo es Obligatorio',
            'cboEdad.integer' => 'El Campo Edad es Obligatorio',            
            'cboEst.integer' => 'El Campo Estado es Obligatorio',
            'cboSubOcupaciones.integer' => 'El Campo SubOcupaciones es Obligatorio',            
            'chkAviso.required' => 'Debe Aceptar el Aviso de Privacidad',
        ];
    }
}
