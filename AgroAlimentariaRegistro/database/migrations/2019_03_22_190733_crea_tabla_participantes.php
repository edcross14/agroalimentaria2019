<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreaTablaParticipantes extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('participantes', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('nombre',75);
            $table->string('apellido_pat',75);
            $table->string('apellido_mat',75);
            $table->string('email',75);
            $table->integer('idSexo');
            $table->integer('idEdad');
            $table->string('celular',20);
            $table->integer('idOcupacion');
            $table->integer('idEstado');
            $table->string('ciudad',70);
            $table->string('cp',10);
            $table->string('direccion',50);
            $table->string('nombreEmpresa',70);
            $table->boolean('aceptaAviso');
            $table->dateTime('fechaRegistro');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('participantes');
    }
}
