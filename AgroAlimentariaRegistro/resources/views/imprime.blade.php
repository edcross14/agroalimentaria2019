
@extends('layout')

@section('titulo')
    Imprimir Gafete
@endsection

@section('contenido')
    <form action="{{ route('participante.store') }}" method="post">
    @csrf                  
        <main role="main" class="flex-shrink-0">
            <div class="container">
                <h2 class="mt-5">&nbsp</h2>  
                <div class="input-group mb-5">
                    <h1 class="mb-5 center-block">REGISTRO EXITOSO, FAVOR DE DESCARGAR E IMPRIMIR TU GAFETE</h1>
                  
                        
                            <a href="{{route('imprimir', [$id, $cboOcupa, $nombreSolo, $AP, $AM, $empresa])}}" type="button" class="btn btn-lg u-btn-primary u-btn-hover-v1-1 g-mr-10 g-mb-15" data-report_id="{{$id}}" >Descargar Gafete</a>
                            <a href="{{route('participante.registro')}}" type="button" class="btn btn-lg u-btn-primary u-btn-hover-v1-1 g-mr-10 g-mb-15" data-report_id="{{$id}}" >Regresar al formulario de registro</a>                           
                    
                </div>
            </div>
        </main>  
    </form>        
@endsection