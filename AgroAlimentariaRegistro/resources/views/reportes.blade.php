@extends('layout2')

@section('titulo')
    Reportes de Participantes    
@endsection

@section('contenido')   
<div class="container" >
    <table class="table" style="margin=30px">
        <thead>
            <th>Ocupaciones</th> 
            <th>Total</th>                  
        </thead>
        <tbody>
            @php
                {{                    
                    $cadenaValores="";                    
                }}
            @endphp                   
            @forelse ($participantes as $participante)
                <tr>                    
                    <td>{{ $participante->ocupacion }}</td>
                    <td>{{ $participante->total }}</td>                    
                </tr>
                @php
                    {{                                        
                    $cadenaValores=$cadenaValores  . $participante->total . ','; 
                    }}
                @endphp
            @empty
                <tr>
                    <td>No hay participantes registrados</td>
                </tr>
            @endforelse
        </tbody>
    </table>  
    {!! $participantes->links() !!}
    <canvas id="canvas"></canvas>    	
</div>  
    
    <script>
		var color = Chart.helpers.color;
		var barChartData = {
			labels: ['COMPRADOR', 'CONFERENCISTA', 'EMPRENDEDOR', 'ESTUDIANTE', 'EXPOSITOR', 'INSTITUCIONAL', 'PRODUCTOR', 'PROF./INVEST', 'SERVICIOS', 'STAFF', 'VISITA EN GENERAL'],
			datasets: [{
				label: 'No. de Reportes',
				backgroundColor: color(window.chartColors.red).alpha(0.5).rgbString(),
				borderColor: window.chartColors.red,
				borderWidth: 1,                
				data: [{{$cadenaValores}}]
			}]

		};	

		window.onload = function() {
			var ctx = document.getElementById('canvas').getContext('2d');
			window.myBar = new Chart(ctx, {
				type: 'bar',
				data: barChartData,
				options: {
					responsive: true,
					legend: {
						position: 'top',
					},
					title: {
						display: true,
						text: 'Reportes del Dia: <%=Fecha%>'
					}
				}
			});
		};		
	</script>
@endsection