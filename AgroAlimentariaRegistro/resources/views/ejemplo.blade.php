

<!DOCTYPE html>
<html lang="es">
    <head>
        <meta charset="UTF-8">
        <title>Document</title>
        <style>
        h1{
        text-align: center;
        text-transform: uppercase;
        }
        .contenido{
        font-size: 20px;
        }
        #primero{
        background-color: #ccc;
        }
        #segundo{
        color:#44a359;
        }
        #tercero{
        text-decoration:line-through;
        }
    </style>
    </head>
    <body>
    @csrf 
        <h1>Titulo de prueba</h1>
        <hr>
        <div class="contenido">
            <img src="{{ '../public/imagenes/Staff.jpg' }}" height="820px" width="612px">
            <p id="primero">{{ $id }}</p>
            <p id="segundo">{{ $cboOcupa }}</p>
            <p id="tercero"></p>
        </div>
    </body>
</html>

