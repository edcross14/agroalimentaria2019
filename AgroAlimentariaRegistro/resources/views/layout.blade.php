<!doctype html>
<html class="h-100">
  <head>
    
    <title>@yield('titulo')</title>
    
    <!-- Required Meta Tags Always Come First -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta http-equiv="x-ua-compatible" content="ie=edge">



    <!-- Google Fonts -->
    <link rel="stylesheet" href="//fonts.googleapis.com/css?family=Open+Sans:400,300,500,600,700,800">

    <!-- CSS Global Compulsory -->
    <link rel="stylesheet" href="/vendor/bootstrap/bootstrap.min.css">

    <!-- CSS Implementing Plugins -->
    <link rel="stylesheet" href="/vendor/icon-awesome/css/font-awesome.min.css">
    <link rel="stylesheet" href="/vendor/icon-line/css/simple-line-icons.css">
    <link rel="stylesheet" href="/vendor/icon-hs/style.css">
    <link rel="stylesheet" href="/vendor/hamburgers/hamburgers.min.css">
    <link rel="stylesheet" href="/vendor/animate.css">
    <link rel="stylesheet" href="/vendor/slick-carousel/slick/slick.css">
    <link rel="stylesheet" href="/vendor/fancybox/jquery.fancybox.css">
    <link  rel="stylesheet" href="/vendor/jquery-ui/themes/base/jquery-ui.min.css">

      <!-- CSS Implementing Plugins -->
      <link  rel="stylesheet" href="/vendor/dzsparallaxer/dzsparallaxer.css">
      <link  rel="stylesheet" href="/vendor/dzsparallaxer/dzsscroller/scroller.css">
      <link  rel="stylesheet" href="/vendor/dzsparallaxer/advancedscroller/plugin.css">
    
      <!-- CSS Implementing Plugins -->
                    <link  rel="stylesheet" href="/vendor/animate.css">
                    <link  rel="stylesheet" href="/vendor/custombox/custombox.min.css">
    <!-- CSS Template -->
    <link rel="stylesheet" href="/css/styles.op-event.css">

    <!-- CSS Customization -->
    <link rel="stylesheet" href="/css/custom.css">
    
   
  </head>
  <body class="d-flex flex-column">
   
    <!-- Fixed navbar -->
    
      
        <a class="navbar-brand" href="https://agroalimentariazac.mx/2019/">
          <div align="center">
            <img src="/imagenes/Banner.png" width="100%" class="d-inline-block align-top" alt="">          
          </div>
        </a>            
      
    
    {!! Html::script('js/jquery-2.1.0.min.js') !!}
   

    <!-- Begin page content -->
    <div class="text-center g-mb-70">
        <h2 class="text-uppercase g-line-height-1 g-font-weight-700 g-font-size-30 g-color-white g-mb-30">REGISTRO DE PARTICIPANTES</h2>
    </div>
    <div class="row d-flex justify-content-center"> 
        <div class="col-md-6">
          <div class="align-self-center w-100">
            
              @yield('contenido')
            
          </div>
        </div>
    </div>
    <footer class="text-center g-pt-60 g-pb-30">
    <div class="container">
    

      <small class="d-block g-font-size-default g-mb-20 gb g-color-white"> 2019 Derechos Reservados 
        <a class="g-color-primary g-color-primary--hover" >AGROALIMENTARIA ZACATECAS 2019</a>
      </small>

    
    </div>
  </footer>

  <!-- JS Global Compulsory -->
  <script src="/vendor/jquery/jquery.min.js"></script>
    <script src="/vendor/jquery-migrate/jquery-migrate.min.js"></script>
    <script src="/vendor/popper.js/popper.min.js"></script>
    <script src="/vendor/bootstrap/bootstrap.min.js"></script>

    <!-- JS Implementing Plugins -->
    <script src="/vendor/appear.js"></script>
    <script src="/vendor/masonry/dist/masonry.pkgd.min.js"></script>
    <script src="/vendor/imagesloaded/imagesloaded.pkgd.min.js"></script>
    <script src="/vendor/jquery.countdown.min.js"></script>
    <script src="/vendor/fancybox/jquery.fancybox.js"></script>
    <script src="/vendor/slick-carousel/slick/slick.js"></script>
    <script src="/vendor/gmaps/gmaps.min.js"></script>

    <!-- JS Unify -->
    <script src="/js/hs.core.js"></script>
    <script src="/js/components/hs.header.js"></script>
    <script src="/js/helpers/hs.hamburgers.js"></script>
    <script src="/js/components/hs.scroll-nav.js"></script>
    <script src="/js/components/hs.tabs.js"></script>
    <script src="/js/components/hs.countdown.js"></script>
    <script src="/js/components/hs.carousel.js"></script>
    <script src="/js/components/gmap/hs.map.js"></script>
    <script src="/js/components/hs.go-to.js"></script>

    
    <!-- JS Customization -->
    <script src="/js/custom.js"></script>

    <!-- JS Plugins Init. -->
    <script>
      // initialization of google map
      function initMap() {
        $.HSCore.components.HSGMap.init('.js-g-map');
      }

      $(document).on('ready', function () {
        // initialization of carousel
        $.HSCore.components.HSCarousel.init('.js-carousel');

        // initialization of header
        $.HSCore.components.HSHeader.init($('#js-header'));
        $.HSCore.helpers.HSHamburgers.init('.hamburger');

        // initialization of tabs
        $.HSCore.components.HSTabs.init('[role="tablist"]');

        // initialization of go to section
        $.HSCore.components.HSGoTo.init('.js-go-to');

        // initialization of countdowns
        var countdowns = $.HSCore.components.HSCountdown.init('.js-countdown', {
          yearsElSelector: '.js-cd-years',
          monthElSelector: '.js-cd-month',
          daysElSelector: '.js-cd-days',
          hoursElSelector: '.js-cd-hours',
          minutesElSelector: '.js-cd-minutes',
          secondsElSelector: '.js-cd-seconds'
        });

      
      });

      $(window).on('load', function() {
        // initialization of HSScrollNav
        $.HSCore.components.HSScrollNav.init($('#js-scroll-nav'), {
          duration: 700
        });

        // initialization of masonry.js
        $('.masonry-grid').imagesLoaded().then(function () {
          $('.masonry-grid').masonry({
            columnWidth: '.masonry-grid-sizer',
            itemSelector: '.masonry-grid-item',
            percentPosition: true
          });
        });
      });

      $(window).on('resize', function () {
        setTimeout(function () {
          $.HSCore.components.HSTabs.init('[role="tablist"]');
        }, 200);
      });
    </script>

     
      <!-- JS Plugins Init. -->
      <script >
        // initialization of google map
        function initMap() {
          $.HSCore.components.HSGMap.init('.js-g-map');
        }
      </script>

        <!-- JS Implementing Plugins -->
        <script  src="/vendor/appear.js"></script>

        <!-- JS Unify -->
        <script  src="/js/components/hs.counter.js"></script>

        <!-- JS Plugins Init. -->
        <script >
          $(document).on('ready', function () {
            // initialization of counters
            var counters = $.HSCore.components.HSCounter.init('[class*="js-counter"]');
          });
        </script>

                  <!-- JS Implementing Plugins -->
                  <script  src="/vendor/custombox/custombox.min.js"></script>

                  <!-- JS Unify -->
                  <script  src="/js/components/hs.modal-window.js"></script>

                  <!-- JS Plugins Init. -->
                  <script >
                    $(document).on('ready', function () {
                      // initialization of popups
                      $.HSCore.components.HSModalWindow.init('[data-modal-target]');
                    });
                  </script>
                

        <!-- JS Implementing Plugins -->
        <script  src="/vendor/dzsparallaxer/dzsparallaxer.js"></script>
        <script  src="/vendor/dzsparallaxer/dzsscroller/scroller.js"></script>
        <script  src="/vendor/dzsparallaxer/advancedscroller/plugin.js"></script>

      
    {!! Html::script('js/dropdown.js') !!}
  </body>
</html>