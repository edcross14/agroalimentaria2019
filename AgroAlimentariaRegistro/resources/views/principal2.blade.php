@extends('layout2')

@section('titulo')
    Registro de Participantes
@endsection

@section('contenido')    
    <form action="{{ route('participante.store') }}" method="post">
        @csrf
        <main role="main" class="flex-shrink-0">
            <div class="container">
                <h2 class="mt-5">Datos Personales</h2>    
                @if(!$errors->isEmpty())
                    <div class="alert alert-warning alert-dismissible fade show" role="alert">
                        @foreach ($errors->all() as $error)
                            <p>{{ $error }}</p>
                        @endforeach
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <script>
                        $(document).ready(function(){
                            function loadOcupa(){
                                var idOcupa = $('#cboOcupa').val();           
                                console.log("jakob " . idOcupa);                     
                                $.get("show2/" + idOcupa +"", function(response, state){                                    		
                                    const valorAnt = '{{ old('cboSubOcupaciones') }}';		                                            
                                    $("#cboSubOcupaciones").empty();		
                                    for(i=0; i<response.length; i++){                                        
                                        seleccionado=(valorAnt==response[i].idSubocupacion) ? 'selected' : '';                                        
                                        $("#cboSubOcupaciones").append("<option value='"+response[i].idSubocupacion+"' " + seleccionado + ">"+ response[i].subOcupacion+"</option>");
                                    }
                                })
                            }

                            function loadEst(){
                                var idPais = $('#cboPais').val();                                
                                $.get("show/" + idPais +"", function(response, state){                                    		
                                    const valorAnt = '{{ old('cboEst') }}';		                                            
                                    $("#cboEst").empty();		
                                    for(i=0; i<response.length; i++){                                        
                                        seleccionado=(valorAnt==response[i].id) ? 'selected' : '';                                        
                                        $("#cboEst").append("<option value='"+response[i].id+"' " + seleccionado + ">"+ response[i].estadonombre+"</option>");
                                    }
                                })
                            }
                            loadOcupa();
                            loadEst();
                        })
                        
                        
                    </script>
                @endif                 
                <div class="input-group mb-3">
                    <div class="input-group-prepend">
                        <span class="input-group-text" id="basic-addon1" style="background-color: #a5d6a7; color:white;">Nombre</span>
                    </div>
                    <input type="text" name="txtNombre" id="txtNombre" value="{{ @old('txtNombre') }}" class="form-control" placeholder="NOMBRE" aria-label="NOMBRE" aria-describedby="basic-addon1" style="text-transform:uppercase;">
                    <!--{!! $errors->first('txtNombre','<span class="error">:message</span>') !!}-->
                </div>
                <div class="input-group mb-3">
                    <div class="input-group-prepend">
                        <span class="input-group-text" id="basic-addon1" style="background-color: #a5d6a7; color:white;">Apellido Paterno</span>
                    </div>
                    <input type="text" name="txtAP" id="txtAP" value="{{ @old('txtAP') }}" class="form-control" placeholder="APELLIDO PATERNO" aria-label="APELLIDO PATERNO" aria-describedby="basic-addon1" style="text-transform:uppercase;">
                </div>
                <div class="input-group mb-3">                    
                    <div class="input-group-prepend">
                        <span class="input-group-text" id="basic-addon1" style="background-color: #a5d6a7; color:white;">Apellido Materno</span>
                    </div>
                    <input type="text" name="txtAM" id="txtAM" value="{{ @old('txtAM') }}"  class="form-control" placeholder="APELLIDO MATERNO" aria-label="APELLIDO MATERNO" aria-describedby="basic-addon1" style="text-transform:uppercase;">                    
                </div>

                <div class="input-group mb-3">
                    <div class="input-group-append">
                        <span class="input-group-text" id="basic-addon2" style="background-color: #a5d6a7; color:white;">Correo Electrónico</span>
                    </div>
                    <input type="text" name="email" id="email" value="{{ @old('email') }}"  class="form-control" placeholder="CORREO ELECTRÓNICO" aria-label="Recipient's username" aria-describedby="basic-addon2">                    
                    
                </div>

                <div class="input-group mb-3">
                    <div class="input-group-prepend">
                        <label class="input-group-text" for="inputGroupSelect01" style="background-color: #a5d6a7; color:white;">Género</label>
                    </div>
                    <select class="custom-select" id="cboSexo" name="cboSexo">
                        <option selected>ESCOGER GÉNERO</option>
                        <option value='1' {{ @old('cboSexo')=='1' ? 'selected' : '' }}>Femenino</option>
                        <option value='2' {{ @old('cboSexo')=='2' ? 'selected' : '' }}>Masculino</option>          
                    </select>

                    <div class="input-group-prepend">
                        <label class="input-group-text" for="inputGroupSelect01" style="background-color: #a5d6a7; color:white;">Edad</label>
                    </div>
                    <select class="custom-select" id="cboEdad" name="cboEdad">
                    <option selected>ESCOGER EDAD</option>
                    <option value='1' {{ @old('cboEdad')=='1' ? 'selected' : '' }}>De 18 a 25</option>
                    <option value='2' {{ @old('cboEdad')=='2' ? 'selected' : '' }}>De 26 a 35</option>
                    <option value='3' {{ @old('cboEdad')=='3' ? 'selected' : '' }}>De 36 a 45</option>
                    <option value='4' {{ @old('cboEdad')=='4' ? 'selected' : '' }}>De 46 a 55</option>
                    <option value='5' {{ @old('cboEdad')=='5' ? 'selected' : '' }}>De 56 a 65</option>
                    <option value='6' {{ @old('cboEdad')=='6' ? 'selected' : '' }}>Mas de 65</option>
                    </select>
                </div>

                <div class="input-group mb-3">
                    <div class="input-group-prepend">
                        <span class="input-group-text" id="basic-addon1" style="background-color: #a5d6a7; color:white;">No. Celular</span>
                    </div>
                    <input type="text" name="txtCel" id="txtCel" value="{{ @old('txtCel') }}"  class="form-control" placeholder="CELULAR" aria-label="CELULAR" aria-describedby="basic-addon1">
                </div>     
                <div class="input-group mb-3">
                    <div class="input-group-prepend">
                        <label class="input-group-text" for="inputGroupSelect01" style="background-color: #a5d6a7; color:white;">Ocupación</label>
                    </div>
                    <select class="custom-select" id="cboOcupa" name="cboOcupa">
                    <option selected>ESCOGER OCUPACION</option>
                        @foreach($ocupaciones as $ocupacion)
                            <option value='{{$ocupacion->idOcupaciones}}' {{ @old('cboOcupa')==$ocupacion->idOcupaciones ? 'selected' : '' }}>{{$ocupacion->ocupacion}}</option>                            
                        @endforeach  
                    </select>  
                </div>
                <div class="input-group mb-3">
                    <div class="input-group-prepend">
                        <label class="input-group-text" for="inputGroupSelect01" style="background-color: #a5d6a7; color:white;">Sector</label>
                    </div>
                    
                        <select class="custom-select" id="cboSubOcupaciones" name="cboSubOcupaciones">
                            <option selected>ESCOGER SECTOR</option>                                             
                        </select>
                    
                </div>
                <div class="input-group mb-3">
                    <div class="input-group-prepend">
                        <label class="input-group-text" for="inputGroupSelect01" style="background-color: #a5d6a7; color:white;">Pais</label>
                    </div>                    
                    <select class="custom-select" id="cboPais" name="cboPais">
                        <option selected>ESCOGER PAIS</option>
                        @foreach($paises as $pais)
                            <option value='{{$pais->id}}' {{ @old('cboPais')==$pais->id ? 'selected' : '' }}>{{$pais->paisnombre}}</option>                            
                        @endforeach                        
                    </select>
                </div>
                <div class="input-group mb-3">
                    <div class="input-group-prepend">
                        <label class="input-group-text" for="inputGroupSelect01" style="background-color: #a5d6a7; color:white;">Estado</label>
                    </div>
                    
                        <select class="custom-select" id="cboEst" name="cboEst">
                            <option selected>ESCOGER ESTADO</option>                                             
                        </select>
                   
                </div>
                <div class="input-group mb-3">
                    <div class="input-group-prepend">
                        <span class="input-group-text" id="basic-addon1" style="background-color: #a5d6a7; color:white;">Ciudad</span>
                    </div>
                    <input type="text" name="txtCd" id="txtCd" value="{{ @old('txtCd') }}"  class="form-control" placeholder="CIUDAD" aria-label="CIUDAD" aria-describedby="basic-addon1" style="text-transform:uppercase;">
                    
                    <div class="input-group-prepend">
                        <span class="input-group-text" id="basic-addon1" style="background-color: #a5d6a7; color:white;">C.P.</span>
                    </div>
                    <input type="text" name="txtCP" id="txtCP" value="{{ @old('txtCP') }}"  class="form-control" placeholder="CÓDIGO POSTAL" aria-label="CÓDIGO POSTAL" aria-describedby="basic-addon1">
                    
                    <div class="input-group-prepend">
                        <span class="input-group-text" id="basic-addon1" style="background-color: #a5d6a7; color:white;">Dirección</span>
                    </div>
                    <input type="text" name="txtDir" id="txtDir" value="{{ @old('txtDir') }}" class="form-control" placeholder="DIRECCIÓN" aria-label="DIRECCIÓN" aria-describedby="basic-addon1" style="text-transform:uppercase;">                    
                </div>
                <div class="input-group mb-3">
                    <div class="input-group-prepend">
                        <span class="input-group-text" id="basic-addon1" style="background-color: #a5d6a7; color:white;">Nombre Empresa</span>
                    </div>
                    <input type="text" name="txtNombreEmpresa" id="txtNombreEmpresa" value="{{ @old('txtNombreEmpresa') }}" class="form-control" placeholder="NOMBRE EMPRESA" aria-label="NOMBRE EMPRESA" aria-describedby="basic-addon1" style="text-transform:uppercase;">                    
                </div>
                
                <div class="input-group mb-3">
                    <div class="input-group-prepend">        
                        <div class="alert alert-success" role="alert">
                            <input type="checkbox" aria-label="Checkbox for following text input" value="1" name="chkAviso" id="chkAviso">&nbsp Acepto
                            <a href="http://agroalimentariazac.mx/aviso-de-privacidad.html" target="_blank">AVISO DE PRIVACIDAD</a>
                        </div>      
                    </div>            
                </div>               
                
                <div class="input-group mb-3">
                    <button type="submit" class="btn btn-primary btn-lg btn-block" style="background-color: #549452; color:white;">Registrar</button>
                </div>    
            </div>             
        </main>
    </form>    
@endsection