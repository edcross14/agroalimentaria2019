<!DOCTYPE html>
<html lang="en">

<head>

  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">

  <title> @yield('titulo')</title>

  <!-- Bootstrap core CSS -->
  <link href="/css/bootstrap.min.css" rel="stylesheet">

  <!-- Custom styles for this template -->
  <link href="/css/simple-sidebar.css" rel="stylesheet">

  <script src="/graficas/Chart.min.js"></script>
    <script src="/graficas/utils.js"></script>
    
    <style>
      canvas {
                -moz-user-select: none;
                -webkit-user-select: none;
                -ms-user-select: none;
            }
      </style>
       {!! Html::script('js/jquery-2.1.0.min.js') !!}

</head>

<body>     

  <div class="d-flex" id="wrapper">

    <!-- Sidebar -->
    <div class="bg-light border-right" id="sidebar-wrapper">
      <div class="sidebar-heading">
        PANEL DE CONTROL 
        </div>
      <div class="list-group list-group-flush">
        <a href="{{route('participante.index')}}" class="list-group-item list-group-item-action bg-light">Reimpresion Gafete</a>
        <a href="{{route('participante.registroStaffAdmin')}}" class="list-group-item list-group-item-action bg-light">Altas de Staff</a>
        <a href="{{route('participante.registroAdmin')}}" class="list-group-item list-group-item-action bg-light">Altas de Asistentes</a>
        <a href="{{route('participante.reportes')}}" class="list-group-item list-group-item-action bg-light">Reportes</a>        
      </div>
    </div>
    <!-- /#sidebar-wrapper -->

    <!-- Page Content -->
    <div id="page-content-wrapper">
     
      <a class="navbar-brand" href="https://agroalimentariazac.mx/2019/">  
        <div align="center">
          <img src="/imagenes/Banner.png" width="100%" class="d-inline-block align-top" alt="">          
        </div>
      </a>
        

      <nav class="navbar navbar-expand-lg navbar-light bg-light border-bottom">
        <button class="btn btn-info" id="menu-toggle">Ocultar Menu</button>

        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>
       
      </nav>

      <div class="container-fluid">
        <!-- Begin page content -->        
        @yield('contenido')
      </div>
    </div>
    <!-- /#page-content-wrapper -->

  </div>
  <!-- /#wrapper -->

  <!-- Bootstrap core JavaScript -->
  <script src="/js/jquery.min.js"></script>
  <script src="/js/bootstrap.bundle.min.js"></script>  

  <!-- Menu Toggle Script -->
  <script>
    $("#menu-toggle").click(function(e) {
      e.preventDefault();
      $("#wrapper").toggleClass("toggled");
    });
  </script>

</body>

<script src="/js/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
  <script>window.jQuery || document.write('<script src="/js/jquery-slim.min.js"><\/script>')</script><script src="/js/bootstrap.bundle.min.js" integrity="sha384-xrRywqdh3PHs8keKZN+8zzc5TX0GRTLCcmivcbNJWm2rs5C8PRhcEn3czEjhAO9o" crossorigin="anonymous"></script></body>
  {!! Html::script('js/dropdown.js') !!}

</html>
