@extends('layout2')

@section('titulo')
    Consulta de Participantes
@endsection

@section('contenido')
<br><br>   
<div class="container" >
    <form method="GET" action="{{ route('participante.consulta') }}">
    <table class="table" style="margin=30px">
        <thead>
            <th>Nombre</th> 
            <th>Correo</th>      
            <th>Sector</th>
            <th>Estado</th>
            <th>Pais</th>
            <th>Acciones</th>
        </thead>
        <tbody>
            <tr>
                
                <td><input name="mail" id="mail" value="{{ @old('mail') }}" class="form-control" type="text" placeholder="Buscar por Correo"></td>
                <td><button type="submit" class="btn btn-primary">Buscar</button></td>                
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
            </tr>            
            @forelse ($participantes as $participante)
                <tr>
                    <td>{{ $participante->nombre }} {{ $participante->apellido_pat }} {{ $participante->apellido_mat }}</td>
                    <td>{{ $participante->email }}</td>
                    <td>{{ $participante->subOcupacion }}</td>
                    <td>{{ $participante->estadonombre }}</td>
                    <td>{{ $participante->paisnombre }}</td>
                    <td>                                        
                    <a class="btn btn-success btn-sm" href="{{route('imprimir', [$participante->id, $participante->idOcupaciones, $participante->nombre, $participante->apellido_pat, $participante->apellido_mat, $participante->nombreEmpresa])}}">Gafete</a>                    
                    </td>
                </tr>
            @empty
                <tr>
                    <td>No hay médicos registrados</td>
                </tr>
            @endforelse
        </tbody>
    </table>     
    {!! $participantes->links() !!}
    </form> 
</div>  
@endsection