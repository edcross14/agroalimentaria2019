<html>
    <head>
    <style>
        /** 
        * Set the margins of the PDF to 0
        * so the background image will cover the entire page.
        **/
        @page {
            margin: 0cm 0cm;
        }

        /**
        * Define the real margins of the content of your PDF
        * Here you will fix the margins of the header and footer
        * Of your background image.
        **/
        body {
            margin-top:    3.5cm;
            margin-bottom: 1cm;
            margin-left:   1cm;
            margin-right:  1cm;
        }

        /** 
        * Define the width, height, margins and position of the watermark.
        **/
        #watermark {
            position: fixed;
            bottom:   0px;
            left:     0px;
            /** The width and height may change 
                according to the dimensions of your letterhead
            **/
            width:    21.8cm;
            height:   28cm;

            /** Your watermark should be behind every content**/
            z-index:  -1000;
        }

        #datos {            
            margin-top:    14.5cm;
            margin-bottom: 1cm;
            margin-left:   1.5cm;
            margin-right:  1cm;
            color:#00000;
        }
        font.tipo {
            font-family: Charcoal;
            font-style: normal;
            font-variant: normal;
            font-weight: 600;
            font-size: 30px;
            line-height: normal;
            }
    </style>
    </head>
    <body>
        <div id="watermark">        
        @if ($cboOcupa==1)
             <!--
            2 Productor            
            -->
            <img src="../public/imagenes/Productor.png" height="100%" width="100%" />
        @elseif ($cboOcupa==2)
            <!--
            3 Comprador            
            -->
            <img src="../public/imagenes/Comprador.png" height="100%" width="100%" />
        @elseif ($cboOcupa==11)
             <!--
            11 staff            
            -->
            <img src="../public/imagenes/Staff.png" height="100%" width="100%" />
        @elseif ($cboOcupa==12)
             <!--
            12 Expositor
            -->
            <img src="../public/imagenes/Expositor.png" height="100%" width="100%" />
        @elseif ($cboOcupa==13)
             <!--
            13 Conferencista
            -->
            <img src="../public/imagenes/Conferencista.png" height="100%" width="100%" />
        @else        
            <!--
            1 Empresario                        
            4 Comerciante
            5 Profesional
            6 Emprendedor
            7 Investigador
            8 Profesor
            9 Estudiante
            10 Visita en General
            -->    
            <img src="../public/imagenes/Visitante.png" height="100%" width="100%" />        
        @endif
        </div>

        <div id="datos"> 
            <!-- The content of your PDF here -->            
            <table width="50%">
                <tr>
                    <td width="50%" align="center"><font class="tipo">{{ $nombre }}</font></td>                    
                </tr>
                <tr>
                    <td width="50%" align="center"><font class="tipo">{{ $AP }}</font></td>                    
                </tr>
                <tr>
                    <td width="50%" align="center"><font class="tipo">{{ $AM }}</font></td>                    
                </tr>
                <tr>
                    <td width="50%" align="center">&nbsp;</td>
                    
                </tr>
                <tr>
                    <td width="50%" align="center"><font class="tipo">{{ $empresa }}</font></td>                    
                </tr>
                <tr>
                    <td width="50%" align="center"><img src="data:image/png;base64,{{DNS1D::getBarcodePNG($id, 'C39')}}" alt="barcode" /></td>                    
                </tr>
            </table>                                            
        </div>
    </body>
</html>