@extends('layout')

@section('titulo')
    Registro de Participantes
@endsection

@section('contenido')    
    <form action="{{ route('participante.store') }}" method="post">
        @csrf
        <main role="main" class="flex-shrink-0">
            <div class="container-fluid">
                <h2 class="mt-5">Datos Personales</h2>    
                @if(!$errors->isEmpty())
                    <div class="alert alert-warning alert-dismissible fade show" role="alert">
                        @foreach ($errors->all() as $error)
                            <p>{{ $error }}</p>
                        @endforeach
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <script>
                        $(document).ready(function(){
                            function loadOcupa(){
                                var idOcupa = $('#cboOcupa').val();                                
                                $.get("show2/" + idOcupa +"", function(response, state){                                    		
                                    const valorAnt = '{{ old('cboSubOcupaciones') }}';		                                            
                                    $("#cboSubOcupaciones").empty();		
                                    for(i=0; i<response.length; i++){                                        
                                        seleccionado=(valorAnt==response[i].idSubocupacion) ? 'selected' : '';                                        
                                        $("#cboSubOcupaciones").append("<option value='"+response[i].idSubocupacion+"' " + seleccionado + ">"+ response[i].subOcupacion+"</option>");
                                    }
                                })
                            }

                            function loadEst(){
                                var idPais = $('#cboPais').val();                                
                                $.get("show/" + idPais +"", function(response, state){                                    		
                                    const valorAnt = '{{ old('cboEst') }}';		                                            
                                    $("#cboEst").empty();		
                                    for(i=0; i<response.length; i++){                                        
                                        seleccionado=(valorAnt==response[i].id) ? 'selected' : '';                                        
                                        $("#cboEst").append("<option value='"+response[i].id+"' " + seleccionado + ">"+ response[i].estadonombre+"</option>");
                                    }
                                })
                            }
                            loadOcupa();
                            loadEst();
                        })
                        
                        
                    </script>
                @endif                 
                <div class="input-group mb-12 d-flex justify-content-end">                                        
                    <a href="{{ route('participante.consultarmail') }}" type="button" class="btn btn-outline-info btn-sm float-right">Descargar Gafete</a>
                </div>
                <br>
                <div class="input-group mb-3">
                    
                    <input type="text" name="txtNombre" id="txtNombre" value="{{ @old('txtNombre') }}" class="form-control g-color-white g-bg-black-opacity-0_6 g-bg-black-opacity-0_4--focus g-brd-black-opacity-0_4 g-brd-primary--focus rounded-0 g-pa-15" placeholder="NOMBRE" aria-label="NOMBRE" aria-describedby="basic-addon1" style="text-transform:uppercase;">                    
                </div>
                <div class="input-group mb-3">
                  
                    <input type="text" name="txtAP" id="txtAP" value="{{ @old('txtAP') }}" class="form-control g-color-white g-bg-black-opacity-0_6 g-bg-black-opacity-0_4--focus g-brd-black-opacity-0_4 g-brd-primary--focus rounded-0 g-pa-15" placeholder="APELLIDO PATERNO" aria-label="APELLIDO PATERNO" aria-describedby="basic-addon1" style="text-transform:uppercase;">
                </div>
                <div class="input-group mb-3">                    
                   
                    
                <input type="text" name="txtAM" id="txtAM" value="{{ @old('txtAM') }}"  class="form-control g-color-white g-bg-black-opacity-0_6 g-bg-black-opacity-0_4--focus g-brd-black-opacity-0_4 g-brd-primary--focus rounded-0 g-pa-15" placeholder="APELLIDO MATERNO" aria-label="APELLIDO MATERNO" aria-describedby="basic-addon1" style="text-transform:uppercase;">   
                                 
                </div>

                <div class="input-group mb-3">
                 
                    <input type="text" name="email" id="email" value="{{ @old('email') }}"  class="form-control g-color-white g-bg-black-opacity-0_6 g-bg-black-opacity-0_4--focus g-brd-black-opacity-0_4 g-brd-primary--focus rounded-0 g-pa-15" placeholder="CORREO ELECTRÓNICO" aria-label="Recipient's username" aria-describedby="basic-addon2">                    
                    
                </div>

                <div class="input-group mb-3">
                  
                    <select class="form-control g-color-white g-bg-black-opacity-0_6  g-brd-black-opacity-0_4 g-brd-primary--focus rounded-0 g-pa-15" id="cboSexo" name="cboSexo">
                        <option selected> GÉNERO</option>
                        <option value='1' {{ @old('cboSexo')=='1' ? 'selected' : '' }}>Femenino</option>
                        <option value='2' {{ @old('cboSexo')=='2' ? 'selected' : '' }}>Masculino</option>          
                    </select>

                   
                   
                    <select class="form-control g-color-white g-bg-black-opacity-0_6  g-brd-black-opacity-0_4 g-brd-primary--focus rounded-0 g-pa-15" id="cboEdad" name="cboEdad">
                    <option selected> EDAD</option>
                    <option value='1' {{ @old('cboEdad')=='1' ? 'selected' : '' }}>De 18 a 25</option>
                    <option value='2' {{ @old('cboEdad')=='2' ? 'selected' : '' }}>De 26 a 35</option>
                    <option value='3' {{ @old('cboEdad')=='3' ? 'selected' : '' }}>De 36 a 45</option>
                    <option value='4' {{ @old('cboEdad')=='4' ? 'selected' : '' }}>De 46 a 55</option>
                    <option value='5' {{ @old('cboEdad')=='5' ? 'selected' : '' }}>De 56 a 65</option>
                    <option value='6' {{ @old('cboEdad')=='6' ? 'selected' : '' }}>Mas de 65</option>
                    </select>

      

                <div class="input-group mb-3">
                  
                    <input type="text" name="txtCel" id="txtCel" value="{{ @old('txtCel') }}"  class="form-control g-color-white g-bg-black-opacity-0_6 g-bg-black-opacity-0_4--focus g-brd-black-opacity-0_4 g-brd-primary--focus rounded-0 g-pa-15" placeholder="CELULAR" aria-label="CELULAR" aria-describedby="basic-addon1">
                </div>     
                <div class="input-group mb-3">
                   
                    <select class="form-control g-color-white g-bg-black-opacity-0_6  g-brd-black-opacity-0_4 g-brd-primary--focus rounded-0 g-pa-15" id="cboOcupa" name="cboOcupa">
                    <option selected> OCUPACIÓN</option>
                        @foreach($ocupaciones as $ocupacion)
                            <option value='{{$ocupacion->idOcupaciones}}' {{ @old('cboOcupa')==$ocupacion->idOcupaciones ? 'selected' : '' }}>{{$ocupacion->ocupacion}}</option>                            
                        @endforeach  
                    </select>  
                </div>
                <div class="input-group mb-3">
                  
                    
                        <select class="form-control g-color-white g-bg-black-opacity-0_6  g-brd-black-opacity-0_4 g-brd-primary--focus rounded-0 g-pa-15" id="cboSubOcupaciones" name="cboSubOcupaciones">
                            <option selected> SECTOR</option>                                             
                        </select>
                    
                </div>
                <div class="input-group mb-3">
                               
                    <select class="form-control g-color-white g-bg-black-opacity-0_6  g-brd-black-opacity-0_4 g-brd-primary--focus rounded-0 g-pa-15" id="cboPais" name="cboPais">
                        <option selected> PAÍS</i></option>
                        @foreach($paises as $pais)
                            <option value='{{$pais->id}}' {{ @old('cboPais')==$pais->id ? 'selected' : '' }}>{{$pais->paisnombre}}</option>                            
                        @endforeach                        
                    </select>
                </div>
                <div class="input-group mb-3">
                   
                    
                        <select class="form-control g-color-white g-bg-black-opacity-0_6 g-bg-black-opacity-0_4--focus g-brd-black-opacity-0_4 g-brd-primary--focus rounded-0 g-pa-15" id="cboEst" name="cboEst">
                            <option selected> ESTADO</option>                                             
                        </select>
                   
                </div>
                <div class="input-group mb-3">
                   
                    <input type="text" name="txtCd" id="txtCd" value="{{ @old('txtCd') }}"  class="form-control g-color-white g-bg-black-opacity-0_6 g-bg-black-opacity-0_4--focus g-brd-black-opacity-0_4 g-brd-primary--focus rounded-0 g-pa-15" placeholder="CIUDAD" aria-label="CIUDAD" aria-describedby="basic-addon1" style="text-transform:uppercase;">
                    
                   
                    <input type="text" name="txtCP" id="txtCP" value="{{ @old('txtCP') }}"  class="form-control g-color-white g-bg-black-opacity-0_6 g-bg-black-opacity-0_4--focus g-brd-black-opacity-0_4 g-brd-primary--focus rounded-0 g-pa-15" placeholder="CÓDIGO POSTAL" aria-label="CÓDIGO POSTAL" aria-describedby="basic-addon1">
                    
                    
                    <input type="text" name="txtDir" id="txtDir" value="{{ @old('txtDir') }}" class="form-control g-color-white g-bg-black-opacity-0_6 g-bg-black-opacity-0_4--focus g-brd-black-opacity-0_4 g-brd-primary--focus rounded-0 g-pa-15" placeholder="DIRECCIÓN" aria-label="DIRECCIÓN" aria-describedby="basic-addon1" style="text-transform:uppercase;">                    
                </div>
                <div class="input-group mb-3">
                  
                    <input type="text" name="txtNombreEmpresa" id="txtNombreEmpresa" value="{{ @old('txtNombreEmpresa') }}" class="form-control g-color-white g-bg-black-opacity-0_6 g-bg-black-opacity-0_4--focus g-brd-black-opacity-0_4 g-brd-primary--focus rounded-0 g-pa-15" placeholder="NOMBRE DE EMPRESA O INSTITUCIÓN" aria-label="NOMBRE EMPRESA" aria-describedby="basic-addon1" style="text-transform:uppercase;">                    
                </div>
                
                <div class="input-group mb-3">
                    <div class="input-group-prepend">        
                        <div class="alert alert-success" role="alert">
                            <input type="checkbox" aria-label="Checkbox for following text input" value="1" name="chkAviso" id="chkAviso">&nbsp Acepto
                            <a href="http://agroalimentariazac.mx/aviso-de-privacidad.html" target="_blank">AVISO DE PRIVACIDAD</a>
                            
                        </div>      
                    </div>            
                </div>               
                
                <div class="input-group mb-3">
                 
                    <button class="btn btn-md u-btn-outline-primary g-mr-10 g-mb-15" type="submit" role="button">Enviar formulario</button>
                </div>    
            </div>             
        </main>
    </form>    
@endsection