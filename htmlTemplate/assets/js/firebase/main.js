// Paste the code from Firebase 
var config = {
    apiKey: "AIzaSyD6jpXBba4nUs-UoYoKQ89hlteioKaSK48",
    authDomain: "agro2019-1a4f7.firebaseapp.com",
    databaseURL: "https://agro2019-1a4f7.firebaseio.com",
    projectId: "agro2019-1a4f7",
    storageBucket: "agro2019-1a4f7.appspot.com",
    messagingSenderId: "37487841811"
};
firebase.initializeApp(config);

// Reference messages collection
var participantesGuardados = firebase.database().ref('participantesGuardados');
var contactosGuardados = firebase.database().ref('contactosGuardados');

$('#contactoForm').submit(function(e) {
    e.preventDefault();
 
    var newContacto= contactosGuardados.push();
    newContacto.set({
        email: $('.correo').val(),
        asunto: $('.subject').val(),
        mensaje: $('.message').val()
    });
 
    $('.success-message').show();
 
    $('#contactoForm')[0].reset();
});

$('#regsitroForm').submit(function(e) {
    e.preventDefault();
 
    var newParticipante= participantesGuardados.push();
    newParticipante.set({
        nombre: $('.nombreP').val(),
        apellidoPat: $('.apellidoPatP').val(),
        apellidoMat: $('.apellidoMatP').val(),
        correo: $('.correoP').val(),
        genero: $('.generoP').val(),
        edad: $('.edadP').val(),
        cel: $('.celularP').val(),
        ocupacion: $('.ocupacionP').val(),
        sector: $('.sectorP').val(),
        pais: $('.paisP').val(),
        estado: $('.estadoP').val(),
        ciudad: $('.ciudadP').val(),
        cp: $('.cpP').val(),
        domicilio: $('.domicilioP').val(),
        nombreEmpresa: $('.empresaP').val(),
       
       
    });
 
    $('.success-message').show();
 
    $('#regsitroForm')[0].reset();
});