@extends('participantes.layout')
   
@section('content')
    <div class="row">
        <div class="col-lg-12 margin-tb">
            <div class="pull-left">
                <h2>Editar Participante</h2>
            </div>
            <div class="pull-right">
                <a class="btn btn-primary" href="{{ route('participantes.index') }}"> Regresar</a>
            </div>
        </div>
    </div>
   
    @if ($errors->any())
        <div class="alert alert-danger">
            <strong>Whoops!</strong> Hubo un problema con los datos que escribiste.<br><br>
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
  
    <form action="{{ route('participantes.update',$participante->id) }}" method="POST">
        @csrf
        @method('PUT')
   
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12">
                <div class="form-group">
                    <strong>Nombre:</strong>
                    <input type="text" name="nombre" value="{{ $participante-> nombre}}" class="form-control" placeholder="Nombre">
                </div>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-12">
                <div class="form-group">
                    <strong>Apellido paterno:</strong>
                    <input class="form-control" style="height:150px" name="apellidoPaterno" value="{{ $participante-> apellidoPaterno}}" placeholder="Apellido paterno"></input>
                </div>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-12">
                <div class="form-group">
                    <strong>Apellido materno:</strong>
                    <input type="text" name="apellidoMaterno" value="{{ $participante-> apellidoMaterno}}" class="form-control" placeholder="Apellido materno">
                </div>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-12">
                <div class="form-group">
                    <strong>E-mail:</strong>
                    <input class="form-control" style="height:150px" name="email" value="{{ $participante-> email}}" placeholder="E-mail"></input>
                </div>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-12">
                <div class="form-group">
                    <strong>Género:</strong>
                    <input type="text" name="genero" value="{{ $participante-> genero}}" class="form-control" placeholder="Género">
                </div>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-12">
                <div class="form-group">
                    <strong>Edad:</strong>
                    <input class="form-control" style="height:150px" name="edad" value="{{ $participante-> edad}}" placeholder="Edad"></input>
                </div>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-12">
                <div class="form-group">
                    <strong>Número celular:</strong>
                    <input type="text" name="celular" value="{{ $participante-> celular}}" class="form-control" placeholder="Número celular">
                </div>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-12">
                <div class="form-group">
                    <strong>Ocupación:</strong>
                    <input class="form-control" style="height:150px" name="ocupacion" value="{{ $participante-> ocupacion}}" placeholder="Ocupación"></input>
                </div>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-12">
                <div class="form-group">
                    <strong>País:</strong>
                    <input type="text" name="pais" value="{{ $participante-> pais}}" class="form-control" placeholder="País">
                </div>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-12">
                <div class="form-group">
                    <strong>Estado:</strong>
                    <input class="form-control" style="height:150px" name="estado" value="{{ $participante-> estado}}" placeholder="Estado"></input>
                </div>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-12">
                <div class="form-group">
                    <strong>Ciudad:</strong>
                    <input type="text" name="ciudad" value="{{ $participante-> ciudad}}" class="form-control" placeholder="Ciudad">
                </div>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-12">
                <div class="form-group">
                    <strong>Código Postal:</strong>
                    <input class="form-control" style="height:150px" name="codigoPostal" value="{{ $participante-> codigoPostal}}" placeholder="Código Postal"></input>
                </div>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-12">
                <div class="form-group">
                    <strong>Dirección:</strong>
                    <input class="form-control" style="height:150px" name="direccion" value="{{ $participante-> direccion}}" placeholder="Dirección"></input>
                </div>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-12">
                <div class="form-group">
                    <strong>Nombre de empresa:</strong>
                    <input class="form-control" style="height:150px" name="nombreEmpresa" value="{{ $participante-> nombreEmpresa}}" placeholder="Nombre de empresa"></input>
                </div>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-12 text-center">
              <button type="submit" class="btn btn-primary">Enviar</button>
            </div>
        </div>
   
    </form>
@endsection