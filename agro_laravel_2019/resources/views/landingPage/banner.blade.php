<!-- Section Content -->
<section id="home" class="g-bg-img-hero g-pos-rel u-bg-overlay  g-pt-150" style="background-image: url({{asset('/img/recursosAgro/bannerAgro.png')}});">
        <div class="container g-max-width-750 u-bg-overlay__inner g-color-white g-mb-60">
          <!-- Countdown v4 
          <div class="js-countdown text-center text-uppercase u-countdown-v4 g-mb-40 g-mb-70--md"
               data-end-date="2019/08/22"
               data-month-format="%m"
               data-days-format="%D"
               data-hours-format="%H"
               data-minutes-format="%M"
               data-seconds-format="%S">
            <div class="d-inline-block g-font-size-12 g-px-10 g-px-50--md g-py-15">
              <div class="js-cd-days g-font-size-20 g-font-size-40--md g-font-weight-700 g-line-height-1 g-mb-5">33</div>
              <span>DÍAS</span>
            </div>

            <div class="d-inline-block g-font-size-12 g-brd-left g-brd-white-opacity-0_4 g-px-10 g-px-60--md g-py-15">
              <div class="js-cd-hours g-font-size-20 g-font-size-40--md g-font-weight-700 g-line-height-1 g-mb-5">01</div>
              <span>HORAS</span>
            </div>

            <div class="d-inline-block g-font-size-12 g-brd-left g-brd-white-opacity-0_4 g-px-10 g-px-60--md g-py-15">
              <div class="js-cd-minutes g-font-size-20 g-font-size-40--md g-font-weight-700 g-line-height-1 g-mb-5">52</div>
              <span>MINUTOS</span>
            </div>

            <div class="d-inline-block g-font-size-12 g-brd-left g-brd-white-opacity-0_4 g-px-10 g-px-50--md g-py-15">
              <div class="js-cd-seconds g-font-size-20 g-font-size-40--md g-font-weight-700 g-line-height-1 g-mb-5">52</div>
              <span>SEGUNDOS</span>
            </div>
          </div>-->
          <!-- End Countdown v4 -->
          <div class="js-countdown u-countdown-v5 text-center" data-end-date="2019/08/22" data-days-format="%D" data-hours-format="%H" data-minutes-format="%M" data-seconds-format="%S">
            <div class="d-inline-block g-mx-20 g-mb-40">
              <div class="js-cd-days g-font-size-56 g-font-weight-300 g-line-height-1"></div>
              <span>DÍAS</span>
            </div>
          
            <div class="hidden-down d-inline-block align-top g-font-size-28 g-font-weight-200 g-line-height-1_8">|</div>
          
            <div class="d-inline-block g-mx-20 g-mb-40">
              <div class="js-cd-hours g-font-size-56 g-font-weight-300 g-line-height-1"></div>
              <span>HORAS</span>
            </div>
          
            <div class="hidden-down d-inline-block align-top g-font-size-28 g-font-weight-200 g-line-height-1_8">|</div>
          
            <div class="d-inline-block g-mx-20 g-mb-40">
              <div class="js-cd-minutes g-font-size-56 g-font-weight-300 g-line-height-1"></div>
              <span>MINUTOS</span>
            </div>
          
            <div class="hidden-down d-inline-block align-top g-font-size-28 g-font-weight-200 g-line-height-1_8">|</div>
          
            <div class="d-inline-block g-mx-20 g-mb-40">
              <div class="js-cd-seconds g-font-size-56 g-font-weight-300 g-line-height-1"></div>
              <span>SEGUNDOS</span>
            </div>
          </div>
          <!--<h2 class="text-center text-uppercase h2 g-font-weight-700 g-font-size-40 g-font-size-60--md g-color-white g-mb-30 g-mb-70--md">UI &amp; UX Design 2017</h2> -->
            <img style="max-width: 100%; height: auto; width: auto;"
            src="{{asset('/img/recursosAgro/agroalimentariazac_logox5.png')}}" alt=""  align-items: g-absolute-centered;">
          <div class="row">
            <div class="col-sm-6 col-md-4 text-center text-md-left g-mb-30 g-mb-0--md">
              <div class="media">
                <div class="media-left d-flex align-self-center g-hidden-sm-down g-mr-20">
                  <i class="fa fa-calendar g-font-size-27 g-color-white-opacity-0_5"></i>
                </div>

                <div class="media-body text-uppercase">
                
                  <h3 class="h3 g-font-size-15 g-color-white mb-0">22, 23 Y 24 Agosto 2019</h3>

                </div>
              </div>
            </div>

            <div class="col-sm-6 col-md-5 text-center text-md-left g-mb-30 g-mb-0--md">
              <div class="media">
                <div class="media-left d-flex align-self-center g-hidden-sm-down g-mr-20">
                  <i class="fa fa-map-marker g-font-size-27 g-color-white-opacity-0_5"></i>
                </div>

                <div class="media-body text-uppercase">
                
                  <h3 class="h3 g-font-size-15 g-color-white mb-0">Palacio de Convenciones, Zacatecas</h3>
                </div>
              </div>
            </div>

            <div class="col-md-3 text-center text-md-right">
             <!--<a class="btn btn-lg text-uppercase u-btn-white g-font-weight-700 g-font-size-11 g-color-white--hover
               g-bg-primary--hover g-brd-none rounded-0 g-py-18 g-px-20" href="#registro">REGISTRATE AHORA</a>--> 
               <a href="#registro" class="btn btn-md u-btn-primary g-mr-10 g-mb-15">REGISTRATE AHORA</a>
            </div>
          </div>
        </div>

        <div class="container u-bg-overlay__inner g-bottom-minus-70 px-0">
          <div class="row u-shadow-v23 g-theme-bg-blue-dark-v2 mx-0">
            <div class="col-md-6 px-0">
              <div class="js-carousel text-center u-carousel-v5 g-overflow-hidden h-100 g-max-height-50vh"
                   data-infinite="true"
                   data-arrows-classes="u-arrow-v1 g-absolute-centered--y g-width-40 g-height-40 g-font-size-20 g-color-white g-color-primary--hover g-bg-primary g-bg-white--hover g-transition-0_2 g-transition--ease-in"
                   data-arrow-left-classes="fa fa-angle-left g-left-0"
                   data-arrow-right-classes="fa fa-angle-right g-right-0">
                <div class="js-slide g-bg-img-hero g-min-height-50vh" style="background-image: url({{asset('/img/recursosAgro/aboutImages/about2.png')}});"></div>

               
              </div>
            </div>

            <div class="col-md-6 d-flex g-min-height-50vh g-theme-color-gray-dark-v1 g-px-50 g-py-80 g-py-50--md g-py-20--lg">
              <div class="align-self-center w-100">
                <h2 class="text-uppercase g-font-weight-700 g-font-size-30 g-color-primary g-mb-10">Agroalimentaria Zacatecas 2019</h2>
                <h3 class="text-uppercase g-font-weight-500 g-font-size-13 g-color-white g-mb-20">Zacatecas, la potencia agroalimentaria de México</h3>
                <p class="g-font-size-14">Por segunda ocasión en el centro-norte del país, el Gobierno del Estado en conjunto con los productores de la cadena agroalimentaria, te invitan a ser parte del evento que será nuevamente punto de encuentro para que México y el mundo conozcan la oportunidad de negocios que ofrece el Campo Zacatecano</p>
                <p class="g-font-size-14 mb-0"></p>
              </div>
            </div>
          </div>
        </div>
      </section>
      <!-- End Section Content -->