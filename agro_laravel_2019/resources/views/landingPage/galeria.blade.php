 <!-- Section Content -->
 <section  class="u-bg-overlay g-bg-img-hero g-bg-darkblue-opacity-0_7--after g-py-100" style="background-image: url({{asset('/img/recursosAgro/aboutImages/bckvideo.png')}});">
        <div class="container u-bg-overlay__inner">
          <div class="text-center g-mb-70">
            
           
          </div>

          <div class="embed-responsive embed-responsive-16by9 event-video-effect">
              <iframe width="560" height="315" src="https://www.youtube.com/embed/bynwI-8Tkdw" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
          </div>
        </div>
      </section>
      <!-- End Section Content -->

       <!-- Section Content -->
       <section  class="u-bg-overlay g-bg-img-hero g-bg-darkblue-opacity-0_7--after  g-py-100" style="background-image: url({{asset('/img/recursosAgro/aboutImages/bckgalleria.png')}});">
        <div class="container u-bg-overlay__inner">
          <div class="text-center g-mb-70">
            <h2 class="text-uppercase g-line-height-1 g-font-weight-700 g-font-size-30 g-color-white g-mb-30">GALERÍA</h2>

            <p class="g-color-white">Experiencias de la Agroalimentaria Zacatecas 2018</p>
          </div>

          <div class="js-carousel text-center g-line-height-0 g-pb-50"
               data-infinite="true"
               data-autoplay="true"
               data-speed="5000"
               data-rows="2"
               data-slides-show="6"
               data-arrows-classes="u-arrow-v1 g-absolute-centered--y g-bottom-0 g-width-40 g-height-40 g-font-size-20 g-color-white g-color-primary--hover g-bg-primary g-bg-white--hover g-mt-minus-25 g-transition-0_2 g-transition--ease-in"
               data-arrow-left-classes="fa fa-angle-left g-left-0"
               data-arrow-right-classes="fa fa-angle-right g-right-0"
               data-pagi-classes="u-carousel-indicators-v1--white g-absolute-centered--x g-bottom-0"
               data-responsive='[{
                 "breakpoint": 1200,
                 "settings": {
                   "slidesToShow": 5
                 }
               }, {
                 "breakpoint": 992,
                 "settings": {
                   "slidesToShow": 4
                 }
               }, {
                 "breakpoint": 768,
                 "settings": {
                   "slidesToShow": 3
                 }
               }, {
                 "breakpoint": 576,
                 "settings": {
                   "slidesToShow": 2
                 }
               }]'>
            <a class="js-slide u-bg-overlay g-parent g-overflow-hidden " href="{{asset('/img-temp/400x269/img1.jpg')}}"
               data-fancybox="fancyGallery1">
              <img class="img-fluid g-transform-scale-1_1--parent-hover g-transition-0_3 g-transition--ease-in" src="{{asset('')}}/img-temp/200x200/img1.jpg" alt="Image description">
            </a>

            <a class="js-slide u-bg-overlay g-parent g-overflow-hidden " href="{{asset('/img-temp/400x269/img3.jpg')}}"
               data-fancybox="fancyGallery1">
              <img class="img-fluid g-transform-scale-1_1--parent-hover g-transition-0_3 g-transition--ease-in" src="{{asset('')}}/img-temp/200x200/img3.jpg" alt="Image description">
            </a>

            <a class="js-slide u-bg-overlay g-parent g-overflow-hidden " href="{{asset('/img-temp/400x269/img2.jpg')}}"
               data-fancybox="fancyGallery1">
              <img class="img-fluid g-transform-scale-1_1--parent-hover g-transition-0_3 g-transition--ease-in" src="{{asset('')}}/img-temp/200x200/img2.jpg" alt="Image description">
            </a>

            <a class="js-slide u-bg-overlay g-parent g-overflow-hidden " href="{{asset('/img-temp/400x269/img4.jpg')}}"
               data-fancybox="fancyGallery1">
              <img class="img-fluid g-transform-scale-1_1--parent-hover g-transition-0_3 g-transition--ease-in" src="{{asset('/img-temp/200x200/img4.jpg')}}" alt="Image description">
            </a>

            <a class="js-slide u-bg-overlay g-parent g-overflow-hidden " href="{{asset('')}}/img-temp/400x269/img1.jpg"
               data-fancybox="fancyGallery1">
              <img class="img-fluid g-transform-scale-1_1--parent-hover g-transition-0_3 g-transition--ease-in" src="{{asset('/img-temp/200x200/img1.jpg')}}" alt="Image description">
            </a>

            <a class="js-slide u-bg-overlay g-parent g-overflow-hidden " href="{{asset('')}}/img-temp/400x269/img3.jpg"
               data-fancybox="fancyGallery1">
              <img class="img-fluid g-transform-scale-1_1--parent-hover g-transition-0_3 g-transition--ease-in" src="{{asset('/img-temp/200x200/img3.jpg')}}" alt="Image description">
            </a>

            <a class="js-slide u-bg-overlay g-parent g-overflow-hidden " href="{{asset('')}}/img-temp/400x269/img2.jpg"
               data-fancybox="fancyGallery1">
              <img class="img-fluid g-transform-scale-1_1--parent-hover g-transition-0_3 g-transition--ease-in" src="{{asset('/img-temp/200x200/img2.jpg')}}" alt="Image description">
            </a>

            <a class="js-slide u-bg-overlay g-parent g-overflow-hidden " href="{{asset('')}}/img-temp/400x269/img4.jpg"
               data-fancybox="fancyGallery1">
              <img class="img-fluid g-transform-scale-1_1--parent-hover g-transition-0_3 g-transition--ease-in" src="{{asset('')}}/img-temp/200x200/img4.jpg" alt="Image description">
            </a>

            <a class="js-slide u-bg-overlay g-parent g-overflow-hidden " href="{{asset('')}}/img-temp/400x269/img1.jpg"
               data-fancybox="fancyGallery1">
              <img class="img-fluid g-transform-scale-1_1--parent-hover g-transition-0_3 g-transition--ease-in" src="{{asset('')}}/img-temp/200x200/img1.jpg" alt="Image description">
            </a>

            <a class="js-slide u-bg-overlay g-parent g-overflow-hidden " href="{{asset('')}}/img-temp/400x269/img3.jpg"
               data-fancybox="fancyGallery1">
              <img class="img-fluid g-transform-scale-1_1--parent-hover g-transition-0_3 g-transition--ease-in" src="{{asset('')}}/img-temp/200x200/img3.jpg" alt="Image description">
            </a>

            <a class="js-slide u-bg-overlay g-parent g-overflow-hidden " href="{{asset('')}}/img-temp/400x269/img2.jpg"
               data-fancybox="fancyGallery1">
              <img class="img-fluid g-transform-scale-1_1--parent-hover g-transition-0_3 g-transition--ease-in" src="{{asset('')}}/img-temp/200x200/img2.jpg" alt="Image description">
            </a>

            <a class="js-slide u-bg-overlay g-parent g-overflow-hidden " href="{{asset('')}}/img-temp/400x269/img4.jpg"
               data-fancybox="fancyGallery1">
              <img class="img-fluid g-transform-scale-1_1--parent-hover g-transition-0_3 g-transition--ease-in" src="{{asset('')}}/img-temp/200x200/img4.jpg" alt="Image description">
            </a>

            <a class="js-slide u-bg-overlay g-parent g-overflow-hidden " href="{{asset('')}}/img-temp/400x269/img1.jpg"
               data-fancybox="fancyGallery1">
              <img class="img-fluid g-transform-scale-1_1--parent-hover g-transition-0_3 g-transition--ease-in" src="{{asset('')}}/img-temp/200x200/img1.jpg" alt="Image description">
            </a>

            <a class="js-slide u-bg-overlay g-parent g-overflow-hidden " href="{{asset('')}}/img-temp/400x269/img3.jpg"
               data-fancybox="fancyGallery1">
              <img class="img-fluid g-transform-scale-1_1--parent-hover g-transition-0_3 g-transition--ease-in" src="{{asset('')}}/img-temp/200x200/img3.jpg" alt="Image description">
            </a>

            <a class="js-slide u-bg-overlay g-parent g-overflow-hidden " href="{{asset('')}}/img-temp/400x269/img2.jpg"
               data-fancybox="fancyGallery1">
              <img class="img-fluid g-transform-scale-1_1--parent-hover g-transition-0_3 g-transition--ease-in" src="{{asset('')}}/img-temp/200x200/img2.jpg" alt="Image description">
            </a>

            <a class="js-slide u-bg-overlay g-parent g-overflow-hidden " href="{{asset('')}}/img-temp/400x269/img4.jpg"
               data-fancybox="fancyGallery1">
              <img class="img-fluid g-transform-scale-1_1--parent-hover g-transition-0_3 g-transition--ease-in" src="{{asset('')}}/img-temp/200x200/img4.jpg" alt="Image description">
            </a>
          </div>
        </div>
      </section>
      <!-- End Section Content -->