 <!-- Header v1 -->
 <header id="js-header" class="u-header u-header--sticky-top u-header--change-appearance g-z-index-9999"
        data-header-fix-moment="100">
    <div class="light-theme u-header__section g-bg-darkblue-opacity-0_7 g-transition-0_3 g-py-10"
        data-header-fix-moment-exclude="light-theme g-bg-darkblue-opacity-0_7"
        data-header-fix-moment-classes="dark-theme u-shadow-v18 g-bg-white-opacity-0_9">
    <nav class="navbar navbar-expand-lg g-py-0">
        <div class="container g-pos-rel">
        <!-- Logo -->
        <a href="#" class="js-go-to navbar-brand u-header__logo"
            data-type="static">
            <img class="u-header__logo-img u-header__logo-img--main d-block" src="{{asset('/img/recursosAgro/agroLogoInicio.png')}}" alt="Image description"
                data-header-fix-moment-exclude="d-block"
                data-header-fix-moment-classes="d-none"
                style="height: 150; width: 60;">

            <img class="u-header__logo-img u-header__logo-img--main d-none" src="{{asset('/img/recursosAgro/agroLogoInicio.png')}}" alt="Image description"
                data-header-fix-moment-exclude="d-none"
                data-header-fix-moment-classes="d-block">
        </a>
        <!-- End Logo -->

        <!-- Navigation -->
        <div class="collapse navbar-collapse align-items-center flex-sm-row" id="navBar" data-mobile-scroll-hide="true">
            <ul id="js-scroll-nav" class="navbar-nav text-uppercase g-font-weight-700 g-font-size-11 g-pt-20 g-pt-5--lg ml-auto">
            <li class="nav-item g-mr-15--lg g-mb-7 g-mb-0--lg active">
                <a href="#home" class="nav-link p-0">INICIO <span class="sr-only">(current)</span></a>
            </li>
            <li class="nav-item g-mx-15--lg g-mb-7 g-mb-0--lg">
                <a href="#ponentes" class="nav-link p-0">PONENTES</a>
            </li>
            <li class="nav-item g-mx-15--lg g-mb-7 g-mb-0--lg">
                <a href="#schedule" class="nav-link p-0">PROGRAMA DEL EVENTO</a>
            </li>
            <li class="nav-item g-mx-15--lg g-mb-7 g-mb-0--lg">
                <a href="#expositores" class="nav-link p-0">EXPOSITORES</a>
            </li>
            <li class="nav-item g-mx-15--lg g-mb-7 g-mb-0--lg">
                <a href="#sponsors" class="nav-link p-0">PATROCINADORES Y ALIADOS</a>
            </li>
            <li class="nav-item g-mx-15--lg g-mb-7 g-mb-0--lg">
                <a href="#registro" class="nav-link p-0">REGISTRO DE PARTICIPANTES</a>
                </li>
            <li class="nav-item g-mx-15--lg g-mb-7 g-mb-0--lg">
                <a href="#contact" class="nav-link p-0">CONTACTO</a>
            </li>
            </ul>
        </div>
        <!-- End Navigation -->

        <!-- Responsive Toggle Button -->
        <button class="navbar-toggler btn g-line-height-1 g-brd-none g-pa-0 g-pos-abs g-top-15 g-right-0" type="button"
                aria-label="Toggle navigation"
                aria-expanded="false"
                aria-controls="navBar"
                data-toggle="collapse"
                data-target="#navBar">
            <span class="hamburger hamburger--slider">
            <span class="hamburger-box">
                <span class="hamburger-inner"></span>
            </span>
            </span>
        </button>
        <!-- End Responsive Toggle Button -->
        </div>
    </nav>
    </div>
</header>
<!-- End Header v1 -->