  <!-- Section Content -->
  <section id="sponsors" class="u-bg-overlay g-bg-img-hero  g-py-100" style="background-image: url({{asset('/img/recursosAgro/bgblanco.png')}});">
          <div class="container u-bg-overlay__inner">
           
            <div class="js-carousel text-center g-line-height-0 g-pb-50"
                 data-infinite="true"
                 data-autoplay="true"
                 data-speed="5000"
                 data-rows="2"
                 data-slides-show="6"
                 data-arrows-classes="u-arrow-v1 g-absolute-centered--y g-bottom-0 g-width-40 g-height-40 g-font-size-20 g-color-white g-color-primary--hover g-bg-primary g-bg-white--hover g-mt-minus-25 g-transition-0_2 g-transition--ease-in"
                 data-arrow-left-classes="fa fa-angle-left g-left-0"
                 data-arrow-right-classes="fa fa-angle-right g-right-0"
                 data-pagi-classes="u-carousel-indicators-v1--white g-absolute-centered--x g-bottom-0"
                 data-responsive='[{
                   "breakpoint": 1200,
                   "settings": {
                     "slidesToShow": 5
                   }
                 }, {
                   "breakpoint": 992,
                   "settings": {
                     "slidesToShow": 4
                   }
                 }, {
                   "breakpoint": 768,
                   "settings": {
                     "slidesToShow": 3
                   }
                 }, {
                   "breakpoint": 576,
                   "settings": {
                     "slidesToShow": 2
                   }
                 }]'>
              <a class="js-slide u-bg-overlay g-parent g-overflow-hidden " href="{{asset('')}}/img/recursosAgro/patrocinadores/dependencias/dep11.png"
                 data-fancybox="fancyGallery1">
                <img class="img-fluid g-transform-scale-1_1--parent-hover g-transition-0_3 g-transition--ease-in" src="{{asset('')}}/img/recursosAgro/patrocinadores/dependencias/dep1.png" alt="Image description">
              </a>
  
            
             
            </div>
          </div>
          <div class="container u-bg-overlay__inner">
           
            <div class="js-carousel text-center g-line-height-0 g-pb-50"
                 data-infinite="true"
                 data-autoplay="true"
                 data-speed="5000"
                 data-rows="2"
                 data-slides-show="6"
                 data-arrows-classes="u-arrow-v1 g-absolute-centered--y g-bottom-0 g-width-40 g-height-40 g-font-size-20 g-color-white g-color-primary--hover g-bg-primary g-bg-white--hover g-mt-minus-25 g-transition-0_2 g-transition--ease-in"
                 data-arrow-left-classes="fa fa-angle-left g-left-0"
                 data-arrow-right-classes="fa fa-angle-right g-right-0"
                 data-pagi-classes="u-carousel-indicators-v1--white g-absolute-centered--x g-bottom-0"
                 data-responsive='[{
                   "breakpoint": 1200,
                   "settings": {
                     "slidesToShow": 5
                   }
                 }, {
                   "breakpoint": 992,
                   "settings": {
                     "slidesToShow": 4
                   }
                 }, {
                   "breakpoint": 768,
                   "settings": {
                     "slidesToShow": 3
                   }
                 }, {
                   "breakpoint": 576,
                   "settings": {
                     "slidesToShow": 2
                   }
                 }]'>
           
              <a class="js-slide u-bg-overlay g-parent g-overflow-hidden" href="{{asset('')}}/img/recursosAgro/patrocinadores/dependencias/dep22.png"
                 data-fancybox="fancyGallery1">
                <img class="img-fluid g-transform-scale-1_1--parent-hover g-transition-0_3 g-transition--ease-in" src="{{asset('')}}/img/recursosAgro/patrocinadores/dependencias/dep2.png" alt="Image description">
              </a>
  
              <a class="js-slide u-bg-overlay g-parent g-overflow-hidden" href="{{asset('')}}/img/recursosAgro/patrocinadores/dependencias/dep33.png"
                 data-fancybox="fancyGallery1">
                <img class="img-fluid g-transform-scale-1_1--parent-hover g-transition-0_3 g-transition--ease-in" src="{{asset('')}}/img/recursosAgro/patrocinadores/dependencias/dep3.png" alt="Image description">
              </a>
  
              <a class="js-slide u-bg-overlay g-parent g-overflow-hidden" href="{{asset('')}}/img/recursosAgro/patrocinadores/dependencias/dep4.png"
                 data-fancybox="fancyGallery1">
                <img class="img-fluid g-transform-scale-1_1--parent-hover g-transition-0_3 g-transition--ease-in" src="{{asset('')}}/img/recursosAgro/patrocinadores/dependencias/dep44.png" alt="Image description">
              </a>
  
             
            </div>
          </div>
        </section>
        <!-- End Section Content -->

          <!-- Section Content -->
          <section  class="u-bg-overlay g-bg-img-hero  g-py-100" style="background-image: url({{asset('')}}/img/recursosAgro/bgblanco.png);">
            <div class="container u-bg-overlay__inner">
              <div class="text-center g-mb-70">
                <h2 class="text-uppercase g-line-height-1 g-font-weight-700 g-font-size-30 g-color-white g-mb-30">Patrocinadores Corporativos</h2>
    
                
              </div>
    
              <div class="js-carousel text-center g-line-height-0 g-pb-50"
                   data-infinite="true"
                   data-autoplay="true"
                   data-speed="5000"
                   data-rows="2"
                   data-slides-show="6"
                   data-arrows-classes="u-arrow-v1 g-absolute-centered--y g-bottom-0 g-width-40 g-height-40 g-font-size-20 g-color-white g-color-primary--hover g-bg-primary g-bg-white--hover g-mt-minus-25 g-transition-0_2 g-transition--ease-in"
                   data-arrow-left-classes="fa fa-angle-left g-left-0"
                   data-arrow-right-classes="fa fa-angle-right g-right-0"
                   data-pagi-classes="u-carousel-indicators-v1--white g-absolute-centered--x g-bottom-0"
                   data-responsive='[{
                     "breakpoint": 1200,
                     "settings": {
                       "slidesToShow": 5
                     }
                   }, {
                     "breakpoint": 992,
                     "settings": {
                       "slidesToShow": 4
                     }
                   }, {
                     "breakpoint": 768,
                     "settings": {
                       "slidesToShow": 3
                     }
                   }, {
                     "breakpoint": 576,
                     "settings": {
                       "slidesToShow": 2
                     }
                   }]'>
                <a class="js-slide u-bg-overlay g-parent g-overflow-hidden " href="{{asset('')}}/img/recursosAgro/patrocinadores/corporativos/pat11.png"
                   data-fancybox="fancyGallery1">
                  <img class="img-fluid g-transform-scale-1_1--parent-hover g-transition-0_3 g-transition--ease-in" src="{{asset('')}}/img/recursosAgro/patrocinadores/corporativos/pat1.png" alt="Image description">
                </a>
    
                <a class="js-slide u-bg-overlay g-parent g-overflow-hidden " href="{{asset('')}}/img/recursosAgro/patrocinadores/corporativos/pat2.png"
                   data-fancybox="fancyGallery1">
                  <img class="img-fluid g-transform-scale-1_1--parent-hover g-transition-0_3 g-transition--ease-in" src="{{asset('')}}/img/recursosAgro/patrocinadores/corporativos/pat22.png" alt="Image description">
                </a>
    
                <a class="js-slide u-bg-overlay g-parent g-overflow-hidden " href="{{asset('')}}/img/recursosAgro/patrocinadores/corporativos/pat33.png"
                   data-fancybox="fancyGallery1">
                  <img class="img-fluid g-transform-scale-1_1--parent-hover g-transition-0_3 g-transition--ease-in" src="{{asset('')}}/img/recursosAgro/patrocinadores/corporativos/pat3.png" alt="Image description">
                </a>
    
                <a class="js-slide u-bg-overlay g-parent g-overflow-hidden " href="{{asset('')}}/img/recursosAgro/patrocinadores/corporativos/pat44.png"
                   data-fancybox="fancyGallery1">
                  <img class="img-fluid g-transform-scale-1_1--parent-hover g-transition-0_3 g-transition--ease-in" src="{{asset('')}}/img/recursosAgro/patrocinadores/corporativos/pat4.png" alt="Image description">
                </a>
    
                <a class="js-slide u-bg-overlay g-parent g-overflow-hidden " href="{{asset('')}}/img/recursosAgro/patrocinadores/corporativos/pat55.png"
                   data-fancybox="fancyGallery1">
                  <img class="img-fluid g-transform-scale-1_1--parent-hover g-transition-0_3 g-transition--ease-in" src="{{asset('')}}/img/recursosAgro/patrocinadores/corporativos/pat5.png" alt="Image description">
                </a>
    
                <a class="js-slide u-bg-overlay g-parent g-overflow-hidden " href="{{asset('')}}/img/recursosAgro/patrocinadores/corporativos/pat66.png"
                   data-fancybox="fancyGallery1">
                  <img class="img-fluid g-transform-scale-1_1--parent-hover g-transition-0_3 g-transition--ease-in" src="{{asset('')}}/img/recursosAgro/patrocinadores/corporativos/pat6.png" alt="Image description">
                </a>
    
                <a class="js-slide u-bg-overlay g-parent g-overflow-hidden " href="{{asset('')}}/img/recursosAgro/patrocinadores/corporativos/pat77.png"
                   data-fancybox="fancyGallery1">
                  <img class="img-fluid g-transform-scale-1_1--parent-hover g-transition-0_3 g-transition--ease-in" src="{{asset('')}}/img/recursosAgro/patrocinadores/corporativos/pat7.png" alt="Image description">
                </a>
    
                <a class="js-slide u-bg-overlay g-parent g-overflow-hidden " href="{{asset('')}}/img/recursosAgro/patrocinadores/corporativos/pat88.png"
                   data-fancybox="fancyGallery1">
                  <img class="img-fluid g-transform-scale-1_1--parent-hover g-transition-0_3 g-transition--ease-in" src="{{asset('')}}/img/recursosAgro/patrocinadores/corporativos/pat8.png" alt="Image description">
                </a>
    
                <a class="js-slide u-bg-overlay g-parent g-overflow-hidden " href="{{asset('')}}/img/recursosAgro/patrocinadores/corporativos/pat99.png"
                   data-fancybox="fancyGallery1">
                  <img class="img-fluid g-transform-scale-1_1--parent-hover g-transition-0_3 g-transition--ease-in" src="{{asset('')}}/img/recursosAgro/patrocinadores/corporativos/pat9.png" alt="Image description">
                </a>
    
               
              </div>
            </div>
          </section>
          <!-- End Section Content -->

          <!-- Section Content -->
          <section  class="u-bg-overlay g-bg-img-hero  g-py-100" style="background-image: url({{asset('/img/recursosAgro/bgblanco.png')}});">
            <div class="container u-bg-overlay__inner">
              <div class="text-center g-mb-70">
                <h2 class="text-uppercase g-line-height-1 g-font-weight-700 g-font-size-30 g-color-white g-mb-30">Invitados Especiales</h2>
    
             
              </div>
    
              <div class="js-carousel text-center g-line-height-0 g-pb-50"
                   data-infinite="true"
                   data-autoplay="true"
                   data-speed="5000"
                   data-rows="2"
                   data-slides-show="6"
                   data-arrows-classes="u-arrow-v1 g-absolute-centered--y g-bottom-0 g-width-40 g-height-40 g-font-size-20 g-color-white g-color-primary--hover g-bg-primary g-bg-white--hover g-mt-minus-25 g-transition-0_2 g-transition--ease-in"
                   data-arrow-left-classes="fa fa-angle-left g-left-0"
                   data-arrow-right-classes="fa fa-angle-right g-right-0"
                   data-pagi-classes="u-carousel-indicators-v1--white g-absolute-centered--x g-bottom-0"
                   data-responsive='[{
                     "breakpoint": 1200,
                     "settings": {
                       "slidesToShow": 5
                     }
                   }, {
                     "breakpoint": 992,
                     "settings": {
                       "slidesToShow": 4
                     }
                   }, {
                     "breakpoint": 768,
                     "settings": {
                       "slidesToShow": 3
                     }
                   }, {
                     "breakpoint": 576,
                     "settings": {
                       "slidesToShow": 2
                     }
                   }]'>
                <a class="js-slide u-bg-overlay g-parent g-overflow-hidden " href="{{asset('')}}/img/recursosAgro/patrocinadores/invitados/inv11.png"
                   data-fancybox="fancyGallery1">
                  <img class="img-fluid g-transform-scale-1_1--parent-hover g-transition-0_3 g-transition--ease-in" src="{{asset('')}}/img/recursosAgro/patrocinadores/invitados/inv1.png" alt="Image description">
                </a>
    
                <a class="js-slide u-bg-overlay g-parent g-overflow-hidden " href="{{asset('')}}/img/recursosAgro/patrocinadores/invitados/inv22.png"
                   data-fancybox="fancyGallery1">
                  <img class="img-fluid g-transform-scale-1_1--parent-hover g-transition-0_3 g-transition--ease-in" src="{{asset('')}}/img/recursosAgro/patrocinadores/invitados/inv2.png" alt="Image description">
                </a>
    
               
              </div>
            </div>
          </section>
          <!-- End Section Content -->