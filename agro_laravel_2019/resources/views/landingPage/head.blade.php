<!DOCTYPE html>
<html lang="en">
  <head>
    <!-- Title -->
    <title>Agroalimentaria Zacatecas 2019</title>

    <!-- Required Meta Tags Always Come First -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta http-equiv="x-ua-compatible" content="ie=edge">

    <!-- Favicon -->
    <link rel="shortcut icon" href="{{asset('/img/recursosAgro/favicon.png')}}">

    <!-- Google Fonts -->
    <link rel="stylesheet" href="//fonts.googleapis.com/css?family=Open+Sans:400,300,500,600,700,800">

    <!-- CSS Global Compulsory -->
    <link rel="stylesheet" href="{{asset('/vendor/bootstrap/bootstrap.min.css')}}">

    <!-- CSS Implementing Plugins -->
    <link rel="stylesheet" href="{{asset('/vendor/icon-awesome/css/font-awesome.min.css')}}">
    <link rel="stylesheet" href="{{asset('/vendor/icon-line/css/simple-line-icons.css')}}">
    <link rel="stylesheet" href="{{asset('/vendor/icon-hs/style.css')}}">
    <link rel="stylesheet" href="{{asset('/vendor/hamburgers/hamburgers.min.css')}}">
    <link rel="stylesheet" href="{{asset('/vendor/animate.css')}}">
    <link rel="stylesheet" href="{{asset('/vendor/slick-carousel/slick/slick.css')}}">
    <link rel="stylesheet" href="{{asset('/vendor/fancybox/jquery.fancybox.css')}}">
    <link  rel="stylesheet" href="{{asset('/vendor/jquery-ui/themes/base/jquery-ui.min.css')}}">

      <!-- CSS Implementing Plugins -->
      <link  rel="stylesheet" href="{{asset('/vendor/dzsparallaxer/dzsparallaxer.css')}}">
      <link  rel="stylesheet" href="{{asset('/vendor/dzsparallaxer/dzsscroller/scroller.css')}}">
      <link  rel="stylesheet" href="{{asset('/vendor/dzsparallaxer/advancedscroller/plugin.css')}}">
    

    <!-- CSS Template -->
    <link rel="stylesheet" href="{{asset('/css/styles.op-event.css')}}">

    <!-- CSS Customization -->
    <link rel="stylesheet" href="{{asset('/css/custom.css')}}">
  </head>

  <body>
    <main>
        