<section id="ponentes" class=" g-py-100">
        <div class="text-center g-mb-70">
            <h2 class="text-uppercase g-line-height-1 g-font-weight-700 g-font-size-30 g-mb-30">PONENTES</h2>
        </div>
      
        <!-- Team Block -->
        <div class="row">
          <div class="col-sm-4 g-mb-60">
            <!-- Figure -->
            <figure class="text-center">
              <!-- Figure Image -->
              <div class="d-block mx-auto rounded-circle g-max-width-200 g-bg-white g-pa-5 g-mb-15">
                <img class="rounded-circle g-max-width-190" src="{{asset('/img/recursosAgro/conferencistas/victor_villalobos.png')}}" alt="Image Description">
              </div>
              <!-- End Figure Image -->

              <!-- Figure Info -->
              <div class="g-mb-15">
                <h4 class="h4 g-color-black g-mb-5">Dr. Víctor Manuel Villalobos Arámbula</h4>
                <em class="d-block g-font-style-normal g-font-size-11 text-uppercase g-color-primary">Secretario de Agricultura y Desarrollo Rural.
                </em>
              </div>
            
            </figure>
            <!-- End Figure -->
          </div>

          <div class="col-sm-4 g-mb-60">
            <!-- Figure -->
            <figure class="text-center">
              <!-- Figure Image -->
              <div class="d-block mx-auto rounded-circle g-max-width-200 g-bg-white g-pa-5 g-mb-15">
                <img class="rounded-circle g-max-width-190" src="{{asset('/img/recursosAgro/conferencistas/bosco.png')}}" alt="Image Description">
              </div>
              <!-- End Figure Image -->

              <!-- Figure Info -->
              <div class="g-mb-15">
                <h4 class="h4 g-color-black g-mb-5">Ing. Bosco de la Vega Valladolid
                </h4>
                <em class="g-font-style-normal g-font-size-11 text-uppercase g-color-primary">Presidente del Consejo Nacional Agropecuario.
                </em>
              </div>
            
            </figure>
            <!-- End Figure -->
          </div>

          <div class="col-sm-4 g-mb-60">
            <!-- Figure -->
            <figure class="text-center">
              <!-- Figure Image -->
              <div class="d-block mx-auto rounded-circle g-max-width-200 g-bg-white g-pa-5 g-mb-15">
                <img class="rounded-circle g-max-width-190" src="{{asset('/img/recursosAgro/conferencistas/bram.png')}}" alt="Image Description">
              </div>
              <!-- End Figure Image -->

              <!-- Figure Info -->
              <div class="g-mb-15">
                <h4 class="h4 g-color-black g-mb-5">Dr. Bram Govaerts</h4>
                <em class="d-block g-font-style-normal g-font-size-11 text-uppercase g-color-primary">Director Global de Innovación Estratégica, CIMMYT</em>
              </div>
            
            </figure>
            <!-- End Figure -->
          </div>

          <div class="col-sm-4 g-mb-60">
            <!-- Figure -->
            <figure class="text-center">
              <!-- Figure Image -->
              <div class="d-block mx-auto rounded-circle g-max-width-200 g-bg-white g-pa-5 g-mb-15">
                <img class="rounded-circle g-max-width-190" src="{{asset('/img/recursosAgro/conferencistas/javierDelgado.png')}}" alt="Image Description">
              </div>
              <!-- End Figure Image -->

              <!-- Figure Info -->
              <div class="g-mb-15">
                <h4 class="h4 g-color-black g-mb-5">Dr. Javier Delgado Mendoza</h4>
                <em class="d-block g-font-style-normal g-font-size-11 text-uppercase g-color-primary">Director General de FND                </em>
              </div>
            
            </figure>
            <!-- End Figure -->
          </div>

          <div class="col-sm-4 g-mb-60">
            <!-- Figure -->
            <figure class="text-center">
              <!-- Figure Image -->
              <div class="d-block mx-auto rounded-circle g-max-width-200 g-bg-white g-pa-5 g-mb-15">
                <img class="rounded-circle g-max-width-190" src="{{asset('/img/recursosAgro/conferencistas/jesusAlan.png')}}" alt="Image Description">
              </div>
              <!-- End Figure Image -->

              <!-- Figure Info -->
              <div class="g-mb-15">
                <h4 class="h4 g-color-black g-mb-5">Act. Jesús Alan Elizondo Flores                </h4>
                <em class="d-block g-font-style-normal g-font-size-11 text-uppercase g-color-primary">Director General de FIRA                </em>
              </div>
            
            </figure>
            <!-- End Figure -->
          </div>
          <div class="col-sm-4 g-mb-60">
            <!-- Figure -->
            <figure class="text-center">
              <!-- Figure Image -->
              <div class="d-block mx-auto rounded-circle g-max-width-200 g-bg-white g-pa-5 g-mb-15">
                <img class="rounded-circle g-max-width-190" src="{{asset('/img/recursosAgro/conferencistas/dovalina.png')}}" alt="Image Description">
              </div>
              <!-- End Figure Image -->

              <!-- Figure Info -->
              <div class="g-mb-15">
                <h4 class="h4 g-color-black g-mb-5"> Ing. José Francisco Dovalina Lara</h4>
                <em class="d-block g-font-style-normal g-font-size-11 text-uppercase g-color-primary"> Ing. José Francisco Dovalina Lara</em>
              </div>
            
            </figure>
            <!-- End Figure -->
          </div>
          <div class="col-sm-4 g-mb-60">
            <!-- Figure -->
            <figure class="text-center">
              <!-- Figure Image -->
              <div class="d-block mx-auto rounded-circle g-max-width-200 g-bg-white g-pa-5 g-mb-15">
                <img class="rounded-circle g-max-width-190" src="{{asset('/img/recursosAgro/conferencistas/francisco_trujillo.png')}}" alt="Image Description">
              </div>
              <!-- End Figure Image -->

              <!-- Figure Info -->
              <div class="g-mb-15">
                <h4 class="h4 g-color-black g-mb-5">Dr. Francisco Javier Trujillo Arriaga</h4>
                <em class="d-block g-font-style-normal g-font-size-11 text-uppercase g-color-primary">Director en Jefe SENASICA</em>
              </div>
            
            </figure>
            <!-- End Figure -->
          </div>

          <div class="col-sm-4 g-mb-60">
            <!-- Figure -->
            <figure class="text-center">
              <!-- Figure Image -->
              <div class="d-block mx-auto rounded-circle g-max-width-200 g-bg-white g-pa-5 g-mb-15">
                <img class="rounded-circle g-max-width-190" src="{{asset('/img/recursosAgro/conferencistas/raviv.png')}}" alt="Image Description">
              </div>
              <!-- End Figure Image -->

              <!-- Figure Info -->
              <div class="g-mb-15">
                <h4 class="h4 g-color-black g-mb-5">Uriel Raviv</h4>
                <em class="d-block g-font-style-normal g-font-size-11 text-uppercase g-color-primary">Consejero Económico en México, Embajada de Israel</em>
              </div>
            
            </figure>
            <!-- End Figure -->
          </div>

          <div class="col-sm-4 g-mb-60">
            <!-- Figure -->
            <figure class="text-center">
              <!-- Figure Image -->
              <div class="d-block mx-auto rounded-circle g-max-width-200 g-bg-white g-pa-5 g-mb-15">
                <img class="rounded-circle g-max-width-190" src="{{asset('/img/recursosAgro/conferencistas/manuelEsteban.png')}}" alt="Image Description">
              </div>
              <!-- End Figure Image -->

              <!-- Figure Info -->
              <div class="g-mb-15">
                <h4 class="h4 g-color-black g-mb-5">Manuel Esteban Tarriba Urtuzuástegui</h4>
                <em class="d-block g-font-style-normal g-font-size-11 text-uppercase g-color-primary">Secretario de Agricultura y Ganadería de Sinaloa</em>
              </div>
            
            </figure>
            <!-- End Figure -->
          </div>

          <div class="col-sm-4 g-mb-60">
            <!-- Figure -->
            <figure class="text-center">
              <!-- Figure Image -->
              <div class="d-block mx-auto rounded-circle g-max-width-200 g-bg-white g-pa-5 g-mb-15">
                <img class="rounded-circle g-max-width-190" src="{{asset('/img/recursosAgro/conferencistas/rosarioAntonio.png')}}" alt="Image Description">
              </div>
              <!-- End Figure Image -->

              <!-- Figure Info -->
              <div class="g-mb-15">
                <h4 class="h4 g-color-black g-mb-5">Rosario Antonio Beltrán Ureta</h4>
                <em class="d-block g-font-style-normal g-font-size-11 text-uppercase g-color-primary">Presidente del Comité Nacional del Sistema Producto
                  Tomate</em>
              </div>
            
            </figure>
            <!-- End Figure -->
          </div>

          <div class="col-sm-4 g-mb-60">
            <!-- Figure -->
            <figure class="text-center">
              <!-- Figure Image -->
              <div class="d-block mx-auto rounded-circle g-max-width-200 g-bg-white g-pa-5 g-mb-15">
                <img class="rounded-circle g-max-width-190" src="{{asset('/img/recursosAgro/conferencistas/huarte.png')}}" alt="Image Description">
              </div>
              <!-- End Figure Image -->

              <!-- Figure Info -->
              <div class="g-mb-15">
                <h4 class="h4 g-color-black g-mb-5">Lic. Fernando Ruíz Huarte</h4>
                <em class="d-block g-font-style-normal g-font-size-11 text-uppercase g-color-primary">Director General COMCE Nacional</em>
              </div>
            
            </figure>
            <!-- End Figure -->
          </div>

          <div class="col-sm-4 g-mb-60">
            <!-- Figure -->
            <figure class="text-center">
              <!-- Figure Image -->
              <div class="d-block mx-auto rounded-circle g-max-width-200 g-bg-white g-pa-5 g-mb-15">
                <img class="rounded-circle g-max-width-190" src="{{asset('/img/recursosAgro/conferencistas/arturoPuente.png')}}" alt="Image Description">
              </div>
              <!-- End Figure Image -->

              <!-- Figure Info -->
              <div class="g-mb-15">
                <h4 class="h4 g-color-black g-mb-5">Dr. Arturo Puente González</h4>
                <em class="d-block g-font-style-normal g-font-size-11 text-uppercase g-color-primary">Director en Jefe de ASERCA</em>
              </div>
            
            </figure>
            <!-- End Figure -->
          </div>

          <div class="col-sm-4 g-mb-60">
            <!-- Figure -->
            <figure class="text-center">
              <!-- Figure Image -->
              <div class="d-block mx-auto rounded-circle g-max-width-200 g-bg-white g-pa-5 g-mb-15">
                <img class="rounded-circle g-max-width-190" src="{{asset('/img/recursosAgro/conferencistas/IgnacioOvalle.png')}}" alt="Image Description">
              </div>
              <!-- End Figure Image -->

              <!-- Figure Info -->
              <div class="g-mb-15">
                <h4 class="h4 g-color-black g-mb-5">Lic. Ignacio Ovalle Fernández</h4>
                <em class="d-block g-font-style-normal g-font-size-11 text-uppercase g-color-primary">Titular de Seguridad Alimentaria Mexicana</em>
              </div>
            
            </figure>
            <!-- End Figure -->
          </div>

          <div class="col-sm-4 g-mb-60">
            <!-- Figure -->
            <figure class="text-center">
              <!-- Figure Image -->
              <div class="d-block mx-auto rounded-circle g-max-width-200 g-bg-white g-pa-5 g-mb-15">
                <img class="rounded-circle g-max-width-190" src="{{asset('/img/recursosAgro/conferencistas/joseluisri.png')}}" alt="Image Description">
              </div>
              <!-- End Figure Image -->

              <!-- Figure Info -->
              <div class="g-mb-15">
                <h4 class="h4 g-color-black g-mb-5">Mtro. José Luis Rhi Sausi</h4>
                <em class="d-block g-font-style-normal g-font-size-11 text-uppercase g-color-primary">Coordinador del Foro Ítalo - Latinoamericano sobre
                  Pymes, IILA</em>
              </div>
            
            </figure>
            <!-- End Figure -->
          </div>

          <div class="col-sm-4 g-mb-60">
            <!-- Figure -->
            <figure class="text-center">
              <!-- Figure Image -->
              <div class="d-block mx-auto rounded-circle g-max-width-200 g-bg-white g-pa-5 g-mb-15">
                <img class="rounded-circle g-max-width-190" src="{{asset('/img/recursosAgro/conferencistas/luisbazan.png')}}" alt="Image Description">
              </div>
              <!-- End Figure Image -->

              <!-- Figure Info -->
              <div class="g-mb-15">
                <h4 class="h4 g-color-black g-mb-5">Luis Bazán</h4>
                <em class="d-block g-font-style-normal g-font-size-11 text-uppercase g-color-primary">Director General Puente Internacional de PHARR</em>
              </div>
            
            </figure>
            <!-- End Figure -->
          </div>

          <div class="col-sm-4 g-mb-60">
            <!-- Figure -->
            <figure class="text-center">
              <!-- Figure Image -->
              <div class="d-block mx-auto rounded-circle g-max-width-200 g-bg-white g-pa-5 g-mb-15">
                <img class="rounded-circle g-max-width-190" src="{{asset('/img/recursosAgro/conferencistas/salvadorfernandez.png')}}" alt="Image Description">
              </div>
              <!-- End Figure Image -->

              <!-- Figure Info -->
              <div class="g-mb-15">
                <h4 class="h4 g-color-black g-mb-5">Dr. Salvador Fernández Rivera</h4>
                <em class="d-block g-font-style-normal g-font-size-11 text-uppercase g-color-primary">Director en Jefe de ASERCA</em>
              </div>
            
            </figure>
            <!-- End Figure -->
          </div>

          <div class="col-sm-4 g-mb-60">
            <!-- Figure -->
            <figure class="text-center">
              <!-- Figure Image -->
              <div class="d-block mx-auto rounded-circle g-max-width-200 g-bg-white g-pa-5 g-mb-15">
                <img class="rounded-circle g-max-width-190" src="{{asset('/img/recursosAgro/conferencistas/maluz.png')}}" alt="Image Description">
              </div>
              <!-- End Figure Image -->

              <!-- Figure Info -->
              <div class="g-mb-15">
                <h4 class="h4 g-color-black g-mb-5">Dra. Luz María de la Mora Sánchez</h4>
                <em class="d-block g-font-style-normal g-font-size-11 text-uppercase g-color-primary">Subsecretaría de Comercio Exterior, Secretaría de
                  Economía</em>
              </div>
            
            </figure>
            <!-- End Figure -->
          </div>

          <div class="col-sm-4 g-mb-60">
            <!-- Figure -->
            <figure class="text-center">
              <!-- Figure Image -->
              <div class="d-block mx-auto rounded-circle g-max-width-200 g-bg-white g-pa-5 g-mb-15">
                <img class="rounded-circle g-max-width-190" src="{{asset('/img/recursosAgro/conferencistas/rodneyOrlando.png')}}" alt="Image Description">
              </div>
              <!-- End Figure Image -->

              <!-- Figure Info -->
              <div class="g-mb-15">
                <h4 class="h4 g-color-black g-mb-5">Mtro. Rodney Orlando Cordero Salas</h4>
                <em class="d-block g-font-style-normal g-font-size-11 text-uppercase g-color-primary">Docente de Especies Zootécnicas, Escuela de Ciencias
                  Agrarias.
                  Universidad Nacional de Costa Rica</em>
              </div>
            
            </figure>
            <!-- End Figure -->
          </div>

          <div class="col-sm-4 g-mb-60">
            <!-- Figure -->
            <figure class="text-center">
              <!-- Figure Image -->
              <div class="d-block mx-auto rounded-circle g-max-width-200 g-bg-white g-pa-5 g-mb-15">
                <img class="rounded-circle g-max-width-190" src="{{asset('/img/recursosAgro/conferencistas/oreste.png')}}" alt="Image Description">
              </div>
              <!-- End Figure Image -->

              <!-- Figure Info -->
              <div class="g-mb-15">
                <h4 class="h4 g-color-black g-mb-5">C. Orestes Vasquez</h4>
                <em class="d-block g-font-style-normal g-font-size-11 text-uppercase g-color-primary">Director de la Oficina Agrocomercial del Consulado de EU
                  en Monterrey</em>
              </div>
            
            </figure>
            <!-- End Figure -->
          </div>

          <div class="col-sm-4 g-mb-60">
            <!-- Figure -->
            <figure class="text-center">
              <!-- Figure Image -->
              <div class="d-block mx-auto rounded-circle g-max-width-200 g-bg-white g-pa-5 g-mb-15">
                <img class="rounded-circle g-max-width-190" src="{{asset('/img/recursosAgro/conferencistas/humbertoMartinez.png')}}" alt="Image Description">
              </div>
              <!-- End Figure Image -->

              <!-- Figure Info -->
              <div class="g-mb-15">
                <h4 class="h4 g-color-black g-mb-5">Lic. Humberto Martínez Romero</h4>
                <em class="d-block g-font-style-normal g-font-size-11 text-uppercase g-color-primary">Director General del Grupo Rosmar</em>
              </div>
            
            </figure>
            <!-- End Figure -->
          </div>

          <div class="col-sm-4 g-mb-60">
            <!-- Figure -->
            <figure class="text-center">
              <!-- Figure Image -->
              <div class="d-block mx-auto rounded-circle g-max-width-200 g-bg-white g-pa-5 g-mb-15">
                <img class="rounded-circle g-max-width-190" src="{{asset('/img/recursosAgro/conferencistas/miller.png')}}" alt="Image Description">
              </div>
              <!-- End Figure Image -->

              <!-- Figure Info -->
              <div class="g-mb-15">
                <h4 class="h4 g-color-black g-mb-5">LAN Juan Jesús Sánchez Miller</h4>
                <em class="d-block g-font-style-normal g-font-size-11 text-uppercase g-color-primary">Director SCS Global Services México</em>
              </div>
            
            </figure>
            <!-- End Figure -->
          </div>


         
        </div>

        <div class="row">
            
          <div class="col-sm-4 g-mb-60">
            <!-- Figure -->
            <figure class="text-center">
              <!-- Figure Image -->
              <div class="d-block mx-auto rounded-circle g-max-width-200 g-bg-white g-pa-5 g-mb-15">
                <img class="rounded-circle g-max-width-190" src="{{asset('/img/recursosAgro/conferencistas/angulo.png')}}" alt="Image Description">
              </div>
              <!-- End Figure Image -->

              <!-- Figure Info -->
              <div class="g-mb-15">
                <h4 class="h4 g-color-black g-mb-5">Ing. Francisco Javier Angulo Rendón</h4>
                <em class="d-block g-font-style-normal g-font-size-11 text-uppercase g-color-primary">Gerente de Agronomía y Soporte al Producto de RivulisEurodrip</em>
              </div>
            
            </figure>
            <!-- End Figure -->
          </div>

          <div class="col-sm-4 g-mb-60">
            <!-- Figure -->
            <figure class="text-center">
              <!-- Figure Image -->
              <div class="d-block mx-auto rounded-circle g-max-width-200 g-bg-white g-pa-5 g-mb-15">
                <img class="rounded-circle g-max-width-190" src="{{asset('/img/recursosAgro/conferencistas/katia.png')}}" alt="Image Description">
              </div>
              <!-- End Figure Image -->

              <!-- Figure Info -->
              <div class="g-mb-15">
                <h4 class="h4 g-color-black g-mb-5">C. Katia A. Figueroa Rodríguez</h4>
                <em class="d-block g-font-style-normal g-font-size-11 text-uppercase g-color-primary">Profesor Investigador Titular</em>
              </div>
            
            </figure>
            <!-- End Figure -->
          </div>
        </div>
        <!-- End Team Block -->

      </section>