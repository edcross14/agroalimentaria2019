      <!-- Footer Background -->
      <div  id="contact" class="u-bg-overlay text-center g-color-white  g-bg-img-hero g-pt-100 g-pb-200" style="background-image: url({{asset('/img/recursosAgro/bannercontacto.png')}});">
  <div class="container u-bg-overlay__inner">
    <div class="u-heading-v8-2 g-mb-30">
      <h2 class="u-heading-v8__title text-uppercase g-font-weight-700 g-color-white">CONTÁCTANOS
        
      </h2>
    </div>

    <div class="lead g-font-weight-400 g-width-80x--md mx-auto g-mb-60">
    </div>

    <address class="row g-color-white-opacity-0_8">

     
      
      <div class="col-sm-6 col-md-3 g-mb-60">
        <i class="icon-line icon-map d-inline-block display-5 g-mb-25"></i>
        <h4 class="small text-uppercase g-mb-5">Dirección</h4>
        <strong>Complejo Ciudad Administrativa, Blvd Héroes de Chapultepec,
          Ciudad Argentum, 98160
          Zacatecas, Zac.</strong>
      </div>

      <div class="col-sm-6 col-md-3 g-mb-60">
        <i class="icon-call-in d-inline-block display-5 g-mb-25"></i>
        <h4 class="small text-uppercase g-mb-5">Teléfono</h4>
        <a class="g-color-white-opacity-0_8" href="tel:014789854210">
          <strong>01 (478) 985 42 10 Ext. 3239</strong></a>
      </div>

      <div class="col-sm-6 col-md-3">
        <i class="icon-envelope d-inline-block display-5 g-mb-25"></i>
        <h4 class="small text-uppercase g-mb-5">Correo Electrónico</h4>
        <a class="g-color-white-opacity-0_8" href="mailto:contacto@agroalimentariazac.mx">
          <strong>contacto@agroalimentariazac.mx</strong></a>
      </div>

      <div class="col-sm-6 col-md-3">
     
        <i class="fa fa-whatsapp  d-inline-block display-5 g-mb-25"></i>
        <h4 class="small text-uppercase g-mb-5">WhatsApp</h4>
        <a class="g-color-white-opacity-0_8" href="https://api.whatsapp.com/send?phone=5214921038159&text=%C2%A1Hola!%20Quiero%20informes%20de%20la%20Agroalimentaria%20Zacatecas%202019">
          <strong>Enviar un mensaje <i class="fa fa-paper-plane"></i></strong></a>
      </div>

     
    </address>
  </div>
</div>
<!-- End Footer Background -->

<!-- Footer Map and Form -->
<div class="container text-center g-color-white-opacity-0_8 g-mt-minus-200">
  <div class="row no-gutters g-min-height-60vh">
      <!-- Google Map -->
      <div class="col-lg-6 d-flex g-bg-gray-dark-v1 g-pa-400">
          <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3678.619662158558!2d-102.61308058464876!3d22.779491931084426!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x86824e063ac8d4c5%3A0xe3338e15a70b2688!2sPalacio+de+Convenciones+Zacatecas!5e0!3m2!1ses-419!2sus!4v1565028403061!5m2!1ses-419!2sus" width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
      </div>
      <!-- End Google Map -->

    <!-- Footer Content -->
    <div class="col-lg-6 d-flex g-bg-gray-dark-v1 g-pa-40">
      <div class="align-self-center w-100">
        <h2 class="g-color-white text-uppercase g-font-weight-700 g-mb-50"> Conoce la oportunidad de negocios que ofrece Zacatecas
        </h2>

        <form>
          <div class="form-group g-mb-10">
            <input class="form-control g-color-white g-bg-black-opacity-0_6 g-bg-black-opacity-0_4--focus g-brd-black-opacity-0_4 g-brd-primary--focus rounded-0 g-pa-15" type="tel" placeholder="Correo Electrónico">
          </div>
          <div class="form-group g-mb-10">
            <input class="form-control g-color-white g-bg-black-opacity-0_6 g-bg-black-opacity-0_4--focus g-brd-black-opacity-0_4 g-brd-primary--focus rounded-0 g-pa-15" type="text" placeholder="Asunto">
          </div>
          <div class="form-group g-mb-40">
            <textarea class="form-control g-resize-none g-color-white g-bg-black-opacity-0_6 g-bg-black-opacity-0_4--focus g-brd-black-opacity-0_4 g-brd-primary--focus rounded-0 g-pa-15" rows="5" placeholder="Mensaje"></textarea>
          </div>
          <button class="btn btn-md u-btn-primary g-font-size-12 text-uppercase rounded-0 g-py-11 g-px-30" type="submit" role="button">Enviar mensaje</button>
        </form>
      </div>
    </div>
    <!-- End Footer Content -->
  </div>
</div>
<!-- Footer Map and Form -->

<!-- Copyright -->
<footer class="text-center g-pt-60 g-pb-30">
  <div class="container">
    <a class="d-block g-mb-30" href="http://nymeria.mx/">
      <img class="img-fluid" src="{{asset('/img/nymeriam.png')}}" alt="Logo">
    </a>

    <small class="d-block g-font-size-default g-mb-20">Desarrollado por NYMERIA, 2019 Derechos Reservados 
      <a class="g-color-primary g-color-primary--hover" href="#">AGROALIMENTARIA ZACATECAS 2019</a>
    </small>

   
  </div>
</footer>
<!-- End Copyright -->

      <a class="js-go-to rounded-0 u-go-to-v1" href="#"
         data-type="fixed"
         data-position='{
           "bottom": 15,
           "right": 15
         }'
         data-offset-top="400"
         data-compensation="#js-header"
         data-show-effect="zoomIn">
        <i class="hs-icon hs-icon-arrow-top"></i>
      </a>
    </main>

    <!-- JS Global Compulsory -->
    <script src="{{asset('/vendor/jquery/jquery.min.js')}}"></script>
    <script src="{{asset('/vendor/jquery-migrate/jquery-migrate.min.js')}}"></script>
    <script src="{{asset('/vendor/popper.js/popper.min.js')}}"></script>
    <script src="{{asset('/vendor/bootstrap/bootstrap.min.js')}}"></script>

    <!-- JS Implementing Plugins -->
    <script src="{{asset('/vendor/appear.js')}}"></script>
    <script src="{{asset('/vendor/masonry/dist/masonry.pkgd.min.js')}}"></script>
    <script src="{{asset('/vendor/imagesloaded/imagesloaded.pkgd.min.js')}}"></script>
    <script src="{{asset('/vendor/jquery.countdown.min.js')}}"></script>
    <script src="{{asset('/vendor/fancybox/jquery.fancybox.js')}}"></script>
    <script src="{{asset('/vendor/slick-carousel/slick/slick.js')}}"></script>
    <script src="{{asset('/vendor/gmaps/gmaps.min.js')}}"></script>

    <!-- JS Unify -->
    <script src="{{asset('/js/hs.core.js')}}"></script>
    <script src="{{asset('/js/components/hs.header.js')}}"></script>
    <script src="{{asset('/js/helpers/hs.hamburgers.js')}}"></script>
    <script src="{{asset('/js/components/hs.scroll-nav.js')}}"></script>
    <script src="{{asset('/js/components/hs.tabs.js')}}"></script>
    <script src="{{asset('/js/components/hs.countdown.js')}}"></script>
    <script src="{{asset('/js/components/hs.carousel.js')}}"></script>
    <script src="{{asset('/js/components/gmap/hs.map.js')}}"></script>
    <script src="{{asset('/js/components/hs.go-to.js')}}"></script>

    <!-- JS Customization -->
    <script src="{{asset('/js/custom.js')}}"></script>

    <!-- JS Plugins Init. -->
    <script>
      // initialization of google map
      function initMap() {
        $.HSCore.components.HSGMap.init('.js-g-map');
      }

      $(document).on('ready', function () {
        // initialization of carousel
        $.HSCore.components.HSCarousel.init('.js-carousel');

        // initialization of header
        $.HSCore.components.HSHeader.init($('#js-header'));
        $.HSCore.helpers.HSHamburgers.init('.hamburger');

        // initialization of tabs
        $.HSCore.components.HSTabs.init('[role="tablist"]');

        // initialization of go to section
        $.HSCore.components.HSGoTo.init('.js-go-to');

        // initialization of countdowns
        var countdowns = $.HSCore.components.HSCountdown.init('.js-countdown', {
          yearsElSelector: '.js-cd-years',
          monthElSelector: '.js-cd-month',
          daysElSelector: '.js-cd-days',
          hoursElSelector: '.js-cd-hours',
          minutesElSelector: '.js-cd-minutes',
          secondsElSelector: '.js-cd-seconds'
        });

      
      });

      $(window).on('load', function() {
        // initialization of HSScrollNav
        $.HSCore.components.HSScrollNav.init($('#js-scroll-nav'), {
          duration: 700
        });

        // initialization of masonry.js
        $('.masonry-grid').imagesLoaded().then(function () {
          $('.masonry-grid').masonry({
            columnWidth: '.masonry-grid-sizer',
            itemSelector: '.masonry-grid-item',
            percentPosition: true
          });
        });
      });

      $(window).on('resize', function () {
        setTimeout(function () {
          $.HSCore.components.HSTabs.init('[role="tablist"]');
        }, 200);
      });
    </script>

     
      <!-- JS Plugins Init. -->
      <script >
        // initialization of google map
        function initMap() {
          $.HSCore.components.HSGMap.init('.js-g-map');
        }
      </script>

           <!-- JS Implementing Plugins -->
           <script  src="{{asset('/vendor/dzsparallaxer/dzsparallaxer.js')}}"></script>
           <script  src="{{asset('/vendor/dzsparallaxer/dzsscroller/scroller.js')}}"></script>
           <script  src="{{asset('/vendor/dzsparallaxer/advancedscroller/plugin.js')}}"></script>

        

         
  </body>
</html>
