<!-- Timeline -->
<section id="schedule" class="g-pt-150 g-pb-90 g-pb-170--md">
        <div class="container">
          <div class="text-center g-mb-70">
            <h2 class="text-uppercase g-line-height-1 g-font-weight-700 g-font-size-30 g-mb-30">PROGRAMA GENERAL DEL EVENTO</h2>

            <a href="{{asset('')}}/pdf/Conferencias-AGROALIMENTARIA2019.pdf" target="_blank">Descargar programa en PDF</a>

          </div>

          <!-- Nav tabs -->
          <ul class="nav justify-content-center text-uppercase u-nav-v5-3 u-nav-primary g-line-height-1_4 g-font-weight-700 g-font-size-14 g-brd-bottom--md g-brd-gray-light-v4 g-mb-80--md" role="tablist"
              data-target="days"
              data-tabs-mobile-type="slide-up-down"
              data-btn-classes="btn btn-md btn-block text-uppercase rounded-0 g-font-weight-700 u-btn-outline-primary">
            <li class="nav-item g-mx-10--md">
              <a class="nav-link g-theme-color-gray-dark-v1 g-color-primary--hover g-pb-15--md active" data-toggle="tab" href="#day-1" role="tab">JUEVES 22 DE AGOSTO</a>
            </li>
            <li class="nav-item g-mx-10--md">
              <a class="nav-link g-theme-color-gray-dark-v1 g-color-primary--hover g-pb-15--md" data-toggle="tab" href="#day-2" role="tab">VIERNES 23 DE AGOSTO</a>
            </li>
            <li class="nav-item g-mx-10--md">
              <a class="nav-link g-theme-color-gray-dark-v1 g-color-primary--hover g-pb-15--md" data-toggle="tab" href="#day-3" role="tab">SABADO 24 DE AGOSTO</a>
            </li>
          </ul>
          <!-- End Nav tabs -->

          <!-- Tab panes -->
          <div id="days" class="tab-content g-pt-20">
            <div class="tab-pane fade show active" id="day-1" role="tabpanel">
              <div class="u-timeline-v3-wrap">
                <a class="u-timeline-v3 d-block text-center text-md-left g-parent u-link-v5 g-mb-50" href="#">
                  <div class="g-hidden-sm-down u-timeline-v3__icon g-absolute-centered--y g-z-index-3 g-line-height-0 g-width-16 g-height-16 g-ml-minus-8">
                    <i class="d-inline-block g-width-16 g-height-16 g-bg-white g-brd-5 g-brd-style-solid g-brd-gray-light-v4 g-rounded-50"></i>
                  </div>

                  <div class="row mx-0">
                    <div class="col-md-3 g-order-2 g-order-1--sm d-flex align-self-center px-0">
                      <div class="u-heading-v1-4 g-brd-gray-light-v2 w-100">
                        <span class="text-center g-pos-rel d-block g-width-110 g-line-height-1_6 g-font-weight-600 g-color-white g-font-size-14 g-bg-gray-dark-v1 
                        g-bg-primary--parent-hover g-py-5 g-px-10 mx-auto g-ml-0--md g-transition-0_2 g-transition--ease-in g-mb-10 g-mb-0--md">13:00 hrs.</span>
                      </div>
                    </div>

                    <div class="col-md-9 g-order-1 g-order-2--sm px-0 g-mb-15 g-mb-0--md">
                      <div class="media d-block d-md-flex">
                    
                        <div class="media-body align-self-center">
                          <h4 class="text-uppercase g-font-weight-700 g-font-size-12 g-color-primary g-mb-5">Conferencia Magistral - La Nueva Visión del Sector Agropecuario                          </h4>
                          <h3 class="text-uppercase g-font-weight-700 g-font-size-23 g-mb-10">Dr. Víctor Manuel Villalobos Arámbula</h3>
                          <p class="g-color-gray-dark-v5 mb-0">Secretario de Agricultura y Desarrollo Rural</p>
                        </div>
                      </div>
                    </div>
                  </div>
                </a>

                <a class="u-timeline-v3 d-block text-center text-md-left g-parent u-link-v5 g-mb-50" href="#">
                  <div class="g-hidden-sm-down u-timeline-v3__icon g-absolute-centered--y g-z-index-3 g-line-height-0 g-width-16 g-height-16 g-ml-minus-8">
                    <i class="d-inline-block g-width-16 g-height-16 g-bg-white g-brd-5 g-brd-style-solid g-brd-gray-light-v4 g-rounded-50"></i>
                  </div>

                  <div class="row mx-0">
                    <div class="col-md-3 g-order-2 g-order-1--sm d-flex align-self-center px-0">
                      <div class="u-heading-v1-4 g-brd-gray-light-v2 w-100">
                        <span class="text-center g-pos-rel d-block g-width-110 g-line-height-1_6 g-font-weight-600 g-color-white 
                        g-font-size-14 g-bg-gray-dark-v1 g-bg-primary--parent-hover g-py-5 g-px-10 mx-auto g-ml-0--md g-transition-0_2 g-transition--ease-in 
                        g-mb-10 g-mb-0--md">14:00 hrs.</span>
                      </div>
                    </div>

                    <div class="col-md-9 g-order-1 g-order-2--sm px-0 g-mb-15 g-mb-0--md">
                      <div class="media d-block d-md-flex">
                       

                        <div class="media-body align-self-center">
                          <h4 class="text-uppercase g-font-weight-700 g-font-size-12 g-color-primary g-mb-5">Panel - Nueva Visión del Financiamiento Integral en el Campo
                            Mexicano</h4>
                                <p class="g-color-gray-dark-v5 mb-0"><b>Moderador: Ing. José Francisco Dovalina Lara</b> - Presidente del Comité Agropecuario de la Asociación de Bancos
                                  de México</p>
                                <p class="g-color-gray-dark-v5 mb-0"><b>- Dr. Javier Delgado Mendoza</b> - Director General de FND</p>

                                <p class="g-color-gray-dark-v5 mb-0"><b>- Act. Jesús Alan Elizondo Flores</b> - Director General de FIRA</p>
                        </div>
                      </div>
                    </div>
                  </div>
                </a>

                <a class="u-timeline-v3 d-block text-center text-md-left g-parent u-link-v5 g-mb-50" href="#">
                  <div class="g-hidden-sm-down u-timeline-v3__icon g-absolute-centered--y g-z-index-3 g-line-height-0 g-width-16 g-height-16 g-ml-minus-8">
                    <i class="d-inline-block g-width-16 g-height-16 g-bg-white g-brd-5 g-brd-style-solid g-brd-gray-light-v4 g-rounded-50"></i>
                  </div>

                  <div class="row mx-0">
                    <div class="col-md-3 g-order-2 g-order-1--sm d-flex align-self-center px-0">
                      <div class="u-heading-v1-4 g-brd-gray-light-v2 w-100">
                        <span class="text-center g-pos-rel d-block g-width-110 g-line-height-1_6 g-font-weight-600 g-color-white g-font-size-14
                         g-bg-gray-dark-v1 g-bg-primary--parent-hover g-py-5 g-px-10 mx-auto g-ml-0--md g-transition-0_2 
                         g-transition--ease-in g-mb-10 g-mb-0--md">16:30 hrs.</span>
                      </div>
                    </div>

                    <div class="col-md-9 g-order-1 g-order-2--sm px-0 g-mb-15 g-mb-0--md">
                      <div class="media d-block d-md-flex">
                      

                        <div class="media-body align-self-center">
                          <h4 class="text-uppercase g-font-weight-700 g-font-size-12 g-color-primary g-mb-5">Conferencia - Perspectivas sobre Sanidad Animal y la Inocuidad
                            Alimentaria en México</h4>
                          <h3 class="text-uppercase g-font-weight-700 g-font-size-23 g-mb-10">Dr. Francisco Javier Trujillo Arriaga</h3>
                          <p class="g-color-gray-dark-v5 mb-0">Director en Jefe SENASICA</p>
                        </div>
                      </div>
                    </div>
                  </div>
                </a>
                <a class="u-timeline-v3 d-block text-center text-md-left g-parent u-link-v5 g-mb-50" href="#">
                  <div class="g-hidden-sm-down u-timeline-v3__icon g-absolute-centered--y g-z-index-3 g-line-height-0 g-width-16 g-height-16 g-ml-minus-8">
                    <i class="d-inline-block g-width-16 g-height-16 g-bg-white g-brd-5 g-brd-style-solid g-brd-gray-light-v4 g-rounded-50"></i>
                  </div>

                  <div class="row mx-0">
                    <div class="col-md-3 g-order-2 g-order-1--sm d-flex align-self-center px-0">
                      <div class="u-heading-v1-4 g-brd-gray-light-v2 w-100">
                        <span class="text-center g-pos-rel d-block g-width-110 g-line-height-1_6 g-font-weight-600 g-color-white g-font-size-14 g-bg-gray-dark-v1
                         g-bg-primary--parent-hover g-py-5 g-px-10 mx-auto g-ml-0--md g-transition-0_2 g-transition--ease-in g-mb-10 g-mb-0--md">17:30 hrs.</span>
                      </div>
                    </div>

                    <div class="col-md-9 g-order-1 g-order-2--sm px-0 g-mb-15 g-mb-0--md">
                      <div class="media d-block d-md-flex">
                     

                        <div class="media-body align-self-center">
                          <h4 class="text-uppercase g-font-weight-700 g-font-size-12 g-color-primary g-mb-5">Conferencia - Innovación Israelí, Revolucionando el Campo</h4>
                          <h3 class="text-uppercase g-font-weight-700 g-font-size-23 g-mb-10">Uriel Raviv</h3>
                          <p class="g-color-gray-dark-v5 mb-0">Consejero Económico en México, Embajada de Israel</p>
                        </div>
                      </div>
                    </div>
                  </div>
                </a>
                <a class="u-timeline-v3 d-block text-center text-md-left g-parent u-link-v5 g-mb-50" href="#">
                  <div class="g-hidden-sm-down u-timeline-v3__icon g-absolute-centered--y g-z-index-3 g-line-height-0 g-width-16 g-height-16 g-ml-minus-8">
                    <i class="d-inline-block g-width-16 g-height-16 g-bg-white g-brd-5 g-brd-style-solid g-brd-gray-light-v4 g-rounded-50"></i>
                  </div>

                  <div class="row mx-0">
                    <div class="col-md-3 g-order-2 g-order-1--sm d-flex align-self-center px-0">
                      <div class="u-heading-v1-4 g-brd-gray-light-v2 w-100">
                        <span class="text-center g-pos-rel d-block g-width-110 g-line-height-1_6 g-font-weight-600 g-color-white g-font-size-14 g-bg-gray-dark-v1 
                        g-bg-primary--parent-hover g-py-5 g-px-10 mx-auto g-ml-0--md g-transition-0_2 g-transition--ease-in g-mb-10 g-mb-0--md">18:30 hrs.</span>
                      </div>
                    </div>

                    <div class="col-md-9 g-order-1 g-order-2--sm px-0 g-mb-15 g-mb-0--md">
                      <div class="media d-block d-md-flex">
                        

                        <div class="media-body align-self-center">
                          <h4 class="text-uppercase g-font-weight-700 g-font-size-12 g-color-primary g-mb-5">Conferencia Magistral - Retos y Oportunidades del Sector
                            Agroalimentario ante el Nuevo Escenario</h4>
                          <h3 class="text-uppercase g-font-weight-700 g-font-size-23 g-mb-10">Ing. Bosco de la Vega Valladolid</h3>
                          <p class="g-color-gray-dark-v5 mb-0">Presidente del Consejo Nacional Agropecuario</p>
                        </div>
                      </div>
                    </div>
                  </div>
                </a>

              </div>
            </div>

            <div class="tab-pane fade" id="day-2" role="tabpanel">
              <div class="u-timeline-v3-wrap">
                <a class="u-timeline-v3 d-block text-center text-md-left g-parent u-link-v5 g-mb-50" href="#">
                  <div class="g-hidden-sm-down u-timeline-v3__icon g-absolute-centered--y g-z-index-3 g-line-height-0 g-width-16 g-height-16 g-ml-minus-8">
                    <i class="d-inline-block g-width-16 g-height-16 g-bg-white g-brd-5 g-brd-style-solid g-brd-gray-light-v4 g-rounded-50"></i>
                  </div>

                  <div class="row mx-0">
                    <div class="col-md-3 g-order-2 g-order-1--sm d-flex align-self-center px-0">
                      <div class="u-heading-v1-4 g-brd-gray-light-v2 w-100">
                        <span class="text-center g-pos-rel d-block g-width-110 g-line-height-1_6 g-font-weight-600 g-color-white g-font-size-14 g-bg-gray-dark-v1 
                        g-bg-primary--parent-hover g-py-5 g-px-10 mx-auto g-ml-0--md g-transition-0_2 g-transition--ease-in g-mb-10 g-mb-0--md">9:00 hrs.</span>
                      </div>
                    </div>

                    <div class="col-md-9 g-order-1 g-order-2--sm px-0 g-mb-15 g-mb-0--md">
                      <div class="media d-block d-md-flex">
                       

                        <div class="media-body align-self-center">
                          <h4 class="text-uppercase g-font-weight-700 g-font-size-12 g-color-primary g-mb-5">Conferencia - Integración y Relevo Generacional de los Jóvenes al
                              Sector Agroindustrial</h4>
                          <h3 class="text-uppercase g-font-weight-700 g-font-size-23 g-mb-10">Mtro. Rodney Orlando Cordero Salas</h3>
                          <p class="g-color-gray-dark-v5 mb-0">Docente de Especies Zootécnicas, Escuela de Ciencias Agrarias.
                              Universidad Nacional de Costa Rica</p>
                        </div>
                      </div>
                    </div>
                  </div>
                </a>

                <a class="u-timeline-v3 d-block text-center text-md-left g-parent u-link-v5 g-mb-50" href="#">
                  <div class="g-hidden-sm-down u-timeline-v3__icon g-absolute-centered--y g-z-index-3 g-line-height-0 g-width-16 g-height-16 g-ml-minus-8">
                    <i class="d-inline-block g-width-16 g-height-16 g-bg-white g-brd-5 g-brd-style-solid g-brd-gray-light-v4 g-rounded-50"></i>
                  </div>

                  <div class="row mx-0">
                    <div class="col-md-3 g-order-2 g-order-1--sm d-flex align-self-center px-0">
                      <div class="u-heading-v1-4 g-brd-gray-light-v2 w-100">
                        <span class="text-center g-pos-rel d-block g-width-110 g-line-height-1_6 g-font-weight-600 g-color-white g-font-size-14 g-bg-gray-dark-v1 
                        g-bg-primary--parent-hover g-py-5 g-px-10 mx-auto g-ml-0--md g-transition-0_2 g-transition--ease-in g-mb-10 g-mb-0--md">10:00 hrs.</span>
                      </div>
                    </div>

                    <div class="col-md-9 g-order-1 g-order-2--sm px-0 g-mb-15 g-mb-0--md">
                      <div class="media d-block d-md-flex">
                    

                        <div class="media-body align-self-center">
                          <h4 class="text-uppercase g-font-weight-700 g-font-size-12 g-color-primary g-mb-5">Panel - Estrategia de Planeación Agropecuaria y Abasto Alimentario</h4>
                          
                          <p class="g-color-gray-dark-v5 mb-0"><b>- Dr. Arturo Puente González</b> - Director en Jefe de ASERCA</p>
                          <p class="g-color-gray-dark-v5 mb-0"><b>- Lic. Ignacio Ovalle Fernández</b> - Titular de Seguridad Alimentaria Mexicana</p>
                        </div>
                      </div>
                    </div>
                  </div>
                </a>

                <a class="u-timeline-v3 d-block text-center text-md-left g-parent u-link-v5 g-mb-50" href="#">
                  <div class="g-hidden-sm-down u-timeline-v3__icon g-absolute-centered--y g-z-index-3 g-line-height-0 g-width-16 g-height-16 g-ml-minus-8">
                    <i class="d-inline-block g-width-16 g-height-16 g-bg-white g-brd-5 g-brd-style-solid g-brd-gray-light-v4 g-rounded-50"></i>
                  </div>

                  <div class="row mx-0">
                    <div class="col-md-3 g-order-2 g-order-1--sm d-flex align-self-center px-0">
                      <div class="u-heading-v1-4 g-brd-gray-light-v2 w-100">
                        <span class="text-center g-pos-rel d-block g-width-110 g-line-height-1_6 g-font-weight-600 g-color-white g-font-size-14 g-bg-gray-dark-v1 
                        g-bg-primary--parent-hover g-py-5 g-px-10 mx-auto g-ml-0--md g-transition-0_2 g-transition--ease-in g-mb-10 g-mb-0--md">11:00 hrs.</span>
                      </div>
                    </div>

                    <div class="col-md-9 g-order-1 g-order-2--sm px-0 g-mb-15 g-mb-0--md">
                      <div class="media d-block d-md-flex">
                      
                        <div class="media-body align-self-center">
                          <h4 class="text-uppercase g-font-weight-700 g-font-size-12 g-color-primary g-mb-5">Conferencia - Innovación en las Cadenas Agroalimentarias. Oportunidades de la
                              Cooperación Italia - Zacatecas</h4>
                          <h3 class="text-uppercase g-font-weight-700 g-font-size-23 g-mb-10">Mtro. José Luis Rhi Sausi</h3>
                          <p class="g-color-gray-dark-v5 mb-0">Coordinador del Foro Ítalo - Latinoamericano sobre Pymes, IILA</p>
                        </div>
                      </div>
                    </div>
                  </div>
                </a>

                <a class="u-timeline-v3 d-block text-center text-md-left g-parent u-link-v5 g-mb-50" href="#">
                    <div class="g-hidden-sm-down u-timeline-v3__icon g-absolute-centered--y g-z-index-3 g-line-height-0 g-width-16 g-height-16 g-ml-minus-8">
                      <i class="d-inline-block g-width-16 g-height-16 g-bg-white g-brd-5 g-brd-style-solid g-brd-gray-light-v4 g-rounded-50"></i>
                    </div>
  
                    <div class="row mx-0">
                      <div class="col-md-3 g-order-2 g-order-1--sm d-flex align-self-center px-0">
                        <div class="u-heading-v1-4 g-brd-gray-light-v2 w-100">
                          <span class="text-center g-pos-rel d-block g-width-110 g-line-height-1_6 g-font-weight-600 g-color-white g-font-size-14 g-bg-gray-dark-v1 
                          g-bg-primary--parent-hover g-py-5 g-px-10 mx-auto g-ml-0--md g-transition-0_2 g-transition--ease-in g-mb-10 g-mb-0--md">12:30 hrs.</span>
                        </div>
                      </div>
  
                      <div class="col-md-9 g-order-1 g-order-2--sm px-0 g-mb-15 g-mb-0--md">
                        <div class="media d-block d-md-flex">
                         
  
                          <div class="media-body align-self-center">
                              <h4 class="text-uppercase g-font-weight-700 g-font-size-12 g-color-primary g-mb-5">Conferencia - Puente Internacional de PHARR</h4>
                              <h3 class="text-uppercase g-font-weight-700 g-font-size-23 g-mb-10">Luis Bazán</h3>
                              <p class="g-color-gray-dark-v5 mb-0">Director General Puente Internacional de PHARR</p>
                          </div>
                        </div>
                      </div>
                    </div>
                  </a>

                  <a class="u-timeline-v3 d-block text-center text-md-left g-parent u-link-v5 g-mb-50" href="#">
                      <div class="g-hidden-sm-down u-timeline-v3__icon g-absolute-centered--y g-z-index-3 g-line-height-0 g-width-16 g-height-16 g-ml-minus-8">
                        <i class="d-inline-block g-width-16 g-height-16 g-bg-white g-brd-5 g-brd-style-solid g-brd-gray-light-v4 g-rounded-50"></i>
                      </div>
    
                      <div class="row mx-0">
                        <div class="col-md-3 g-order-2 g-order-1--sm d-flex align-self-center px-0">
                          <div class="u-heading-v1-4 g-brd-gray-light-v2 w-100">
                            <span class="text-center g-pos-rel d-block g-width-110 g-line-height-1_6 g-font-weight-600 g-color-white g-font-size-14 g-bg-gray-dark-v1 
                            g-bg-primary--parent-hover g-py-5 g-px-10 mx-auto g-ml-0--md g-transition-0_2 g-transition--ease-in g-mb-10 g-mb-0--md">13:30 hrs.</span>
                          </div>
                        </div>
    
                        <div class="col-md-9 g-order-1 g-order-2--sm px-0 g-mb-15 g-mb-0--md">
                          <div class="media d-block d-md-flex">
                           
    
                            <div class="media-body align-self-center">
                                <h4 class="text-uppercase g-font-weight-700 g-font-size-12 g-color-primary g-mb-5">Conferencia - Estrategia Integral del Desarrollo Rural.
                                  </h4>
                                   <h3 class="text-uppercase g-font-weight-700 g-font-size-23 g-mb-10"> Dr. Salvador Fernández Rivera</h3>
                                   <p class="g-color-gray-dark-v5 mb-0">Coordinador General de Desarrollo Rural SADER</p>
                            </div>
                          </div>
                        </div>
                      </div>
                    </a>


                    <a class="u-timeline-v3 d-block text-center text-md-left g-parent u-link-v5 g-mb-50" href="#">
                        <div class="g-hidden-sm-down u-timeline-v3__icon g-absolute-centered--y g-z-index-3 g-line-height-0 g-width-16 g-height-16 g-ml-minus-8">
                          <i class="d-inline-block g-width-16 g-height-16 g-bg-white g-brd-5 g-brd-style-solid g-brd-gray-light-v4 g-rounded-50"></i>
                        </div>
      
                        <div class="row mx-0">
                          <div class="col-md-3 g-order-2 g-order-1--sm d-flex align-self-center px-0">
                            <div class="u-heading-v1-4 g-brd-gray-light-v2 w-100">
                              <span class="text-center g-pos-rel d-block g-width-110 g-line-height-1_6 g-font-weight-600 g-color-white g-font-size-14 g-bg-gray-dark-v1 
                              g-bg-primary--parent-hover g-py-5 g-px-10 mx-auto g-ml-0--md g-transition-0_2 g-transition--ease-in g-mb-10 g-mb-0--md">14:30 hrs.</span>
                            </div>
                          </div>
      
                          <div class="col-md-9 g-order-1 g-order-2--sm px-0 g-mb-15 g-mb-0--md">
                            <div class="media d-block d-md-flex">
                             
                              <div class="media-body align-self-center">
                                  <h4 class="text-uppercase g-font-weight-700 g-font-size-12 g-color-primary g-mb-5">Conferencia - Promoción del Comercio y la Atracción de Inversiones
                                      Tratado Comercial América del Norte (México, Estados Unidos y Canadá)</h4>
                                  <h3 class="text-uppercase g-font-weight-700 g-font-size-23 g-mb-10">Dra. Luz María de la Mora Sánchez</h3>
                                  <p class="g-color-gray-dark-v5 mb-0">Subsecretaría de Comercio Exterior, Secretaría de Economía</p>
                              </div>
                            </div>
                          </div>
                        </div>
                      </a>

                      <a class="u-timeline-v3 d-block text-center text-md-left g-parent u-link-v5 g-mb-50" href="#">
                          <div class="g-hidden-sm-down u-timeline-v3__icon g-absolute-centered--y g-z-index-3 g-line-height-0 g-width-16 g-height-16 g-ml-minus-8">
                            <i class="d-inline-block g-width-16 g-height-16 g-bg-white g-brd-5 g-brd-style-solid g-brd-gray-light-v4 g-rounded-50"></i>
                          </div>
        
                          <div class="row mx-0">
                            <div class="col-md-3 g-order-2 g-order-1--sm d-flex align-self-center px-0">
                              <div class="u-heading-v1-4 g-brd-gray-light-v2 w-100">
                                <span class="text-center g-pos-rel d-block g-width-110 g-line-height-1_6 g-font-weight-600 g-color-white g-font-size-14 g-bg-gray-dark-v1 
                                g-bg-primary--parent-hover g-py-5 g-px-10 mx-auto g-ml-0--md g-transition-0_2 g-transition--ease-in g-mb-10 g-mb-0--md">16:30 hrs.</span>
                              </div>
                            </div>
        
                            <div class="col-md-9 g-order-1 g-order-2--sm px-0 g-mb-15 g-mb-0--md">
                              <div class="media d-block d-md-flex">
                               
        
                                <div class="media-body align-self-center">
                                    <h4 class="text-uppercase g-font-weight-700 g-font-size-12 g-color-primary g-mb-5">Panel - Tomate y su Comercialización</h4>
                                    <h3 class="text-uppercase g-font-weight-700 g-font-size-23 g-mb-10">Lic. Fernando Ruíz Huarte</h3>
                                    <p class="g-color-gray-dark-v5 mb-0">Director General COMCE Nacional</p>
                                </div>
                              </div>
                            </div>
                          </div>
                        </a>

                        <a class="u-timeline-v3 d-block text-center text-md-left g-parent u-link-v5 g-mb-50" href="#">
                            <div class="g-hidden-sm-down u-timeline-v3__icon g-absolute-centered--y g-z-index-3 g-line-height-0 g-width-16 g-height-16 g-ml-minus-8">
                              <i class="d-inline-block g-width-16 g-height-16 g-bg-white g-brd-5 g-brd-style-solid g-brd-gray-light-v4 g-rounded-50"></i>
                            </div>
          
                            <div class="row mx-0">
                              <div class="col-md-3 g-order-2 g-order-1--sm d-flex align-self-center px-0">
                                <div class="u-heading-v1-4 g-brd-gray-light-v2 w-100">
                                  <span class="text-center g-pos-rel d-block g-width-110 g-line-height-1_6 g-font-weight-600 g-color-white g-font-size-14 g-bg-gray-dark-v1 
                                  g-bg-primary--parent-hover g-py-5 g-px-10 mx-auto g-ml-0--md g-transition-0_2 g-transition--ease-in g-mb-10 g-mb-0--md">17:30 hrs.</span>
                                </div>
                              </div>
          
                              <div class="col-md-9 g-order-1 g-order-2--sm px-0 g-mb-15 g-mb-0--md">
                                <div class="media d-block d-md-flex">
                                 
          
                                  <div class="media-body align-self-center">
                                      <h4 class="text-uppercase g-font-weight-700 g-font-size-12 g-color-primary g-mb-5">Conferencia - Comercio Bilateral de Productos Agrícolas MEX - USA</h4>
                                      <h3 class="text-uppercase g-font-weight-700 g-font-size-23 g-mb-10">C. Orestes Vasquez</h3>
                                      <p class="g-color-gray-dark-v5 mb-0">	Director de la Oficina Agrocomercial del Consulado de EU en Monterrey</p>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </a>

                          <a class="u-timeline-v3 d-block text-center text-md-left g-parent u-link-v5 g-mb-50" href="#">
                              <div class="g-hidden-sm-down u-timeline-v3__icon g-absolute-centered--y g-z-index-3 g-line-height-0 g-width-16 g-height-16 g-ml-minus-8">
                                <i class="d-inline-block g-width-16 g-height-16 g-bg-white g-brd-5 g-brd-style-solid g-brd-gray-light-v4 g-rounded-50"></i>
                              </div>
            
                              <div class="row mx-0">
                                <div class="col-md-3 g-order-2 g-order-1--sm d-flex align-self-center px-0">
                                  <div class="u-heading-v1-4 g-brd-gray-light-v2 w-100">
                                    <span class="text-center g-pos-rel d-block g-width-110 g-line-height-1_6 g-font-weight-600 g-color-white g-font-size-14 g-bg-gray-dark-v1 
                                    g-bg-primary--parent-hover g-py-5 g-px-10 mx-auto g-ml-0--md g-transition-0_2 g-transition--ease-in g-mb-10 g-mb-0--md">18:30 hrs.</span>
                                  </div>
                                </div>
            
                                <div class="col-md-9 g-order-1 g-order-2--sm px-0 g-mb-15 g-mb-0--md">
                                  <div class="media d-block d-md-flex">
                                   
                                    <div class="media-body align-self-center">
                                      <h4 class="text-uppercase g-font-weight-700 g-font-size-12 g-color-primary g-mb-5">Panel - Certificaciones con enfoque de mercado global, “Sanidad, inocuidad,
                                          medio ambiente y responsabilidad social”</h4>
                                          <p class="g-color-gray-dark-v5 mb-0"><b>- Lic. Humberto Martínez Romero</b> - Director General del Grupo Rosmar</p>
                                          <p class="g-color-gray-dark-v5 mb-0"><b>- LAN Juan Jesús Sánchez Miller</b> - Director SCS Global Services México</p>
                                    </div>
                                  </div>
                                </div>
                              </div>
                            </a>

              
              </div>
            </div>

            <div class="tab-pane fade" id="day-3" role="tabpanel">
              <div class="u-timeline-v3-wrap">
                <a class="u-timeline-v3 d-block text-center text-md-left g-parent u-link-v5 g-mb-50" href="#">
                  <div class="g-hidden-sm-down u-timeline-v3__icon g-absolute-centered--y g-z-index-3 g-line-height-0 g-width-16 g-height-16 g-ml-minus-8">
                    <i class="d-inline-block g-width-16 g-height-16 g-bg-white g-brd-5 g-brd-style-solid g-brd-gray-light-v4 g-rounded-50"></i>
                  </div>

                  <div class="row mx-0">
                    <div class="col-md-3 g-order-2 g-order-1--sm d-flex align-self-center px-0">
                      <div class="u-heading-v1-4 g-brd-gray-light-v2 w-100">
                        <span class="text-center g-pos-rel d-block g-width-110 g-line-height-1_6 g-font-weight-600 g-color-white g-font-size-14 g-bg-gray-dark-v1 
                        g-bg-primary--parent-hover g-py-5 g-px-10 mx-auto g-ml-0--md g-transition-0_2 g-transition--ease-in g-mb-10 g-mb-0--md">10:00 hrs.</span>
                      </div>
                    </div>

                    <div class="col-md-9 g-order-1 g-order-2--sm px-0 g-mb-15 g-mb-0--md">
                      <div class="media d-block d-md-flex">
                        

                        <div class="media-body align-self-center">
                          <h4 class="text-uppercase g-font-weight-700 g-font-size-12 g-color-primary g-mb-5">Conferencia - Inteligencia Satelital; Tecnología en Irrigación para
                              Cultivos de Campo Abierto</h4>
                          <h3 class="text-uppercase g-font-weight-700 g-font-size-23 g-mb-10">Ing. Francisco Javier Angulo Rendón</h3>
                          <p class="g-color-gray-dark-v5 mb-0">Gerente de Agronomía y Soporte al Producto de Rivulis-Eurodrip</p>
                        </div>
                      </div>
                    </div>
                  </div>
                </a>

                <a class="u-timeline-v3 d-block text-center text-md-left g-parent u-link-v5 g-mb-50" href="#">
                  <div class="g-hidden-sm-down u-timeline-v3__icon g-absolute-centered--y g-z-index-3 g-line-height-0 g-width-16 g-height-16 g-ml-minus-8">
                    <i class="d-inline-block g-width-16 g-height-16 g-bg-white g-brd-5 g-brd-style-solid g-brd-gray-light-v4 g-rounded-50"></i>
                  </div>

                  <div class="row mx-0">
                    <div class="col-md-3 g-order-2 g-order-1--sm d-flex align-self-center px-0">
                      <div class="u-heading-v1-4 g-brd-gray-light-v2 w-100">
                        <span class="text-center g-pos-rel d-block g-width-110 g-line-height-1_6 g-font-weight-600 g-color-white g-font-size-14 g-bg-gray-dark-v1 
                        g-bg-primary--parent-hover g-py-5 g-px-10 mx-auto g-ml-0--md g-transition-0_2 g-transition--ease-in g-mb-10 g-mb-0--md">11:00 hrs.</span>
                      </div>
                    </div>

                    <div class="col-md-9 g-order-1 g-order-2--sm px-0 g-mb-15 g-mb-0--md">
                      <div class="media d-block d-md-flex">
                       

                        <div class="media-body align-self-center">
                          <h4 class="text-uppercase g-font-weight-700 g-font-size-12 g-color-primary g-mb-5">Conferencia Magistral - Desde Semillas para la Paz, hacia Sistemas
                              Integrales para la Prosperidad</h4>
                          <h3 class="text-uppercase g-font-weight-700 g-font-size-23 g-mb-10">Dr. Bram Govaerts</h3>
                          <p class="g-color-gray-dark-v5 mb-0">Director Global de Innovación Estratégica, CIMMYT</p>
                        </div>
                      </div>
                    </div>
                  </div>
                </a>

                <a class="u-timeline-v3 d-block text-center text-md-left g-parent u-link-v5 g-mb-50" href="#">
                  <div class="g-hidden-sm-down u-timeline-v3__icon g-absolute-centered--y g-z-index-3 g-line-height-0 g-width-16 g-height-16 g-ml-minus-8">
                    <i class="d-inline-block g-width-16 g-height-16 g-bg-white g-brd-5 g-brd-style-solid g-brd-gray-light-v4 g-rounded-50"></i>
                  </div>

                  <div class="row mx-0">
                    <div class="col-md-3 g-order-2 g-order-1--sm d-flex align-self-center px-0">
                      <div class="u-heading-v1-4 g-brd-gray-light-v2 w-100">
                        <span class="text-center g-pos-rel d-block g-width-110 g-line-height-1_6 g-font-weight-600 g-color-white g-font-size-14 g-bg-gray-dark-v1 
                        g-bg-primary--parent-hover g-py-5 g-px-10 mx-auto g-ml-0--md g-transition-0_2 g-transition--ease-in g-mb-10 g-mb-0--md">12:00 hrs.</span>
                      </div>
                    </div>

                    <div class="col-md-9 g-order-1 g-order-2--sm px-0 g-mb-15 g-mb-0--md">
                      <div class="media d-block d-md-flex">
                      

                        <div class="media-body align-self-center">
                          <h4 class="text-uppercase g-font-weight-700 g-font-size-12 g-color-primary g-mb-5">Conferencia - Agricultura Sostenible</h4>
                          <h3 class="text-uppercase g-font-weight-700 g-font-size-23 g-mb-10">Dr. Benjamín Figueroa Sandoval</h3>
                          <p class="g-color-gray-dark-v5 mb-0">Conferensista Magistral en el Congreso Nacional de Educación,
                              AMIP y Profesor Investigador de COLPOS</p>
                        </div>
                      </div>
                    </div>
                  </div>
                </a>
              </div>
            </div>
          </div>
          <!-- End Tab panes -->
          
        </div>
      </section>
      <!-- End Timeline -->