<!DOCTYPE html>
<html lang="en">
  <head>
    <!-- Title -->
    <title>Agroalimentaria Zacatecas 2019</title>

    <!-- Required Meta Tags Always Come First -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta http-equiv="x-ua-compatible" content="ie=edge">

    <!-- Favicon -->
    <link rel="shortcut icon" href="{{asset('/img/recursosAgro/favicon.png')}}">

    <!-- Google Fonts -->
    <link rel="stylesheet" href="//fonts.googleapis.com/css?family=Open+Sans:400,300,500,600,700,800">

    <!-- CSS Global Compulsory -->
    <link rel="stylesheet" href="{{asset('/vendor/bootstrap/bootstrap.min.css')}}">

    <!-- CSS Implementing Plugins -->
    <link rel="stylesheet" href="{{asset('/vendor/icon-awesome/css/font-awesome.min.css')}}">
    <link rel="stylesheet" href="{{asset('/vendor/icon-line/css/simple-line-icons.css')}}">
    <link rel="stylesheet" href="{{asset('/vendor/icon-hs/style.css')}}">
    <link rel="stylesheet" href="{{asset('/vendor/hamburgers/hamburgers.min.css')}}">
    <link rel="stylesheet" href="{{asset('/vendor/animate.css')}}">
    <link rel="stylesheet" href="{{asset('/vendor/slick-carousel/slick/slick.css')}}">
    <link rel="stylesheet" href="{{asset('/vendor/fancybox/jquery.fancybox.css')}}">
    <link  rel="stylesheet" href="{{asset('/vendor/jquery-ui/themes/base/jquery-ui.min.css')}}">

      <!-- CSS Implementing Plugins -->
      <link  rel="stylesheet" href="{{asset('/vendor/dzsparallaxer/dzsparallaxer.css')}}">
      <link  rel="stylesheet" href="{{asset('/vendor/dzsparallaxer/dzsscroller/scroller.css')}}">
      <link  rel="stylesheet" href="{{asset('/vendor/dzsparallaxer/advancedscroller/plugin.css')}}">
    

    <!-- CSS Template -->
    <link rel="stylesheet" href="{{asset('/css/styles.op-event.css')}}">

    <!-- CSS Customization -->
    <link rel="stylesheet" href="{{asset('/css/custom.css')}}">
  </head>

  <body>
    <main>
      <!-- Header v1 -->
      <header id="js-header" class="u-header u-header--sticky-top u-header--change-appearance g-z-index-9999"
              data-header-fix-moment="100">
        <div class="light-theme u-header__section g-bg-darkblue-opacity-0_7 g-transition-0_3 g-py-10"
             data-header-fix-moment-exclude="light-theme g-bg-darkblue-opacity-0_7"
             data-header-fix-moment-classes="dark-theme u-shadow-v18 g-bg-white-opacity-0_9">
          <nav class="navbar navbar-expand-lg g-py-0">
            <div class="container g-pos-rel">
              <!-- Logo -->
              <a href="#" class="js-go-to navbar-brand u-header__logo"
                 data-type="static">
                <img class="u-header__logo-img u-header__logo-img--main d-block" src="{{asset('/img/recursosAgro/agroLogoInicio.png')}}" alt="Image description"
                     data-header-fix-moment-exclude="d-block"
                     data-header-fix-moment-classes="d-none"
                     style="height: 150; width: 60;">

                <img class="u-header__logo-img u-header__logo-img--main d-none" src="{{asset('/img/recursosAgro/agroLogoInicio.png')}}" alt="Image description"
                     data-header-fix-moment-exclude="d-none"
                     data-header-fix-moment-classes="d-block">
              </a>
              <!-- End Logo -->

              <!-- Navigation -->
              <div class="collapse navbar-collapse align-items-center flex-sm-row" id="navBar" data-mobile-scroll-hide="true">
                <ul id="js-scroll-nav" class="navbar-nav text-uppercase g-font-weight-700 g-font-size-11 g-pt-20 g-pt-5--lg ml-auto">
                  <li class="nav-item g-mr-15--lg g-mb-7 g-mb-0--lg active">
                    <a href="#home" class="nav-link p-0">INICIO <span class="sr-only">(current)</span></a>
                  </li>
                  <li class="nav-item g-mx-15--lg g-mb-7 g-mb-0--lg">
                    <a href="#ponentes" class="nav-link p-0">PONENTES</a>
                  </li>
                  <li class="nav-item g-mx-15--lg g-mb-7 g-mb-0--lg">
                    <a href="#schedule" class="nav-link p-0">PROGRAMA DEL EVENTO</a>
                  </li>
                  <li class="nav-item g-mx-15--lg g-mb-7 g-mb-0--lg">
                    <a href="#expositores" class="nav-link p-0">EXPOSITORES</a>
                  </li>
                  <li class="nav-item g-mx-15--lg g-mb-7 g-mb-0--lg">
                    <a href="#sponsors" class="nav-link p-0">PATROCINADORES Y ALIADOS</a>
                  </li>
                  <li class="nav-item g-mx-15--lg g-mb-7 g-mb-0--lg">
                      <a href="#registro" class="nav-link p-0">REGISTRO DE PARTICIPANTES</a>
                    </li>
                  <li class="nav-item g-mx-15--lg g-mb-7 g-mb-0--lg">
                    <a href="#contact" class="nav-link p-0">CONTACTO</a>
                  </li>
                </ul>
              </div>
              <!-- End Navigation -->

              <!-- Responsive Toggle Button -->
              <button class="navbar-toggler btn g-line-height-1 g-brd-none g-pa-0 g-pos-abs g-top-15 g-right-0" type="button"
                      aria-label="Toggle navigation"
                      aria-expanded="false"
                      aria-controls="navBar"
                      data-toggle="collapse"
                      data-target="#navBar">
                <span class="hamburger hamburger--slider">
                  <span class="hamburger-box">
                    <span class="hamburger-inner"></span>
                  </span>
                </span>
              </button>
              <!-- End Responsive Toggle Button -->
            </div>
          </nav>
        </div>
      </header>
      <!-- End Header v1 -->

      <!-- Section Content -->
      <section id="home" class="g-bg-img-hero g-pos-rel u-bg-overlay  g-pt-150" style="background-image: url({{asset('/img/recursosAgro/bannerAgro.png')}});">
        <div class="container g-max-width-750 u-bg-overlay__inner g-color-white g-mb-60">
          <!-- Countdown v4 
          <div class="js-countdown text-center text-uppercase u-countdown-v4 g-mb-40 g-mb-70--md"
               data-end-date="2019/08/22"
               data-month-format="%m"
               data-days-format="%D"
               data-hours-format="%H"
               data-minutes-format="%M"
               data-seconds-format="%S">
            <div class="d-inline-block g-font-size-12 g-px-10 g-px-50--md g-py-15">
              <div class="js-cd-days g-font-size-20 g-font-size-40--md g-font-weight-700 g-line-height-1 g-mb-5">33</div>
              <span>DÍAS</span>
            </div>

            <div class="d-inline-block g-font-size-12 g-brd-left g-brd-white-opacity-0_4 g-px-10 g-px-60--md g-py-15">
              <div class="js-cd-hours g-font-size-20 g-font-size-40--md g-font-weight-700 g-line-height-1 g-mb-5">01</div>
              <span>HORAS</span>
            </div>

            <div class="d-inline-block g-font-size-12 g-brd-left g-brd-white-opacity-0_4 g-px-10 g-px-60--md g-py-15">
              <div class="js-cd-minutes g-font-size-20 g-font-size-40--md g-font-weight-700 g-line-height-1 g-mb-5">52</div>
              <span>MINUTOS</span>
            </div>

            <div class="d-inline-block g-font-size-12 g-brd-left g-brd-white-opacity-0_4 g-px-10 g-px-50--md g-py-15">
              <div class="js-cd-seconds g-font-size-20 g-font-size-40--md g-font-weight-700 g-line-height-1 g-mb-5">52</div>
              <span>SEGUNDOS</span>
            </div>
          </div>-->
          <!-- End Countdown v4 -->
          <div class="js-countdown u-countdown-v5 text-center" data-end-date="2019/08/22" data-days-format="%D" data-hours-format="%H" data-minutes-format="%M" data-seconds-format="%S">
            <div class="d-inline-block g-mx-20 g-mb-40">
              <div class="js-cd-days g-font-size-56 g-font-weight-300 g-line-height-1"></div>
              <span>DÍAS</span>
            </div>
          
            <div class="hidden-down d-inline-block align-top g-font-size-28 g-font-weight-200 g-line-height-1_8">|</div>
          
            <div class="d-inline-block g-mx-20 g-mb-40">
              <div class="js-cd-hours g-font-size-56 g-font-weight-300 g-line-height-1"></div>
              <span>HORAS</span>
            </div>
          
            <div class="hidden-down d-inline-block align-top g-font-size-28 g-font-weight-200 g-line-height-1_8">|</div>
          
            <div class="d-inline-block g-mx-20 g-mb-40">
              <div class="js-cd-minutes g-font-size-56 g-font-weight-300 g-line-height-1"></div>
              <span>MINUTOS</span>
            </div>
          
            <div class="hidden-down d-inline-block align-top g-font-size-28 g-font-weight-200 g-line-height-1_8">|</div>
          
            <div class="d-inline-block g-mx-20 g-mb-40">
              <div class="js-cd-seconds g-font-size-56 g-font-weight-300 g-line-height-1"></div>
              <span>SEGUNDOS</span>
            </div>
          </div>
          <!--<h2 class="text-center text-uppercase h2 g-font-weight-700 g-font-size-40 g-font-size-60--md g-color-white g-mb-30 g-mb-70--md">UI &amp; UX Design 2017</h2> -->
            <img style="max-width: 100%; height: auto; width: auto;"
            src="{{asset('/img/recursosAgro/agroalimentariazac_logox5.png')}}" alt=""  align-items: g-absolute-centered;">
          <div class="row">
            <div class="col-sm-6 col-md-4 text-center text-md-left g-mb-30 g-mb-0--md">
              <div class="media">
                <div class="media-left d-flex align-self-center g-hidden-sm-down g-mr-20">
                  <i class="fa fa-calendar g-font-size-27 g-color-white-opacity-0_5"></i>
                </div>

                <div class="media-body text-uppercase">
                
                  <h3 class="h3 g-font-size-15 g-color-white mb-0">22, 23 Y 24 Agosto 2019</h3>

                </div>
              </div>
            </div>

            <div class="col-sm-6 col-md-5 text-center text-md-left g-mb-30 g-mb-0--md">
              <div class="media">
                <div class="media-left d-flex align-self-center g-hidden-sm-down g-mr-20">
                  <i class="fa fa-map-marker g-font-size-27 g-color-white-opacity-0_5"></i>
                </div>

                <div class="media-body text-uppercase">
                
                  <h3 class="h3 g-font-size-15 g-color-white mb-0">Palacio de Convenciones, Zacatecas</h3>
                </div>
              </div>
            </div>

            <div class="col-md-3 text-center text-md-right">
             <!--<a class="btn btn-lg text-uppercase u-btn-white g-font-weight-700 g-font-size-11 g-color-white--hover
               g-bg-primary--hover g-brd-none rounded-0 g-py-18 g-px-20" href="#registro">REGISTRATE AHORA</a>--> 
               <a href="#registro" class="btn btn-md u-btn-primary g-mr-10 g-mb-15">REGISTRATE AHORA</a>
            </div>
          </div>
        </div>

        <div class="container u-bg-overlay__inner g-bottom-minus-70 px-0">
          <div class="row u-shadow-v23 g-theme-bg-blue-dark-v2 mx-0">
            <div class="col-md-6 px-0">
              <div class="js-carousel text-center u-carousel-v5 g-overflow-hidden h-100 g-max-height-50vh"
                   data-infinite="true"
                   data-arrows-classes="u-arrow-v1 g-absolute-centered--y g-width-40 g-height-40 g-font-size-20 g-color-white g-color-primary--hover g-bg-primary g-bg-white--hover g-transition-0_2 g-transition--ease-in"
                   data-arrow-left-classes="fa fa-angle-left g-left-0"
                   data-arrow-right-classes="fa fa-angle-right g-right-0">
                <div class="js-slide g-bg-img-hero g-min-height-50vh" style="background-image: url({{asset('/img/recursosAgro/aboutImages/about2.png')}});"></div>

               
              </div>
            </div>

            <div class="col-md-6 d-flex g-min-height-50vh g-theme-color-gray-dark-v1 g-px-50 g-py-80 g-py-50--md g-py-20--lg">
              <div class="align-self-center w-100">
                <h2 class="text-uppercase g-font-weight-700 g-font-size-30 g-color-primary g-mb-10">Agroalimentaria Zacatecas 2019</h2>
                <h3 class="text-uppercase g-font-weight-500 g-font-size-13 g-color-white g-mb-20">Zacatecas, la potencia agroalimentaria de México</h3>
                <p class="g-font-size-14">Por segunda ocasión en el centro-norte del país, el Gobierno del Estado en conjunto con los productores de la cadena agroalimentaria, te invitan a ser parte del evento que será nuevamente punto de encuentro para que México y el mundo conozcan la oportunidad de negocios que ofrece el Campo Zacatecano</p>
                <p class="g-font-size-14 mb-0"></p>
              </div>
            </div>
          </div>
        </div>
      </section>
      <!-- End Section Content -->

       <!-- Section Content -->
      <section  class="u-bg-overlay g-bg-img-hero g-bg-darkblue-opacity-0_7--after g-py-100" style="background-image: url({{asset('/img/recursosAgro/aboutImages/bckvideo.png')}});">
        <div class="container u-bg-overlay__inner">
          <div class="text-center g-mb-70">
            
           
          </div>

          <div class="embed-responsive embed-responsive-16by9 event-video-effect">
              <iframe width="560" height="315" src="https://www.youtube.com/embed/bynwI-8Tkdw" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
          </div>
        </div>
      </section>
      <!-- End Section Content -->

       <!-- Section Content -->
       <section  class="u-bg-overlay g-bg-img-hero g-bg-darkblue-opacity-0_7--after  g-py-100" style="background-image: url({{asset('/img/recursosAgro/aboutImages/bckgalleria.png')}});">
        <div class="container u-bg-overlay__inner">
          <div class="text-center g-mb-70">
            <h2 class="text-uppercase g-line-height-1 g-font-weight-700 g-font-size-30 g-color-white g-mb-30">GALERÍA</h2>

            <p class="g-color-white">Experiencias de la Agroalimentaria Zacatecas 2018</p>
          </div>

          <div class="js-carousel text-center g-line-height-0 g-pb-50"
               data-infinite="true"
               data-autoplay="true"
               data-speed="5000"
               data-rows="2"
               data-slides-show="6"
               data-arrows-classes="u-arrow-v1 g-absolute-centered--y g-bottom-0 g-width-40 g-height-40 g-font-size-20 g-color-white g-color-primary--hover g-bg-primary g-bg-white--hover g-mt-minus-25 g-transition-0_2 g-transition--ease-in"
               data-arrow-left-classes="fa fa-angle-left g-left-0"
               data-arrow-right-classes="fa fa-angle-right g-right-0"
               data-pagi-classes="u-carousel-indicators-v1--white g-absolute-centered--x g-bottom-0"
               data-responsive='[{
                 "breakpoint": 1200,
                 "settings": {
                   "slidesToShow": 5
                 }
               }, {
                 "breakpoint": 992,
                 "settings": {
                   "slidesToShow": 4
                 }
               }, {
                 "breakpoint": 768,
                 "settings": {
                   "slidesToShow": 3
                 }
               }, {
                 "breakpoint": 576,
                 "settings": {
                   "slidesToShow": 2
                 }
               }]'>
            <a class="js-slide u-bg-overlay g-parent g-overflow-hidden " href="{{asset('/img-temp/400x269/img1.jpg')}}"
               data-fancybox="fancyGallery1">
              <img class="img-fluid g-transform-scale-1_1--parent-hover g-transition-0_3 g-transition--ease-in" src="{{asset('')}}/img-temp/200x200/img1.jpg" alt="Image description">
            </a>

            <a class="js-slide u-bg-overlay g-parent g-overflow-hidden " href="{{asset('/img-temp/400x269/img3.jpg')}}"
               data-fancybox="fancyGallery1">
              <img class="img-fluid g-transform-scale-1_1--parent-hover g-transition-0_3 g-transition--ease-in" src="{{asset('')}}/img-temp/200x200/img3.jpg" alt="Image description">
            </a>

            <a class="js-slide u-bg-overlay g-parent g-overflow-hidden " href="{{asset('/img-temp/400x269/img2.jpg')}}"
               data-fancybox="fancyGallery1">
              <img class="img-fluid g-transform-scale-1_1--parent-hover g-transition-0_3 g-transition--ease-in" src="{{asset('')}}/img-temp/200x200/img2.jpg" alt="Image description">
            </a>

            <a class="js-slide u-bg-overlay g-parent g-overflow-hidden " href="{{asset('/img-temp/400x269/img4.jpg')}}"
               data-fancybox="fancyGallery1">
              <img class="img-fluid g-transform-scale-1_1--parent-hover g-transition-0_3 g-transition--ease-in" src="{{asset('/img-temp/200x200/img4.jpg')}}" alt="Image description">
            </a>

            <a class="js-slide u-bg-overlay g-parent g-overflow-hidden " href="{{asset('')}}/img-temp/400x269/img1.jpg"
               data-fancybox="fancyGallery1">
              <img class="img-fluid g-transform-scale-1_1--parent-hover g-transition-0_3 g-transition--ease-in" src="{{asset('/img-temp/200x200/img1.jpg')}}" alt="Image description">
            </a>

            <a class="js-slide u-bg-overlay g-parent g-overflow-hidden " href="{{asset('')}}/img-temp/400x269/img3.jpg"
               data-fancybox="fancyGallery1">
              <img class="img-fluid g-transform-scale-1_1--parent-hover g-transition-0_3 g-transition--ease-in" src="{{asset('/img-temp/200x200/img3.jpg')}}" alt="Image description">
            </a>

            <a class="js-slide u-bg-overlay g-parent g-overflow-hidden " href="{{asset('')}}/img-temp/400x269/img2.jpg"
               data-fancybox="fancyGallery1">
              <img class="img-fluid g-transform-scale-1_1--parent-hover g-transition-0_3 g-transition--ease-in" src="{{asset('/img-temp/200x200/img2.jpg')}}" alt="Image description">
            </a>

            <a class="js-slide u-bg-overlay g-parent g-overflow-hidden " href="{{asset('')}}/img-temp/400x269/img4.jpg"
               data-fancybox="fancyGallery1">
              <img class="img-fluid g-transform-scale-1_1--parent-hover g-transition-0_3 g-transition--ease-in" src="{{asset('')}}/img-temp/200x200/img4.jpg" alt="Image description">
            </a>

            <a class="js-slide u-bg-overlay g-parent g-overflow-hidden " href="{{asset('')}}/img-temp/400x269/img1.jpg"
               data-fancybox="fancyGallery1">
              <img class="img-fluid g-transform-scale-1_1--parent-hover g-transition-0_3 g-transition--ease-in" src="{{asset('')}}/img-temp/200x200/img1.jpg" alt="Image description">
            </a>

            <a class="js-slide u-bg-overlay g-parent g-overflow-hidden " href="{{asset('')}}/img-temp/400x269/img3.jpg"
               data-fancybox="fancyGallery1">
              <img class="img-fluid g-transform-scale-1_1--parent-hover g-transition-0_3 g-transition--ease-in" src="{{asset('')}}/img-temp/200x200/img3.jpg" alt="Image description">
            </a>

            <a class="js-slide u-bg-overlay g-parent g-overflow-hidden " href="{{asset('')}}/img-temp/400x269/img2.jpg"
               data-fancybox="fancyGallery1">
              <img class="img-fluid g-transform-scale-1_1--parent-hover g-transition-0_3 g-transition--ease-in" src="{{asset('')}}/img-temp/200x200/img2.jpg" alt="Image description">
            </a>

            <a class="js-slide u-bg-overlay g-parent g-overflow-hidden " href="{{asset('')}}/img-temp/400x269/img4.jpg"
               data-fancybox="fancyGallery1">
              <img class="img-fluid g-transform-scale-1_1--parent-hover g-transition-0_3 g-transition--ease-in" src="{{asset('')}}/img-temp/200x200/img4.jpg" alt="Image description">
            </a>

            <a class="js-slide u-bg-overlay g-parent g-overflow-hidden " href="{{asset('')}}/img-temp/400x269/img1.jpg"
               data-fancybox="fancyGallery1">
              <img class="img-fluid g-transform-scale-1_1--parent-hover g-transition-0_3 g-transition--ease-in" src="{{asset('')}}/img-temp/200x200/img1.jpg" alt="Image description">
            </a>

            <a class="js-slide u-bg-overlay g-parent g-overflow-hidden " href="{{asset('')}}/img-temp/400x269/img3.jpg"
               data-fancybox="fancyGallery1">
              <img class="img-fluid g-transform-scale-1_1--parent-hover g-transition-0_3 g-transition--ease-in" src="{{asset('')}}/img-temp/200x200/img3.jpg" alt="Image description">
            </a>

            <a class="js-slide u-bg-overlay g-parent g-overflow-hidden " href="{{asset('')}}/img-temp/400x269/img2.jpg"
               data-fancybox="fancyGallery1">
              <img class="img-fluid g-transform-scale-1_1--parent-hover g-transition-0_3 g-transition--ease-in" src="{{asset('')}}/img-temp/200x200/img2.jpg" alt="Image description">
            </a>

            <a class="js-slide u-bg-overlay g-parent g-overflow-hidden " href="{{asset('')}}/img-temp/400x269/img4.jpg"
               data-fancybox="fancyGallery1">
              <img class="img-fluid g-transform-scale-1_1--parent-hover g-transition-0_3 g-transition--ease-in" src="{{asset('')}}/img-temp/200x200/img4.jpg" alt="Image description">
            </a>
          </div>
        </div>
      </section>
      <!-- End Section Content -->

      <section id="ponentes" class=" g-py-100">
        <div class="text-center g-mb-70">
            <h2 class="text-uppercase g-line-height-1 g-font-weight-700 g-font-size-30 g-mb-30">PONENTES</h2>
        </div>
      
        <!-- Team Block -->
        <div class="row">
          <div class="col-sm-4 g-mb-60">
            <!-- Figure -->
            <figure class="text-center">
              <!-- Figure Image -->
              <div class="d-block mx-auto rounded-circle g-max-width-200 g-bg-white g-pa-5 g-mb-15">
                <img class="rounded-circle g-max-width-190" src="{{asset('/img/recursosAgro/conferencistas/victor_villalobos.png')}}" alt="Image Description">
              </div>
              <!-- End Figure Image -->

              <!-- Figure Info -->
              <div class="g-mb-15">
                <h4 class="h4 g-color-black g-mb-5">Dr. Víctor Manuel Villalobos Arámbula</h4>
                <em class="d-block g-font-style-normal g-font-size-11 text-uppercase g-color-primary">Secretario de Agricultura y Desarrollo Rural.
                </em>
              </div>
            
            </figure>
            <!-- End Figure -->
          </div>

          <div class="col-sm-4 g-mb-60">
            <!-- Figure -->
            <figure class="text-center">
              <!-- Figure Image -->
              <div class="d-block mx-auto rounded-circle g-max-width-200 g-bg-white g-pa-5 g-mb-15">
                <img class="rounded-circle g-max-width-190" src="{{asset('/img/recursosAgro/conferencistas/bosco.png')}}" alt="Image Description">
              </div>
              <!-- End Figure Image -->

              <!-- Figure Info -->
              <div class="g-mb-15">
                <h4 class="h4 g-color-black g-mb-5">Ing. Bosco de la Vega Valladolid
                </h4>
                <em class="g-font-style-normal g-font-size-11 text-uppercase g-color-primary">Presidente del Consejo Nacional Agropecuario.
                </em>
              </div>
            
            </figure>
            <!-- End Figure -->
          </div>

          <div class="col-sm-4 g-mb-60">
            <!-- Figure -->
            <figure class="text-center">
              <!-- Figure Image -->
              <div class="d-block mx-auto rounded-circle g-max-width-200 g-bg-white g-pa-5 g-mb-15">
                <img class="rounded-circle g-max-width-190" src="{{asset('/img/recursosAgro/conferencistas/bram.png')}}" alt="Image Description">
              </div>
              <!-- End Figure Image -->

              <!-- Figure Info -->
              <div class="g-mb-15">
                <h4 class="h4 g-color-black g-mb-5">Dr. Bram Govaerts</h4>
                <em class="d-block g-font-style-normal g-font-size-11 text-uppercase g-color-primary">Director Global de Innovación Estratégica, CIMMYT</em>
              </div>
            
            </figure>
            <!-- End Figure -->
          </div>

          <div class="col-sm-4 g-mb-60">
            <!-- Figure -->
            <figure class="text-center">
              <!-- Figure Image -->
              <div class="d-block mx-auto rounded-circle g-max-width-200 g-bg-white g-pa-5 g-mb-15">
                <img class="rounded-circle g-max-width-190" src="{{asset('/img/recursosAgro/conferencistas/javierDelgado.png')}}" alt="Image Description">
              </div>
              <!-- End Figure Image -->

              <!-- Figure Info -->
              <div class="g-mb-15">
                <h4 class="h4 g-color-black g-mb-5">Dr. Javier Delgado Mendoza</h4>
                <em class="d-block g-font-style-normal g-font-size-11 text-uppercase g-color-primary">Director General de FND                </em>
              </div>
            
            </figure>
            <!-- End Figure -->
          </div>

          <div class="col-sm-4 g-mb-60">
            <!-- Figure -->
            <figure class="text-center">
              <!-- Figure Image -->
              <div class="d-block mx-auto rounded-circle g-max-width-200 g-bg-white g-pa-5 g-mb-15">
                <img class="rounded-circle g-max-width-190" src="{{asset('/img/recursosAgro/conferencistas/jesusAlan.png')}}" alt="Image Description">
              </div>
              <!-- End Figure Image -->

              <!-- Figure Info -->
              <div class="g-mb-15">
                <h4 class="h4 g-color-black g-mb-5">Act. Jesús Alan Elizondo Flores                </h4>
                <em class="d-block g-font-style-normal g-font-size-11 text-uppercase g-color-primary">Director General de FIRA                </em>
              </div>
            
            </figure>
            <!-- End Figure -->
          </div>
          <div class="col-sm-4 g-mb-60">
            <!-- Figure -->
            <figure class="text-center">
              <!-- Figure Image -->
              <div class="d-block mx-auto rounded-circle g-max-width-200 g-bg-white g-pa-5 g-mb-15">
                <img class="rounded-circle g-max-width-190" src="{{asset('/img/recursosAgro/conferencistas/dovalina.png')}}" alt="Image Description">
              </div>
              <!-- End Figure Image -->

              <!-- Figure Info -->
              <div class="g-mb-15">
                <h4 class="h4 g-color-black g-mb-5"> Ing. José Francisco Dovalina Lara</h4>
                <em class="d-block g-font-style-normal g-font-size-11 text-uppercase g-color-primary"> Ing. José Francisco Dovalina Lara</em>
              </div>
            
            </figure>
            <!-- End Figure -->
          </div>
          <div class="col-sm-4 g-mb-60">
            <!-- Figure -->
            <figure class="text-center">
              <!-- Figure Image -->
              <div class="d-block mx-auto rounded-circle g-max-width-200 g-bg-white g-pa-5 g-mb-15">
                <img class="rounded-circle g-max-width-190" src="{{asset('/img/recursosAgro/conferencistas/francisco_trujillo.png')}}" alt="Image Description">
              </div>
              <!-- End Figure Image -->

              <!-- Figure Info -->
              <div class="g-mb-15">
                <h4 class="h4 g-color-black g-mb-5">Dr. Francisco Javier Trujillo Arriaga</h4>
                <em class="d-block g-font-style-normal g-font-size-11 text-uppercase g-color-primary">Director en Jefe SENASICA</em>
              </div>
            
            </figure>
            <!-- End Figure -->
          </div>

          <div class="col-sm-4 g-mb-60">
            <!-- Figure -->
            <figure class="text-center">
              <!-- Figure Image -->
              <div class="d-block mx-auto rounded-circle g-max-width-200 g-bg-white g-pa-5 g-mb-15">
                <img class="rounded-circle g-max-width-190" src="{{asset('/img/recursosAgro/conferencistas/raviv.png')}}" alt="Image Description">
              </div>
              <!-- End Figure Image -->

              <!-- Figure Info -->
              <div class="g-mb-15">
                <h4 class="h4 g-color-black g-mb-5">Uriel Raviv</h4>
                <em class="d-block g-font-style-normal g-font-size-11 text-uppercase g-color-primary">Consejero Económico en México, Embajada de Israel</em>
              </div>
            
            </figure>
            <!-- End Figure -->
          </div>

          <div class="col-sm-4 g-mb-60">
            <!-- Figure -->
            <figure class="text-center">
              <!-- Figure Image -->
              <div class="d-block mx-auto rounded-circle g-max-width-200 g-bg-white g-pa-5 g-mb-15">
                <img class="rounded-circle g-max-width-190" src="{{asset('/img/recursosAgro/conferencistas/manuelEsteban.png')}}" alt="Image Description">
              </div>
              <!-- End Figure Image -->

              <!-- Figure Info -->
              <div class="g-mb-15">
                <h4 class="h4 g-color-black g-mb-5">Manuel Esteban Tarriba Urtuzuástegui</h4>
                <em class="d-block g-font-style-normal g-font-size-11 text-uppercase g-color-primary">Secretario de Agricultura y Ganadería de Sinaloa</em>
              </div>
            
            </figure>
            <!-- End Figure -->
          </div>

          <div class="col-sm-4 g-mb-60">
            <!-- Figure -->
            <figure class="text-center">
              <!-- Figure Image -->
              <div class="d-block mx-auto rounded-circle g-max-width-200 g-bg-white g-pa-5 g-mb-15">
                <img class="rounded-circle g-max-width-190" src="{{asset('/img/recursosAgro/conferencistas/rosarioAntonio.png')}}" alt="Image Description">
              </div>
              <!-- End Figure Image -->

              <!-- Figure Info -->
              <div class="g-mb-15">
                <h4 class="h4 g-color-black g-mb-5">Rosario Antonio Beltrán Ureta</h4>
                <em class="d-block g-font-style-normal g-font-size-11 text-uppercase g-color-primary">Presidente del Comité Nacional del Sistema Producto
                  Tomate</em>
              </div>
            
            </figure>
            <!-- End Figure -->
          </div>

          <div class="col-sm-4 g-mb-60">
            <!-- Figure -->
            <figure class="text-center">
              <!-- Figure Image -->
              <div class="d-block mx-auto rounded-circle g-max-width-200 g-bg-white g-pa-5 g-mb-15">
                <img class="rounded-circle g-max-width-190" src="{{asset('/img/recursosAgro/conferencistas/huarte.png')}}" alt="Image Description">
              </div>
              <!-- End Figure Image -->

              <!-- Figure Info -->
              <div class="g-mb-15">
                <h4 class="h4 g-color-black g-mb-5">Lic. Fernando Ruíz Huarte</h4>
                <em class="d-block g-font-style-normal g-font-size-11 text-uppercase g-color-primary">Director General COMCE Nacional</em>
              </div>
            
            </figure>
            <!-- End Figure -->
          </div>

          <div class="col-sm-4 g-mb-60">
            <!-- Figure -->
            <figure class="text-center">
              <!-- Figure Image -->
              <div class="d-block mx-auto rounded-circle g-max-width-200 g-bg-white g-pa-5 g-mb-15">
                <img class="rounded-circle g-max-width-190" src="{{asset('/img/recursosAgro/conferencistas/arturoPuente.png')}}" alt="Image Description">
              </div>
              <!-- End Figure Image -->

              <!-- Figure Info -->
              <div class="g-mb-15">
                <h4 class="h4 g-color-black g-mb-5">Dr. Arturo Puente González</h4>
                <em class="d-block g-font-style-normal g-font-size-11 text-uppercase g-color-primary">Director en Jefe de ASERCA</em>
              </div>
            
            </figure>
            <!-- End Figure -->
          </div>

          <div class="col-sm-4 g-mb-60">
            <!-- Figure -->
            <figure class="text-center">
              <!-- Figure Image -->
              <div class="d-block mx-auto rounded-circle g-max-width-200 g-bg-white g-pa-5 g-mb-15">
                <img class="rounded-circle g-max-width-190" src="{{asset('/img/recursosAgro/conferencistas/IgnacioOvalle.png')}}" alt="Image Description">
              </div>
              <!-- End Figure Image -->

              <!-- Figure Info -->
              <div class="g-mb-15">
                <h4 class="h4 g-color-black g-mb-5">Lic. Ignacio Ovalle Fernández</h4>
                <em class="d-block g-font-style-normal g-font-size-11 text-uppercase g-color-primary">Titular de Seguridad Alimentaria Mexicana</em>
              </div>
            
            </figure>
            <!-- End Figure -->
          </div>

          <div class="col-sm-4 g-mb-60">
            <!-- Figure -->
            <figure class="text-center">
              <!-- Figure Image -->
              <div class="d-block mx-auto rounded-circle g-max-width-200 g-bg-white g-pa-5 g-mb-15">
                <img class="rounded-circle g-max-width-190" src="{{asset('/img/recursosAgro/conferencistas/joseluisri.png')}}" alt="Image Description">
              </div>
              <!-- End Figure Image -->

              <!-- Figure Info -->
              <div class="g-mb-15">
                <h4 class="h4 g-color-black g-mb-5">Mtro. José Luis Rhi Sausi</h4>
                <em class="d-block g-font-style-normal g-font-size-11 text-uppercase g-color-primary">Coordinador del Foro Ítalo - Latinoamericano sobre
                  Pymes, IILA</em>
              </div>
            
            </figure>
            <!-- End Figure -->
          </div>

          <div class="col-sm-4 g-mb-60">
            <!-- Figure -->
            <figure class="text-center">
              <!-- Figure Image -->
              <div class="d-block mx-auto rounded-circle g-max-width-200 g-bg-white g-pa-5 g-mb-15">
                <img class="rounded-circle g-max-width-190" src="{{asset('/img/recursosAgro/conferencistas/luisbazan.png')}}" alt="Image Description">
              </div>
              <!-- End Figure Image -->

              <!-- Figure Info -->
              <div class="g-mb-15">
                <h4 class="h4 g-color-black g-mb-5">Luis Bazán</h4>
                <em class="d-block g-font-style-normal g-font-size-11 text-uppercase g-color-primary">Director General Puente Internacional de PHARR</em>
              </div>
            
            </figure>
            <!-- End Figure -->
          </div>

          <div class="col-sm-4 g-mb-60">
            <!-- Figure -->
            <figure class="text-center">
              <!-- Figure Image -->
              <div class="d-block mx-auto rounded-circle g-max-width-200 g-bg-white g-pa-5 g-mb-15">
                <img class="rounded-circle g-max-width-190" src="{{asset('/img/recursosAgro/conferencistas/salvadorfernandez.png')}}" alt="Image Description">
              </div>
              <!-- End Figure Image -->

              <!-- Figure Info -->
              <div class="g-mb-15">
                <h4 class="h4 g-color-black g-mb-5">Dr. Salvador Fernández Rivera</h4>
                <em class="d-block g-font-style-normal g-font-size-11 text-uppercase g-color-primary">Director en Jefe de ASERCA</em>
              </div>
            
            </figure>
            <!-- End Figure -->
          </div>

          <div class="col-sm-4 g-mb-60">
            <!-- Figure -->
            <figure class="text-center">
              <!-- Figure Image -->
              <div class="d-block mx-auto rounded-circle g-max-width-200 g-bg-white g-pa-5 g-mb-15">
                <img class="rounded-circle g-max-width-190" src="{{asset('/img/recursosAgro/conferencistas/maluz.png')}}" alt="Image Description">
              </div>
              <!-- End Figure Image -->

              <!-- Figure Info -->
              <div class="g-mb-15">
                <h4 class="h4 g-color-black g-mb-5">Dra. Luz María de la Mora Sánchez</h4>
                <em class="d-block g-font-style-normal g-font-size-11 text-uppercase g-color-primary">Subsecretaría de Comercio Exterior, Secretaría de
                  Economía</em>
              </div>
            
            </figure>
            <!-- End Figure -->
          </div>

          <div class="col-sm-4 g-mb-60">
            <!-- Figure -->
            <figure class="text-center">
              <!-- Figure Image -->
              <div class="d-block mx-auto rounded-circle g-max-width-200 g-bg-white g-pa-5 g-mb-15">
                <img class="rounded-circle g-max-width-190" src="{{asset('/img/recursosAgro/conferencistas/rodneyOrlando.png')}}" alt="Image Description">
              </div>
              <!-- End Figure Image -->

              <!-- Figure Info -->
              <div class="g-mb-15">
                <h4 class="h4 g-color-black g-mb-5">Mtro. Rodney Orlando Cordero Salas</h4>
                <em class="d-block g-font-style-normal g-font-size-11 text-uppercase g-color-primary">Docente de Especies Zootécnicas, Escuela de Ciencias
                  Agrarias.
                  Universidad Nacional de Costa Rica</em>
              </div>
            
            </figure>
            <!-- End Figure -->
          </div>

          <div class="col-sm-4 g-mb-60">
            <!-- Figure -->
            <figure class="text-center">
              <!-- Figure Image -->
              <div class="d-block mx-auto rounded-circle g-max-width-200 g-bg-white g-pa-5 g-mb-15">
                <img class="rounded-circle g-max-width-190" src="{{asset('/img/recursosAgro/conferencistas/oreste.png')}}" alt="Image Description">
              </div>
              <!-- End Figure Image -->

              <!-- Figure Info -->
              <div class="g-mb-15">
                <h4 class="h4 g-color-black g-mb-5">C. Orestes Vasquez</h4>
                <em class="d-block g-font-style-normal g-font-size-11 text-uppercase g-color-primary">Director de la Oficina Agrocomercial del Consulado de EU
                  en Monterrey</em>
              </div>
            
            </figure>
            <!-- End Figure -->
          </div>

          <div class="col-sm-4 g-mb-60">
            <!-- Figure -->
            <figure class="text-center">
              <!-- Figure Image -->
              <div class="d-block mx-auto rounded-circle g-max-width-200 g-bg-white g-pa-5 g-mb-15">
                <img class="rounded-circle g-max-width-190" src="{{asset('/img/recursosAgro/conferencistas/humbertoMartinez.png')}}" alt="Image Description">
              </div>
              <!-- End Figure Image -->

              <!-- Figure Info -->
              <div class="g-mb-15">
                <h4 class="h4 g-color-black g-mb-5">Lic. Humberto Martínez Romero</h4>
                <em class="d-block g-font-style-normal g-font-size-11 text-uppercase g-color-primary">Director General del Grupo Rosmar</em>
              </div>
            
            </figure>
            <!-- End Figure -->
          </div>

          <div class="col-sm-4 g-mb-60">
            <!-- Figure -->
            <figure class="text-center">
              <!-- Figure Image -->
              <div class="d-block mx-auto rounded-circle g-max-width-200 g-bg-white g-pa-5 g-mb-15">
                <img class="rounded-circle g-max-width-190" src="{{asset('/img/recursosAgro/conferencistas/miller.png')}}" alt="Image Description">
              </div>
              <!-- End Figure Image -->

              <!-- Figure Info -->
              <div class="g-mb-15">
                <h4 class="h4 g-color-black g-mb-5">LAN Juan Jesús Sánchez Miller</h4>
                <em class="d-block g-font-style-normal g-font-size-11 text-uppercase g-color-primary">Director SCS Global Services México</em>
              </div>
            
            </figure>
            <!-- End Figure -->
          </div>


         
        </div>

        <div class="row">
            
          <div class="col-sm-4 g-mb-60">
            <!-- Figure -->
            <figure class="text-center">
              <!-- Figure Image -->
              <div class="d-block mx-auto rounded-circle g-max-width-200 g-bg-white g-pa-5 g-mb-15">
                <img class="rounded-circle g-max-width-190" src="{{asset('/img/recursosAgro/conferencistas/angulo.png')}}" alt="Image Description">
              </div>
              <!-- End Figure Image -->

              <!-- Figure Info -->
              <div class="g-mb-15">
                <h4 class="h4 g-color-black g-mb-5">Ing. Francisco Javier Angulo Rendón</h4>
                <em class="d-block g-font-style-normal g-font-size-11 text-uppercase g-color-primary">Gerente de Agronomía y Soporte al Producto de RivulisEurodrip</em>
              </div>
            
            </figure>
            <!-- End Figure -->
          </div>

          <div class="col-sm-4 g-mb-60">
            <!-- Figure -->
            <figure class="text-center">
              <!-- Figure Image -->
              <div class="d-block mx-auto rounded-circle g-max-width-200 g-bg-white g-pa-5 g-mb-15">
                <img class="rounded-circle g-max-width-190" src="{{asset('/img/recursosAgro/conferencistas/katia.png')}}" alt="Image Description">
              </div>
              <!-- End Figure Image -->

              <!-- Figure Info -->
              <div class="g-mb-15">
                <h4 class="h4 g-color-black g-mb-5">C. Katia A. Figueroa Rodríguez</h4>
                <em class="d-block g-font-style-normal g-font-size-11 text-uppercase g-color-primary">Profesor Investigador Titular</em>
              </div>
            
            </figure>
            <!-- End Figure -->
          </div>
        </div>
        <!-- End Team Block -->

      </section>

      

      <!-- Timeline -->
      <section id="schedule" class="g-pt-150 g-pb-90 g-pb-170--md">
        <div class="container">
          <div class="text-center g-mb-70">
            <h2 class="text-uppercase g-line-height-1 g-font-weight-700 g-font-size-30 g-mb-30">PROGRAMA GENERAL DEL EVENTO</h2>

            <a href="{{asset('')}}/pdf/Conferencias-AGROALIMENTARIA2019.pdf" target="_blank">Descargar programa en PDF</a>

          </div>

          <!-- Nav tabs -->
          <ul class="nav justify-content-center text-uppercase u-nav-v5-3 u-nav-primary g-line-height-1_4 g-font-weight-700 g-font-size-14 g-brd-bottom--md g-brd-gray-light-v4 g-mb-80--md" role="tablist"
              data-target="days"
              data-tabs-mobile-type="slide-up-down"
              data-btn-classes="btn btn-md btn-block text-uppercase rounded-0 g-font-weight-700 u-btn-outline-primary">
            <li class="nav-item g-mx-10--md">
              <a class="nav-link g-theme-color-gray-dark-v1 g-color-primary--hover g-pb-15--md active" data-toggle="tab" href="#day-1" role="tab">JUEVES 22 DE AGOSTO</a>
            </li>
            <li class="nav-item g-mx-10--md">
              <a class="nav-link g-theme-color-gray-dark-v1 g-color-primary--hover g-pb-15--md" data-toggle="tab" href="#day-2" role="tab">VIERNES 23 DE AGOSTO</a>
            </li>
            <li class="nav-item g-mx-10--md">
              <a class="nav-link g-theme-color-gray-dark-v1 g-color-primary--hover g-pb-15--md" data-toggle="tab" href="#day-3" role="tab">SABADO 24 DE AGOSTO</a>
            </li>
          </ul>
          <!-- End Nav tabs -->

          <!-- Tab panes -->
          <div id="days" class="tab-content g-pt-20">
            <div class="tab-pane fade show active" id="day-1" role="tabpanel">
              <div class="u-timeline-v3-wrap">
                <a class="u-timeline-v3 d-block text-center text-md-left g-parent u-link-v5 g-mb-50" href="#">
                  <div class="g-hidden-sm-down u-timeline-v3__icon g-absolute-centered--y g-z-index-3 g-line-height-0 g-width-16 g-height-16 g-ml-minus-8">
                    <i class="d-inline-block g-width-16 g-height-16 g-bg-white g-brd-5 g-brd-style-solid g-brd-gray-light-v4 g-rounded-50"></i>
                  </div>

                  <div class="row mx-0">
                    <div class="col-md-3 g-order-2 g-order-1--sm d-flex align-self-center px-0">
                      <div class="u-heading-v1-4 g-brd-gray-light-v2 w-100">
                        <span class="text-center g-pos-rel d-block g-width-110 g-line-height-1_6 g-font-weight-600 g-color-white g-font-size-14 g-bg-gray-dark-v1 
                        g-bg-primary--parent-hover g-py-5 g-px-10 mx-auto g-ml-0--md g-transition-0_2 g-transition--ease-in g-mb-10 g-mb-0--md">13:00 hrs.</span>
                      </div>
                    </div>

                    <div class="col-md-9 g-order-1 g-order-2--sm px-0 g-mb-15 g-mb-0--md">
                      <div class="media d-block d-md-flex">
                    
                        <div class="media-body align-self-center">
                          <h4 class="text-uppercase g-font-weight-700 g-font-size-12 g-color-primary g-mb-5">Conferencia Magistral - La Nueva Visión del Sector Agropecuario                          </h4>
                          <h3 class="text-uppercase g-font-weight-700 g-font-size-23 g-mb-10">Dr. Víctor Manuel Villalobos Arámbula</h3>
                          <p class="g-color-gray-dark-v5 mb-0">Secretario de Agricultura y Desarrollo Rural</p>
                        </div>
                      </div>
                    </div>
                  </div>
                </a>

                <a class="u-timeline-v3 d-block text-center text-md-left g-parent u-link-v5 g-mb-50" href="#">
                  <div class="g-hidden-sm-down u-timeline-v3__icon g-absolute-centered--y g-z-index-3 g-line-height-0 g-width-16 g-height-16 g-ml-minus-8">
                    <i class="d-inline-block g-width-16 g-height-16 g-bg-white g-brd-5 g-brd-style-solid g-brd-gray-light-v4 g-rounded-50"></i>
                  </div>

                  <div class="row mx-0">
                    <div class="col-md-3 g-order-2 g-order-1--sm d-flex align-self-center px-0">
                      <div class="u-heading-v1-4 g-brd-gray-light-v2 w-100">
                        <span class="text-center g-pos-rel d-block g-width-110 g-line-height-1_6 g-font-weight-600 g-color-white 
                        g-font-size-14 g-bg-gray-dark-v1 g-bg-primary--parent-hover g-py-5 g-px-10 mx-auto g-ml-0--md g-transition-0_2 g-transition--ease-in 
                        g-mb-10 g-mb-0--md">14:00 hrs.</span>
                      </div>
                    </div>

                    <div class="col-md-9 g-order-1 g-order-2--sm px-0 g-mb-15 g-mb-0--md">
                      <div class="media d-block d-md-flex">
                       

                        <div class="media-body align-self-center">
                          <h4 class="text-uppercase g-font-weight-700 g-font-size-12 g-color-primary g-mb-5">Panel - Nueva Visión del Financiamiento Integral en el Campo
                            Mexicano</h4>
                                <p class="g-color-gray-dark-v5 mb-0"><b>Moderador: Ing. José Francisco Dovalina Lara</b> - Presidente del Comité Agropecuario de la Asociación de Bancos
                                  de México</p>
                                <p class="g-color-gray-dark-v5 mb-0"><b>- Dr. Javier Delgado Mendoza</b> - Director General de FND</p>

                                <p class="g-color-gray-dark-v5 mb-0"><b>- Act. Jesús Alan Elizondo Flores</b> - Director General de FIRA</p>
                        </div>
                      </div>
                    </div>
                  </div>
                </a>

                <a class="u-timeline-v3 d-block text-center text-md-left g-parent u-link-v5 g-mb-50" href="#">
                  <div class="g-hidden-sm-down u-timeline-v3__icon g-absolute-centered--y g-z-index-3 g-line-height-0 g-width-16 g-height-16 g-ml-minus-8">
                    <i class="d-inline-block g-width-16 g-height-16 g-bg-white g-brd-5 g-brd-style-solid g-brd-gray-light-v4 g-rounded-50"></i>
                  </div>

                  <div class="row mx-0">
                    <div class="col-md-3 g-order-2 g-order-1--sm d-flex align-self-center px-0">
                      <div class="u-heading-v1-4 g-brd-gray-light-v2 w-100">
                        <span class="text-center g-pos-rel d-block g-width-110 g-line-height-1_6 g-font-weight-600 g-color-white g-font-size-14
                         g-bg-gray-dark-v1 g-bg-primary--parent-hover g-py-5 g-px-10 mx-auto g-ml-0--md g-transition-0_2 
                         g-transition--ease-in g-mb-10 g-mb-0--md">16:30 hrs.</span>
                      </div>
                    </div>

                    <div class="col-md-9 g-order-1 g-order-2--sm px-0 g-mb-15 g-mb-0--md">
                      <div class="media d-block d-md-flex">
                      

                        <div class="media-body align-self-center">
                          <h4 class="text-uppercase g-font-weight-700 g-font-size-12 g-color-primary g-mb-5">Conferencia - Perspectivas sobre Sanidad Animal y la Inocuidad
                            Alimentaria en México</h4>
                          <h3 class="text-uppercase g-font-weight-700 g-font-size-23 g-mb-10">Dr. Francisco Javier Trujillo Arriaga</h3>
                          <p class="g-color-gray-dark-v5 mb-0">Director en Jefe SENASICA</p>
                        </div>
                      </div>
                    </div>
                  </div>
                </a>
                <a class="u-timeline-v3 d-block text-center text-md-left g-parent u-link-v5 g-mb-50" href="#">
                  <div class="g-hidden-sm-down u-timeline-v3__icon g-absolute-centered--y g-z-index-3 g-line-height-0 g-width-16 g-height-16 g-ml-minus-8">
                    <i class="d-inline-block g-width-16 g-height-16 g-bg-white g-brd-5 g-brd-style-solid g-brd-gray-light-v4 g-rounded-50"></i>
                  </div>

                  <div class="row mx-0">
                    <div class="col-md-3 g-order-2 g-order-1--sm d-flex align-self-center px-0">
                      <div class="u-heading-v1-4 g-brd-gray-light-v2 w-100">
                        <span class="text-center g-pos-rel d-block g-width-110 g-line-height-1_6 g-font-weight-600 g-color-white g-font-size-14 g-bg-gray-dark-v1
                         g-bg-primary--parent-hover g-py-5 g-px-10 mx-auto g-ml-0--md g-transition-0_2 g-transition--ease-in g-mb-10 g-mb-0--md">17:30 hrs.</span>
                      </div>
                    </div>

                    <div class="col-md-9 g-order-1 g-order-2--sm px-0 g-mb-15 g-mb-0--md">
                      <div class="media d-block d-md-flex">
                     

                        <div class="media-body align-self-center">
                          <h4 class="text-uppercase g-font-weight-700 g-font-size-12 g-color-primary g-mb-5">Conferencia - Innovación Israelí, Revolucionando el Campo</h4>
                          <h3 class="text-uppercase g-font-weight-700 g-font-size-23 g-mb-10">Uriel Raviv</h3>
                          <p class="g-color-gray-dark-v5 mb-0">Consejero Económico en México, Embajada de Israel</p>
                        </div>
                      </div>
                    </div>
                  </div>
                </a>
                <a class="u-timeline-v3 d-block text-center text-md-left g-parent u-link-v5 g-mb-50" href="#">
                  <div class="g-hidden-sm-down u-timeline-v3__icon g-absolute-centered--y g-z-index-3 g-line-height-0 g-width-16 g-height-16 g-ml-minus-8">
                    <i class="d-inline-block g-width-16 g-height-16 g-bg-white g-brd-5 g-brd-style-solid g-brd-gray-light-v4 g-rounded-50"></i>
                  </div>

                  <div class="row mx-0">
                    <div class="col-md-3 g-order-2 g-order-1--sm d-flex align-self-center px-0">
                      <div class="u-heading-v1-4 g-brd-gray-light-v2 w-100">
                        <span class="text-center g-pos-rel d-block g-width-110 g-line-height-1_6 g-font-weight-600 g-color-white g-font-size-14 g-bg-gray-dark-v1 
                        g-bg-primary--parent-hover g-py-5 g-px-10 mx-auto g-ml-0--md g-transition-0_2 g-transition--ease-in g-mb-10 g-mb-0--md">18:30 hrs.</span>
                      </div>
                    </div>

                    <div class="col-md-9 g-order-1 g-order-2--sm px-0 g-mb-15 g-mb-0--md">
                      <div class="media d-block d-md-flex">
                        

                        <div class="media-body align-self-center">
                          <h4 class="text-uppercase g-font-weight-700 g-font-size-12 g-color-primary g-mb-5">Conferencia Magistral - Retos y Oportunidades del Sector
                            Agroalimentario ante el Nuevo Escenario</h4>
                          <h3 class="text-uppercase g-font-weight-700 g-font-size-23 g-mb-10">Ing. Bosco de la Vega Valladolid</h3>
                          <p class="g-color-gray-dark-v5 mb-0">Presidente del Consejo Nacional Agropecuario</p>
                        </div>
                      </div>
                    </div>
                  </div>
                </a>

              </div>
            </div>

            <div class="tab-pane fade" id="day-2" role="tabpanel">
              <div class="u-timeline-v3-wrap">
                <a class="u-timeline-v3 d-block text-center text-md-left g-parent u-link-v5 g-mb-50" href="#">
                  <div class="g-hidden-sm-down u-timeline-v3__icon g-absolute-centered--y g-z-index-3 g-line-height-0 g-width-16 g-height-16 g-ml-minus-8">
                    <i class="d-inline-block g-width-16 g-height-16 g-bg-white g-brd-5 g-brd-style-solid g-brd-gray-light-v4 g-rounded-50"></i>
                  </div>

                  <div class="row mx-0">
                    <div class="col-md-3 g-order-2 g-order-1--sm d-flex align-self-center px-0">
                      <div class="u-heading-v1-4 g-brd-gray-light-v2 w-100">
                        <span class="text-center g-pos-rel d-block g-width-110 g-line-height-1_6 g-font-weight-600 g-color-white g-font-size-14 g-bg-gray-dark-v1 
                        g-bg-primary--parent-hover g-py-5 g-px-10 mx-auto g-ml-0--md g-transition-0_2 g-transition--ease-in g-mb-10 g-mb-0--md">9:00 hrs.</span>
                      </div>
                    </div>

                    <div class="col-md-9 g-order-1 g-order-2--sm px-0 g-mb-15 g-mb-0--md">
                      <div class="media d-block d-md-flex">
                       

                        <div class="media-body align-self-center">
                          <h4 class="text-uppercase g-font-weight-700 g-font-size-12 g-color-primary g-mb-5">Conferencia - Integración y Relevo Generacional de los Jóvenes al
                              Sector Agroindustrial</h4>
                          <h3 class="text-uppercase g-font-weight-700 g-font-size-23 g-mb-10">Mtro. Rodney Orlando Cordero Salas</h3>
                          <p class="g-color-gray-dark-v5 mb-0">Docente de Especies Zootécnicas, Escuela de Ciencias Agrarias.
                              Universidad Nacional de Costa Rica</p>
                        </div>
                      </div>
                    </div>
                  </div>
                </a>

                <a class="u-timeline-v3 d-block text-center text-md-left g-parent u-link-v5 g-mb-50" href="#">
                  <div class="g-hidden-sm-down u-timeline-v3__icon g-absolute-centered--y g-z-index-3 g-line-height-0 g-width-16 g-height-16 g-ml-minus-8">
                    <i class="d-inline-block g-width-16 g-height-16 g-bg-white g-brd-5 g-brd-style-solid g-brd-gray-light-v4 g-rounded-50"></i>
                  </div>

                  <div class="row mx-0">
                    <div class="col-md-3 g-order-2 g-order-1--sm d-flex align-self-center px-0">
                      <div class="u-heading-v1-4 g-brd-gray-light-v2 w-100">
                        <span class="text-center g-pos-rel d-block g-width-110 g-line-height-1_6 g-font-weight-600 g-color-white g-font-size-14 g-bg-gray-dark-v1 
                        g-bg-primary--parent-hover g-py-5 g-px-10 mx-auto g-ml-0--md g-transition-0_2 g-transition--ease-in g-mb-10 g-mb-0--md">10:00 hrs.</span>
                      </div>
                    </div>

                    <div class="col-md-9 g-order-1 g-order-2--sm px-0 g-mb-15 g-mb-0--md">
                      <div class="media d-block d-md-flex">
                    

                        <div class="media-body align-self-center">
                          <h4 class="text-uppercase g-font-weight-700 g-font-size-12 g-color-primary g-mb-5">Panel - Estrategia de Planeación Agropecuaria y Abasto Alimentario</h4>
                          
                          <p class="g-color-gray-dark-v5 mb-0"><b>- Dr. Arturo Puente González</b> - Director en Jefe de ASERCA</p>
                          <p class="g-color-gray-dark-v5 mb-0"><b>- Lic. Ignacio Ovalle Fernández</b> - Titular de Seguridad Alimentaria Mexicana</p>
                        </div>
                      </div>
                    </div>
                  </div>
                </a>

                <a class="u-timeline-v3 d-block text-center text-md-left g-parent u-link-v5 g-mb-50" href="#">
                  <div class="g-hidden-sm-down u-timeline-v3__icon g-absolute-centered--y g-z-index-3 g-line-height-0 g-width-16 g-height-16 g-ml-minus-8">
                    <i class="d-inline-block g-width-16 g-height-16 g-bg-white g-brd-5 g-brd-style-solid g-brd-gray-light-v4 g-rounded-50"></i>
                  </div>

                  <div class="row mx-0">
                    <div class="col-md-3 g-order-2 g-order-1--sm d-flex align-self-center px-0">
                      <div class="u-heading-v1-4 g-brd-gray-light-v2 w-100">
                        <span class="text-center g-pos-rel d-block g-width-110 g-line-height-1_6 g-font-weight-600 g-color-white g-font-size-14 g-bg-gray-dark-v1 
                        g-bg-primary--parent-hover g-py-5 g-px-10 mx-auto g-ml-0--md g-transition-0_2 g-transition--ease-in g-mb-10 g-mb-0--md">11:00 hrs.</span>
                      </div>
                    </div>

                    <div class="col-md-9 g-order-1 g-order-2--sm px-0 g-mb-15 g-mb-0--md">
                      <div class="media d-block d-md-flex">
                      
                        <div class="media-body align-self-center">
                          <h4 class="text-uppercase g-font-weight-700 g-font-size-12 g-color-primary g-mb-5">Conferencia - Innovación en las Cadenas Agroalimentarias. Oportunidades de la
                              Cooperación Italia - Zacatecas</h4>
                          <h3 class="text-uppercase g-font-weight-700 g-font-size-23 g-mb-10">Mtro. José Luis Rhi Sausi</h3>
                          <p class="g-color-gray-dark-v5 mb-0">Coordinador del Foro Ítalo - Latinoamericano sobre Pymes, IILA</p>
                        </div>
                      </div>
                    </div>
                  </div>
                </a>

                <a class="u-timeline-v3 d-block text-center text-md-left g-parent u-link-v5 g-mb-50" href="#">
                    <div class="g-hidden-sm-down u-timeline-v3__icon g-absolute-centered--y g-z-index-3 g-line-height-0 g-width-16 g-height-16 g-ml-minus-8">
                      <i class="d-inline-block g-width-16 g-height-16 g-bg-white g-brd-5 g-brd-style-solid g-brd-gray-light-v4 g-rounded-50"></i>
                    </div>
  
                    <div class="row mx-0">
                      <div class="col-md-3 g-order-2 g-order-1--sm d-flex align-self-center px-0">
                        <div class="u-heading-v1-4 g-brd-gray-light-v2 w-100">
                          <span class="text-center g-pos-rel d-block g-width-110 g-line-height-1_6 g-font-weight-600 g-color-white g-font-size-14 g-bg-gray-dark-v1 
                          g-bg-primary--parent-hover g-py-5 g-px-10 mx-auto g-ml-0--md g-transition-0_2 g-transition--ease-in g-mb-10 g-mb-0--md">12:30 hrs.</span>
                        </div>
                      </div>
  
                      <div class="col-md-9 g-order-1 g-order-2--sm px-0 g-mb-15 g-mb-0--md">
                        <div class="media d-block d-md-flex">
                         
  
                          <div class="media-body align-self-center">
                              <h4 class="text-uppercase g-font-weight-700 g-font-size-12 g-color-primary g-mb-5">Conferencia - Puente Internacional de PHARR</h4>
                              <h3 class="text-uppercase g-font-weight-700 g-font-size-23 g-mb-10">Luis Bazán</h3>
                              <p class="g-color-gray-dark-v5 mb-0">Director General Puente Internacional de PHARR</p>
                          </div>
                        </div>
                      </div>
                    </div>
                  </a>

                  <a class="u-timeline-v3 d-block text-center text-md-left g-parent u-link-v5 g-mb-50" href="#">
                      <div class="g-hidden-sm-down u-timeline-v3__icon g-absolute-centered--y g-z-index-3 g-line-height-0 g-width-16 g-height-16 g-ml-minus-8">
                        <i class="d-inline-block g-width-16 g-height-16 g-bg-white g-brd-5 g-brd-style-solid g-brd-gray-light-v4 g-rounded-50"></i>
                      </div>
    
                      <div class="row mx-0">
                        <div class="col-md-3 g-order-2 g-order-1--sm d-flex align-self-center px-0">
                          <div class="u-heading-v1-4 g-brd-gray-light-v2 w-100">
                            <span class="text-center g-pos-rel d-block g-width-110 g-line-height-1_6 g-font-weight-600 g-color-white g-font-size-14 g-bg-gray-dark-v1 
                            g-bg-primary--parent-hover g-py-5 g-px-10 mx-auto g-ml-0--md g-transition-0_2 g-transition--ease-in g-mb-10 g-mb-0--md">13:30 hrs.</span>
                          </div>
                        </div>
    
                        <div class="col-md-9 g-order-1 g-order-2--sm px-0 g-mb-15 g-mb-0--md">
                          <div class="media d-block d-md-flex">
                           
    
                            <div class="media-body align-self-center">
                                <h4 class="text-uppercase g-font-weight-700 g-font-size-12 g-color-primary g-mb-5">Conferencia - Estrategia Integral del Desarrollo Rural.
                                  </h4>
                                   <h3 class="text-uppercase g-font-weight-700 g-font-size-23 g-mb-10"> Dr. Salvador Fernández Rivera</h3>
                                   <p class="g-color-gray-dark-v5 mb-0">Coordinador General de Desarrollo Rural SADER</p>
                            </div>
                          </div>
                        </div>
                      </div>
                    </a>


                    <a class="u-timeline-v3 d-block text-center text-md-left g-parent u-link-v5 g-mb-50" href="#">
                        <div class="g-hidden-sm-down u-timeline-v3__icon g-absolute-centered--y g-z-index-3 g-line-height-0 g-width-16 g-height-16 g-ml-minus-8">
                          <i class="d-inline-block g-width-16 g-height-16 g-bg-white g-brd-5 g-brd-style-solid g-brd-gray-light-v4 g-rounded-50"></i>
                        </div>
      
                        <div class="row mx-0">
                          <div class="col-md-3 g-order-2 g-order-1--sm d-flex align-self-center px-0">
                            <div class="u-heading-v1-4 g-brd-gray-light-v2 w-100">
                              <span class="text-center g-pos-rel d-block g-width-110 g-line-height-1_6 g-font-weight-600 g-color-white g-font-size-14 g-bg-gray-dark-v1 
                              g-bg-primary--parent-hover g-py-5 g-px-10 mx-auto g-ml-0--md g-transition-0_2 g-transition--ease-in g-mb-10 g-mb-0--md">14:30 hrs.</span>
                            </div>
                          </div>
      
                          <div class="col-md-9 g-order-1 g-order-2--sm px-0 g-mb-15 g-mb-0--md">
                            <div class="media d-block d-md-flex">
                             
                              <div class="media-body align-self-center">
                                  <h4 class="text-uppercase g-font-weight-700 g-font-size-12 g-color-primary g-mb-5">Conferencia - Promoción del Comercio y la Atracción de Inversiones
                                      Tratado Comercial América del Norte (México, Estados Unidos y Canadá)</h4>
                                  <h3 class="text-uppercase g-font-weight-700 g-font-size-23 g-mb-10">Dra. Luz María de la Mora Sánchez</h3>
                                  <p class="g-color-gray-dark-v5 mb-0">Subsecretaría de Comercio Exterior, Secretaría de Economía</p>
                              </div>
                            </div>
                          </div>
                        </div>
                      </a>

                      <a class="u-timeline-v3 d-block text-center text-md-left g-parent u-link-v5 g-mb-50" href="#">
                          <div class="g-hidden-sm-down u-timeline-v3__icon g-absolute-centered--y g-z-index-3 g-line-height-0 g-width-16 g-height-16 g-ml-minus-8">
                            <i class="d-inline-block g-width-16 g-height-16 g-bg-white g-brd-5 g-brd-style-solid g-brd-gray-light-v4 g-rounded-50"></i>
                          </div>
        
                          <div class="row mx-0">
                            <div class="col-md-3 g-order-2 g-order-1--sm d-flex align-self-center px-0">
                              <div class="u-heading-v1-4 g-brd-gray-light-v2 w-100">
                                <span class="text-center g-pos-rel d-block g-width-110 g-line-height-1_6 g-font-weight-600 g-color-white g-font-size-14 g-bg-gray-dark-v1 
                                g-bg-primary--parent-hover g-py-5 g-px-10 mx-auto g-ml-0--md g-transition-0_2 g-transition--ease-in g-mb-10 g-mb-0--md">16:30 hrs.</span>
                              </div>
                            </div>
        
                            <div class="col-md-9 g-order-1 g-order-2--sm px-0 g-mb-15 g-mb-0--md">
                              <div class="media d-block d-md-flex">
                               
        
                                <div class="media-body align-self-center">
                                    <h4 class="text-uppercase g-font-weight-700 g-font-size-12 g-color-primary g-mb-5">Panel - Tomate y su Comercialización</h4>
                                    <h3 class="text-uppercase g-font-weight-700 g-font-size-23 g-mb-10">Lic. Fernando Ruíz Huarte</h3>
                                    <p class="g-color-gray-dark-v5 mb-0">Director General COMCE Nacional</p>
                                </div>
                              </div>
                            </div>
                          </div>
                        </a>

                        <a class="u-timeline-v3 d-block text-center text-md-left g-parent u-link-v5 g-mb-50" href="#">
                            <div class="g-hidden-sm-down u-timeline-v3__icon g-absolute-centered--y g-z-index-3 g-line-height-0 g-width-16 g-height-16 g-ml-minus-8">
                              <i class="d-inline-block g-width-16 g-height-16 g-bg-white g-brd-5 g-brd-style-solid g-brd-gray-light-v4 g-rounded-50"></i>
                            </div>
          
                            <div class="row mx-0">
                              <div class="col-md-3 g-order-2 g-order-1--sm d-flex align-self-center px-0">
                                <div class="u-heading-v1-4 g-brd-gray-light-v2 w-100">
                                  <span class="text-center g-pos-rel d-block g-width-110 g-line-height-1_6 g-font-weight-600 g-color-white g-font-size-14 g-bg-gray-dark-v1 
                                  g-bg-primary--parent-hover g-py-5 g-px-10 mx-auto g-ml-0--md g-transition-0_2 g-transition--ease-in g-mb-10 g-mb-0--md">17:30 hrs.</span>
                                </div>
                              </div>
          
                              <div class="col-md-9 g-order-1 g-order-2--sm px-0 g-mb-15 g-mb-0--md">
                                <div class="media d-block d-md-flex">
                                 
          
                                  <div class="media-body align-self-center">
                                      <h4 class="text-uppercase g-font-weight-700 g-font-size-12 g-color-primary g-mb-5">Conferencia - Comercio Bilateral de Productos Agrícolas MEX - USA</h4>
                                      <h3 class="text-uppercase g-font-weight-700 g-font-size-23 g-mb-10">C. Orestes Vasquez</h3>
                                      <p class="g-color-gray-dark-v5 mb-0">	Director de la Oficina Agrocomercial del Consulado de EU en Monterrey</p>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </a>

                          <a class="u-timeline-v3 d-block text-center text-md-left g-parent u-link-v5 g-mb-50" href="#">
                              <div class="g-hidden-sm-down u-timeline-v3__icon g-absolute-centered--y g-z-index-3 g-line-height-0 g-width-16 g-height-16 g-ml-minus-8">
                                <i class="d-inline-block g-width-16 g-height-16 g-bg-white g-brd-5 g-brd-style-solid g-brd-gray-light-v4 g-rounded-50"></i>
                              </div>
            
                              <div class="row mx-0">
                                <div class="col-md-3 g-order-2 g-order-1--sm d-flex align-self-center px-0">
                                  <div class="u-heading-v1-4 g-brd-gray-light-v2 w-100">
                                    <span class="text-center g-pos-rel d-block g-width-110 g-line-height-1_6 g-font-weight-600 g-color-white g-font-size-14 g-bg-gray-dark-v1 
                                    g-bg-primary--parent-hover g-py-5 g-px-10 mx-auto g-ml-0--md g-transition-0_2 g-transition--ease-in g-mb-10 g-mb-0--md">18:30 hrs.</span>
                                  </div>
                                </div>
            
                                <div class="col-md-9 g-order-1 g-order-2--sm px-0 g-mb-15 g-mb-0--md">
                                  <div class="media d-block d-md-flex">
                                   
                                    <div class="media-body align-self-center">
                                      <h4 class="text-uppercase g-font-weight-700 g-font-size-12 g-color-primary g-mb-5">Panel - Certificaciones con enfoque de mercado global, “Sanidad, inocuidad,
                                          medio ambiente y responsabilidad social”</h4>
                                          <p class="g-color-gray-dark-v5 mb-0"><b>- Lic. Humberto Martínez Romero</b> - Director General del Grupo Rosmar</p>
                                          <p class="g-color-gray-dark-v5 mb-0"><b>- LAN Juan Jesús Sánchez Miller</b> - Director SCS Global Services México</p>
                                    </div>
                                  </div>
                                </div>
                              </div>
                            </a>

              
              </div>
            </div>

            <div class="tab-pane fade" id="day-3" role="tabpanel">
              <div class="u-timeline-v3-wrap">
                <a class="u-timeline-v3 d-block text-center text-md-left g-parent u-link-v5 g-mb-50" href="#">
                  <div class="g-hidden-sm-down u-timeline-v3__icon g-absolute-centered--y g-z-index-3 g-line-height-0 g-width-16 g-height-16 g-ml-minus-8">
                    <i class="d-inline-block g-width-16 g-height-16 g-bg-white g-brd-5 g-brd-style-solid g-brd-gray-light-v4 g-rounded-50"></i>
                  </div>

                  <div class="row mx-0">
                    <div class="col-md-3 g-order-2 g-order-1--sm d-flex align-self-center px-0">
                      <div class="u-heading-v1-4 g-brd-gray-light-v2 w-100">
                        <span class="text-center g-pos-rel d-block g-width-110 g-line-height-1_6 g-font-weight-600 g-color-white g-font-size-14 g-bg-gray-dark-v1 
                        g-bg-primary--parent-hover g-py-5 g-px-10 mx-auto g-ml-0--md g-transition-0_2 g-transition--ease-in g-mb-10 g-mb-0--md">10:00 hrs.</span>
                      </div>
                    </div>

                    <div class="col-md-9 g-order-1 g-order-2--sm px-0 g-mb-15 g-mb-0--md">
                      <div class="media d-block d-md-flex">
                        

                        <div class="media-body align-self-center">
                          <h4 class="text-uppercase g-font-weight-700 g-font-size-12 g-color-primary g-mb-5">Conferencia - Inteligencia Satelital; Tecnología en Irrigación para
                              Cultivos de Campo Abierto</h4>
                          <h3 class="text-uppercase g-font-weight-700 g-font-size-23 g-mb-10">Ing. Francisco Javier Angulo Rendón</h3>
                          <p class="g-color-gray-dark-v5 mb-0">Gerente de Agronomía y Soporte al Producto de Rivulis-Eurodrip</p>
                        </div>
                      </div>
                    </div>
                  </div>
                </a>

                <a class="u-timeline-v3 d-block text-center text-md-left g-parent u-link-v5 g-mb-50" href="#">
                  <div class="g-hidden-sm-down u-timeline-v3__icon g-absolute-centered--y g-z-index-3 g-line-height-0 g-width-16 g-height-16 g-ml-minus-8">
                    <i class="d-inline-block g-width-16 g-height-16 g-bg-white g-brd-5 g-brd-style-solid g-brd-gray-light-v4 g-rounded-50"></i>
                  </div>

                  <div class="row mx-0">
                    <div class="col-md-3 g-order-2 g-order-1--sm d-flex align-self-center px-0">
                      <div class="u-heading-v1-4 g-brd-gray-light-v2 w-100">
                        <span class="text-center g-pos-rel d-block g-width-110 g-line-height-1_6 g-font-weight-600 g-color-white g-font-size-14 g-bg-gray-dark-v1 
                        g-bg-primary--parent-hover g-py-5 g-px-10 mx-auto g-ml-0--md g-transition-0_2 g-transition--ease-in g-mb-10 g-mb-0--md">11:00 hrs.</span>
                      </div>
                    </div>

                    <div class="col-md-9 g-order-1 g-order-2--sm px-0 g-mb-15 g-mb-0--md">
                      <div class="media d-block d-md-flex">
                       

                        <div class="media-body align-self-center">
                          <h4 class="text-uppercase g-font-weight-700 g-font-size-12 g-color-primary g-mb-5">Conferencia Magistral - Desde Semillas para la Paz, hacia Sistemas
                              Integrales para la Prosperidad</h4>
                          <h3 class="text-uppercase g-font-weight-700 g-font-size-23 g-mb-10">Dr. Bram Govaerts</h3>
                          <p class="g-color-gray-dark-v5 mb-0">Director Global de Innovación Estratégica, CIMMYT</p>
                        </div>
                      </div>
                    </div>
                  </div>
                </a>

                <a class="u-timeline-v3 d-block text-center text-md-left g-parent u-link-v5 g-mb-50" href="#">
                  <div class="g-hidden-sm-down u-timeline-v3__icon g-absolute-centered--y g-z-index-3 g-line-height-0 g-width-16 g-height-16 g-ml-minus-8">
                    <i class="d-inline-block g-width-16 g-height-16 g-bg-white g-brd-5 g-brd-style-solid g-brd-gray-light-v4 g-rounded-50"></i>
                  </div>

                  <div class="row mx-0">
                    <div class="col-md-3 g-order-2 g-order-1--sm d-flex align-self-center px-0">
                      <div class="u-heading-v1-4 g-brd-gray-light-v2 w-100">
                        <span class="text-center g-pos-rel d-block g-width-110 g-line-height-1_6 g-font-weight-600 g-color-white g-font-size-14 g-bg-gray-dark-v1 
                        g-bg-primary--parent-hover g-py-5 g-px-10 mx-auto g-ml-0--md g-transition-0_2 g-transition--ease-in g-mb-10 g-mb-0--md">12:00 hrs.</span>
                      </div>
                    </div>

                    <div class="col-md-9 g-order-1 g-order-2--sm px-0 g-mb-15 g-mb-0--md">
                      <div class="media d-block d-md-flex">
                      

                        <div class="media-body align-self-center">
                          <h4 class="text-uppercase g-font-weight-700 g-font-size-12 g-color-primary g-mb-5">Conferencia - Agricultura Sostenible</h4>
                          <h3 class="text-uppercase g-font-weight-700 g-font-size-23 g-mb-10">Dr. Benjamín Figueroa Sandoval</h3>
                          <p class="g-color-gray-dark-v5 mb-0">Conferensista Magistral en el Congreso Nacional de Educación,
                              AMIP y Profesor Investigador de COLPOS</p>
                        </div>
                      </div>
                    </div>
                  </div>
                </a>
              </div>
            </div>
          </div>
          <!-- End Tab panes -->
          
        </div>
      </section>
      <!-- End Timeline -->

  

     

      <!-- Section Content -->
      <section id="expositores" class="g-theme-bg-gray-light-v1 g-py-100">
        <div>
          <h3>EXPOSITORES</h3>
        </div>
      </section>
      <!-- End Section Content -->

     

        <!-- Section Content -->
        <section id="sponsors" class="u-bg-overlay g-bg-img-hero  g-py-100" style="background-image: url({{asset('/img/recursosAgro/bgblanco.png')}});">
          <div class="container u-bg-overlay__inner">
           
            <div class="js-carousel text-center g-line-height-0 g-pb-50"
                 data-infinite="true"
                 data-autoplay="true"
                 data-speed="5000"
                 data-rows="2"
                 data-slides-show="6"
                 data-arrows-classes="u-arrow-v1 g-absolute-centered--y g-bottom-0 g-width-40 g-height-40 g-font-size-20 g-color-white g-color-primary--hover g-bg-primary g-bg-white--hover g-mt-minus-25 g-transition-0_2 g-transition--ease-in"
                 data-arrow-left-classes="fa fa-angle-left g-left-0"
                 data-arrow-right-classes="fa fa-angle-right g-right-0"
                 data-pagi-classes="u-carousel-indicators-v1--white g-absolute-centered--x g-bottom-0"
                 data-responsive='[{
                   "breakpoint": 1200,
                   "settings": {
                     "slidesToShow": 5
                   }
                 }, {
                   "breakpoint": 992,
                   "settings": {
                     "slidesToShow": 4
                   }
                 }, {
                   "breakpoint": 768,
                   "settings": {
                     "slidesToShow": 3
                   }
                 }, {
                   "breakpoint": 576,
                   "settings": {
                     "slidesToShow": 2
                   }
                 }]'>
              <a class="js-slide u-bg-overlay g-parent g-overflow-hidden " href="{{asset('')}}/img/recursosAgro/patrocinadores/dependencias/dep11.png"
                 data-fancybox="fancyGallery1">
                <img class="img-fluid g-transform-scale-1_1--parent-hover g-transition-0_3 g-transition--ease-in" src="{{asset('')}}/img/recursosAgro/patrocinadores/dependencias/dep1.png" alt="Image description">
              </a>
  
            
             
            </div>
          </div>
          <div class="container u-bg-overlay__inner">
           
            <div class="js-carousel text-center g-line-height-0 g-pb-50"
                 data-infinite="true"
                 data-autoplay="true"
                 data-speed="5000"
                 data-rows="2"
                 data-slides-show="6"
                 data-arrows-classes="u-arrow-v1 g-absolute-centered--y g-bottom-0 g-width-40 g-height-40 g-font-size-20 g-color-white g-color-primary--hover g-bg-primary g-bg-white--hover g-mt-minus-25 g-transition-0_2 g-transition--ease-in"
                 data-arrow-left-classes="fa fa-angle-left g-left-0"
                 data-arrow-right-classes="fa fa-angle-right g-right-0"
                 data-pagi-classes="u-carousel-indicators-v1--white g-absolute-centered--x g-bottom-0"
                 data-responsive='[{
                   "breakpoint": 1200,
                   "settings": {
                     "slidesToShow": 5
                   }
                 }, {
                   "breakpoint": 992,
                   "settings": {
                     "slidesToShow": 4
                   }
                 }, {
                   "breakpoint": 768,
                   "settings": {
                     "slidesToShow": 3
                   }
                 }, {
                   "breakpoint": 576,
                   "settings": {
                     "slidesToShow": 2
                   }
                 }]'>
           
              <a class="js-slide u-bg-overlay g-parent g-overflow-hidden" href="{{asset('')}}/img/recursosAgro/patrocinadores/dependencias/dep22.png"
                 data-fancybox="fancyGallery1">
                <img class="img-fluid g-transform-scale-1_1--parent-hover g-transition-0_3 g-transition--ease-in" src="{{asset('')}}/img/recursosAgro/patrocinadores/dependencias/dep2.png" alt="Image description">
              </a>
  
              <a class="js-slide u-bg-overlay g-parent g-overflow-hidden" href="{{asset('')}}/img/recursosAgro/patrocinadores/dependencias/dep33.png"
                 data-fancybox="fancyGallery1">
                <img class="img-fluid g-transform-scale-1_1--parent-hover g-transition-0_3 g-transition--ease-in" src="{{asset('')}}/img/recursosAgro/patrocinadores/dependencias/dep3.png" alt="Image description">
              </a>
  
              <a class="js-slide u-bg-overlay g-parent g-overflow-hidden" href="{{asset('')}}/img/recursosAgro/patrocinadores/dependencias/dep4.png"
                 data-fancybox="fancyGallery1">
                <img class="img-fluid g-transform-scale-1_1--parent-hover g-transition-0_3 g-transition--ease-in" src="{{asset('')}}/img/recursosAgro/patrocinadores/dependencias/dep44.png" alt="Image description">
              </a>
  
             
            </div>
          </div>
        </section>
        <!-- End Section Content -->

          <!-- Section Content -->
          <section  class="u-bg-overlay g-bg-img-hero  g-py-100" style="background-image: url({{asset('')}}/img/recursosAgro/bgblanco.png);">
            <div class="container u-bg-overlay__inner">
              <div class="text-center g-mb-70">
                <h2 class="text-uppercase g-line-height-1 g-font-weight-700 g-font-size-30 g-color-white g-mb-30">Patrocinadores Corporativos</h2>
    
                
              </div>
    
              <div class="js-carousel text-center g-line-height-0 g-pb-50"
                   data-infinite="true"
                   data-autoplay="true"
                   data-speed="5000"
                   data-rows="2"
                   data-slides-show="6"
                   data-arrows-classes="u-arrow-v1 g-absolute-centered--y g-bottom-0 g-width-40 g-height-40 g-font-size-20 g-color-white g-color-primary--hover g-bg-primary g-bg-white--hover g-mt-minus-25 g-transition-0_2 g-transition--ease-in"
                   data-arrow-left-classes="fa fa-angle-left g-left-0"
                   data-arrow-right-classes="fa fa-angle-right g-right-0"
                   data-pagi-classes="u-carousel-indicators-v1--white g-absolute-centered--x g-bottom-0"
                   data-responsive='[{
                     "breakpoint": 1200,
                     "settings": {
                       "slidesToShow": 5
                     }
                   }, {
                     "breakpoint": 992,
                     "settings": {
                       "slidesToShow": 4
                     }
                   }, {
                     "breakpoint": 768,
                     "settings": {
                       "slidesToShow": 3
                     }
                   }, {
                     "breakpoint": 576,
                     "settings": {
                       "slidesToShow": 2
                     }
                   }]'>
                <a class="js-slide u-bg-overlay g-parent g-overflow-hidden " href="{{asset('')}}/img/recursosAgro/patrocinadores/corporativos/pat11.png"
                   data-fancybox="fancyGallery1">
                  <img class="img-fluid g-transform-scale-1_1--parent-hover g-transition-0_3 g-transition--ease-in" src="{{asset('')}}/img/recursosAgro/patrocinadores/corporativos/pat1.png" alt="Image description">
                </a>
    
                <a class="js-slide u-bg-overlay g-parent g-overflow-hidden " href="{{asset('')}}/img/recursosAgro/patrocinadores/corporativos/pat2.png"
                   data-fancybox="fancyGallery1">
                  <img class="img-fluid g-transform-scale-1_1--parent-hover g-transition-0_3 g-transition--ease-in" src="{{asset('')}}/img/recursosAgro/patrocinadores/corporativos/pat22.png" alt="Image description">
                </a>
    
                <a class="js-slide u-bg-overlay g-parent g-overflow-hidden " href="{{asset('')}}/img/recursosAgro/patrocinadores/corporativos/pat33.png"
                   data-fancybox="fancyGallery1">
                  <img class="img-fluid g-transform-scale-1_1--parent-hover g-transition-0_3 g-transition--ease-in" src="{{asset('')}}/img/recursosAgro/patrocinadores/corporativos/pat3.png" alt="Image description">
                </a>
    
                <a class="js-slide u-bg-overlay g-parent g-overflow-hidden " href="{{asset('')}}/img/recursosAgro/patrocinadores/corporativos/pat44.png"
                   data-fancybox="fancyGallery1">
                  <img class="img-fluid g-transform-scale-1_1--parent-hover g-transition-0_3 g-transition--ease-in" src="{{asset('')}}/img/recursosAgro/patrocinadores/corporativos/pat4.png" alt="Image description">
                </a>
    
                <a class="js-slide u-bg-overlay g-parent g-overflow-hidden " href="{{asset('')}}/img/recursosAgro/patrocinadores/corporativos/pat55.png"
                   data-fancybox="fancyGallery1">
                  <img class="img-fluid g-transform-scale-1_1--parent-hover g-transition-0_3 g-transition--ease-in" src="{{asset('')}}/img/recursosAgro/patrocinadores/corporativos/pat5.png" alt="Image description">
                </a>
    
                <a class="js-slide u-bg-overlay g-parent g-overflow-hidden " href="{{asset('')}}/img/recursosAgro/patrocinadores/corporativos/pat66.png"
                   data-fancybox="fancyGallery1">
                  <img class="img-fluid g-transform-scale-1_1--parent-hover g-transition-0_3 g-transition--ease-in" src="{{asset('')}}/img/recursosAgro/patrocinadores/corporativos/pat6.png" alt="Image description">
                </a>
    
                <a class="js-slide u-bg-overlay g-parent g-overflow-hidden " href="{{asset('')}}/img/recursosAgro/patrocinadores/corporativos/pat77.png"
                   data-fancybox="fancyGallery1">
                  <img class="img-fluid g-transform-scale-1_1--parent-hover g-transition-0_3 g-transition--ease-in" src="{{asset('')}}/img/recursosAgro/patrocinadores/corporativos/pat7.png" alt="Image description">
                </a>
    
                <a class="js-slide u-bg-overlay g-parent g-overflow-hidden " href="{{asset('')}}/img/recursosAgro/patrocinadores/corporativos/pat88.png"
                   data-fancybox="fancyGallery1">
                  <img class="img-fluid g-transform-scale-1_1--parent-hover g-transition-0_3 g-transition--ease-in" src="{{asset('')}}/img/recursosAgro/patrocinadores/corporativos/pat8.png" alt="Image description">
                </a>
    
                <a class="js-slide u-bg-overlay g-parent g-overflow-hidden " href="{{asset('')}}/img/recursosAgro/patrocinadores/corporativos/pat99.png"
                   data-fancybox="fancyGallery1">
                  <img class="img-fluid g-transform-scale-1_1--parent-hover g-transition-0_3 g-transition--ease-in" src="{{asset('')}}/img/recursosAgro/patrocinadores/corporativos/pat9.png" alt="Image description">
                </a>
    
               
              </div>
            </div>
          </section>
          <!-- End Section Content -->

          <!-- Section Content -->
          <section  class="u-bg-overlay g-bg-img-hero  g-py-100" style="background-image: url({{asset('/img/recursosAgro/bgblanco.png')}});">
            <div class="container u-bg-overlay__inner">
              <div class="text-center g-mb-70">
                <h2 class="text-uppercase g-line-height-1 g-font-weight-700 g-font-size-30 g-color-white g-mb-30">Invitados Especiales</h2>
    
             
              </div>
    
              <div class="js-carousel text-center g-line-height-0 g-pb-50"
                   data-infinite="true"
                   data-autoplay="true"
                   data-speed="5000"
                   data-rows="2"
                   data-slides-show="6"
                   data-arrows-classes="u-arrow-v1 g-absolute-centered--y g-bottom-0 g-width-40 g-height-40 g-font-size-20 g-color-white g-color-primary--hover g-bg-primary g-bg-white--hover g-mt-minus-25 g-transition-0_2 g-transition--ease-in"
                   data-arrow-left-classes="fa fa-angle-left g-left-0"
                   data-arrow-right-classes="fa fa-angle-right g-right-0"
                   data-pagi-classes="u-carousel-indicators-v1--white g-absolute-centered--x g-bottom-0"
                   data-responsive='[{
                     "breakpoint": 1200,
                     "settings": {
                       "slidesToShow": 5
                     }
                   }, {
                     "breakpoint": 992,
                     "settings": {
                       "slidesToShow": 4
                     }
                   }, {
                     "breakpoint": 768,
                     "settings": {
                       "slidesToShow": 3
                     }
                   }, {
                     "breakpoint": 576,
                     "settings": {
                       "slidesToShow": 2
                     }
                   }]'>
                <a class="js-slide u-bg-overlay g-parent g-overflow-hidden " href="{{asset('')}}/img/recursosAgro/patrocinadores/invitados/inv11.png"
                   data-fancybox="fancyGallery1">
                  <img class="img-fluid g-transform-scale-1_1--parent-hover g-transition-0_3 g-transition--ease-in" src="{{asset('')}}/img/recursosAgro/patrocinadores/invitados/inv1.png" alt="Image description">
                </a>
    
                <a class="js-slide u-bg-overlay g-parent g-overflow-hidden " href="{{asset('')}}/img/recursosAgro/patrocinadores/invitados/inv22.png"
                   data-fancybox="fancyGallery1">
                  <img class="img-fluid g-transform-scale-1_1--parent-hover g-transition-0_3 g-transition--ease-in" src="{{asset('')}}/img/recursosAgro/patrocinadores/invitados/inv2.png" alt="Image description">
                </a>
    
               
              </div>
            </div>
          </section>
          <!-- End Section Content -->

          

      <section id="registro" class="g-py-100">
          <div class="text-center g-mb-70">
              <h2 class="text-uppercase g-line-height-1 g-font-weight-700 g-font-size-30 g-color-white g-mb-30">REGISTRO</h2>
  
              <p class="g-color-white">Registrate y forma parte de este gran evento</p>
            </div>

            <div class="shortcode-html">
                <!-- Textual Inputs -->
        <form class="g-brd-around g-brd-gray-light-v4 g-pa-30 g-mb-30">
            <div class="form-group row g-mb-25">
              <label for="example-text-input" class="col-2 col-form-label">Text</label>
              <div class="col-10">
                <input class="form-control rounded-0 form-control-md" type="text" value="Artisanal kale" id="example-text-input">
              </div>
            </div>
            <div class="form-group row g-mb-25">
              <label for="example-search-input" class="col-2 col-form-label">Search</label>
              <div class="col-10">
                <input class="form-control rounded-0 form-control-md" type="search" value="How do I shoot web" id="example-search-input">
              </div>
            </div>
            <div class="form-group row g-mb-25">
              <label for="example-email-input" class="col-2 col-form-label">Email</label>
              <div class="col-10">
                <input class="form-control rounded-0 form-control-md" type="email" value="bootstrap@example.com" id="example-email-input">
              </div>
            </div>
            <div class="form-group row g-mb-25">
              <label for="example-url-input" class="col-2 col-form-label">URL</label>
              <div class="col-10">
                <input class="form-control rounded-0 form-control-md" type="url" value="https://getbootstrap.com" id="example-url-input">
              </div>
            </div>
            <div class="form-group row g-mb-25">
              <label for="example-tel-input" class="col-2 col-form-label">Telephone</label>
              <div class="col-10">
                <input class="form-control rounded-0 form-control-md" type="tel" value="1-(555)-555-5555" id="example-tel-input">
              </div>
            </div>
            <div class="form-group row g-mb-25">
              <label for="example-password-input" class="col-2 col-form-label">Password</label>
              <div class="col-10">
                <input class="form-control rounded-0 form-control-md" type="password" value="hunter2" id="example-password-input">
              </div>
            </div>
            <div class="form-group row g-mb-25">
              <label for="example-number-input" class="col-2 col-form-label">Number</label>
              <div class="col-10">
                <input class="form-control rounded-0 form-control-md" type="number" value="42" id="example-number-input">
              </div>
            </div>
            <div class="form-group row g-mb-25">
              <label for="example-datetime-local-input" class="col-2 col-form-label">Date and time</label>
              <div class="col-10">
                <input class="form-control rounded-0 form-control-md" type="datetime-local" value="2011-08-19T13:45:00" id="example-datetime-local-input">
              </div>
            </div>
            <div class="form-group row g-mb-25">
              <label for="example-date-input" class="col-2 col-form-label">Date</label>
              <div class="col-10">
                <input class="form-control rounded-0 form-control-md" type="date" value="2011-08-19" id="example-date-input">
              </div>
            </div>
            <div class="form-group row g-mb-25">
              <label for="example-month-input" class="col-2 col-form-label">Month</label>
              <div class="col-10">
                <input class="form-control rounded-0 form-control-md" type="month" value="2011-08" id="example-month-input">
              </div>
            </div>
            <div class="form-group row g-mb-25">
              <label for="example-week-input" class="col-2 col-form-label">Week</label>
              <div class="col-10">
                <input class="form-control rounded-0 form-control-md" type="week" value="2011-W33" id="example-week-input">
              </div>
            </div>
            <div class="form-group row g-mb-25">
              <label for="example-time-input" class="col-2 col-form-label">Time</label>
              <div class="col-10">
                <input class="form-control rounded-0 form-control-md" type="time" value="13:45:00" id="example-time-input">
              </div>
            </div>
            <div class="form-group row g-mb-25">
              <label for="example-color-input" class="col-2 col-form-label">Color</label>
              <div class="col-10">
                <input class="form-control rounded-0" type="color" value="#563d7c" id="example-color-input">
              </div>
            </div>
          </form>

            </div>
        
        <!-- End Textual Inputs -->
      </section>

  
      <!-- Footer Background -->
<div  id="contact" class="u-bg-overlay text-center g-color-white  g-bg-img-hero g-pt-100 g-pb-200" style="background-image: url({{asset('/img/recursosAgro/bannercontacto.png')}});">
  <div class="container u-bg-overlay__inner">
    <div class="u-heading-v8-2 g-mb-30">
      <h2 class="u-heading-v8__title text-uppercase g-font-weight-700 g-color-white">CONTÁCTANOS
        
      </h2>
    </div>

    <div class="lead g-font-weight-400 g-width-80x--md mx-auto g-mb-60">
    </div>

    <address class="row g-color-white-opacity-0_8">

     
      
      <div class="col-sm-6 col-md-3 g-mb-60">
        <i class="icon-line icon-map d-inline-block display-5 g-mb-25"></i>
        <h4 class="small text-uppercase g-mb-5">Dirección</h4>
        <strong>Complejo Ciudad Administrativa, Blvd Héroes de Chapultepec,
          Ciudad Argentum, 98160
          Zacatecas, Zac.</strong>
      </div>

      <div class="col-sm-6 col-md-3 g-mb-60">
        <i class="icon-call-in d-inline-block display-5 g-mb-25"></i>
        <h4 class="small text-uppercase g-mb-5">Teléfono</h4>
        <a class="g-color-white-opacity-0_8" href="tel:014789854210">
          <strong>01 (478) 985 42 10 Ext. 3239</strong></a>
      </div>

      <div class="col-sm-6 col-md-3">
        <i class="icon-envelope d-inline-block display-5 g-mb-25"></i>
        <h4 class="small text-uppercase g-mb-5">Correo Electrónico</h4>
        <a class="g-color-white-opacity-0_8" href="mailto:contacto@agroalimentariazac.mx">
          <strong>contacto@agroalimentariazac.mx</strong></a>
      </div>

      <div class="col-sm-6 col-md-3">
     
        <i class="fa fa-whatsapp  d-inline-block display-5 g-mb-25"></i>
        <h4 class="small text-uppercase g-mb-5">WhatsApp</h4>
        <a class="g-color-white-opacity-0_8" href="https://api.whatsapp.com/send?phone=5214921038159&text=%C2%A1Hola!%20Quiero%20informes%20de%20la%20Agroalimentaria%20Zacatecas%202019">
          <strong>Enviar un mensaje <i class="fa fa-paper-plane"></i></strong></a>
      </div>

     
    </address>
  </div>
</div>
<!-- End Footer Background -->

<!-- Footer Map and Form -->
<div class="container text-center g-color-white-opacity-0_8 g-mt-minus-200">
  <div class="row no-gutters g-min-height-60vh">
      <!-- Google Map -->
      <div class="col-lg-6 d-flex g-bg-gray-dark-v1 g-pa-400">
          <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3678.619662158558!2d-102.61308058464876!3d22.779491931084426!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x86824e063ac8d4c5%3A0xe3338e15a70b2688!2sPalacio+de+Convenciones+Zacatecas!5e0!3m2!1ses-419!2sus!4v1565028403061!5m2!1ses-419!2sus" width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
      </div>
      <!-- End Google Map -->

    <!-- Footer Content -->
    <div class="col-lg-6 d-flex g-bg-gray-dark-v1 g-pa-40">
      <div class="align-self-center w-100">
        <h2 class="g-color-white text-uppercase g-font-weight-700 g-mb-50"> Conoce la oportunidad de negocios que ofrece Zacatecas
        </h2>

        <form>
          <div class="form-group g-mb-10">
            <input class="form-control g-color-white g-bg-black-opacity-0_6 g-bg-black-opacity-0_4--focus g-brd-black-opacity-0_4 g-brd-primary--focus rounded-0 g-pa-15" type="tel" placeholder="Correo Electrónico">
          </div>
          <div class="form-group g-mb-10">
            <input class="form-control g-color-white g-bg-black-opacity-0_6 g-bg-black-opacity-0_4--focus g-brd-black-opacity-0_4 g-brd-primary--focus rounded-0 g-pa-15" type="text" placeholder="Asunto">
          </div>
          <div class="form-group g-mb-40">
            <textarea class="form-control g-resize-none g-color-white g-bg-black-opacity-0_6 g-bg-black-opacity-0_4--focus g-brd-black-opacity-0_4 g-brd-primary--focus rounded-0 g-pa-15" rows="5" placeholder="Mensaje"></textarea>
          </div>
          <button class="btn btn-md u-btn-primary g-font-size-12 text-uppercase rounded-0 g-py-11 g-px-30" type="submit" role="button">Enviar mensaje</button>
        </form>
      </div>
    </div>
    <!-- End Footer Content -->
  </div>
</div>
<!-- Footer Map and Form -->

<!-- Copyright -->
<footer class="text-center g-pt-60 g-pb-30">
  <div class="container">
    <a class="d-block g-mb-30" href="http://nymeria.mx/">
      <img class="img-fluid" src="{{asset('/img/nymeriam.png')}}" alt="Logo">
    </a>

    <small class="d-block g-font-size-default g-mb-20">Desarrollado por NYMERIA, 2019 Derechos Reservados 
      <a class="g-color-primary g-color-primary--hover" href="#">AGROALIMENTARIA ZACATECAS 2019</a>
    </small>

   
  </div>
</footer>
<!-- End Copyright -->

      <a class="js-go-to rounded-0 u-go-to-v1" href="#"
         data-type="fixed"
         data-position='{
           "bottom": 15,
           "right": 15
         }'
         data-offset-top="400"
         data-compensation="#js-header"
         data-show-effect="zoomIn">
        <i class="hs-icon hs-icon-arrow-top"></i>
      </a>
    </main>

    <!-- JS Global Compulsory -->
    <script src="{{asset('/vendor/jquery/jquery.min.js')}}"></script>
    <script src="{{asset('/vendor/jquery-migrate/jquery-migrate.min.js')}}"></script>
    <script src="{{asset('/vendor/popper.js/popper.min.js')}}"></script>
    <script src="{{asset('/vendor/bootstrap/bootstrap.min.js')}}"></script>

    <!-- JS Implementing Plugins -->
    <script src="{{asset('/vendor/appear.js')}}"></script>
    <script src="{{asset('/vendor/masonry/dist/masonry.pkgd.min.js')}}"></script>
    <script src="{{asset('/vendor/imagesloaded/imagesloaded.pkgd.min.js')}}"></script>
    <script src="{{asset('/vendor/jquery.countdown.min.js')}}"></script>
    <script src="{{asset('/vendor/fancybox/jquery.fancybox.js')}}"></script>
    <script src="{{asset('/vendor/slick-carousel/slick/slick.js')}}"></script>
    <script src="{{asset('/vendor/gmaps/gmaps.min.js')}}"></script>

    <!-- JS Unify -->
    <script src="{{asset('/js/hs.core.js')}}"></script>
    <script src="{{asset('/js/components/hs.header.js')}}"></script>
    <script src="{{asset('/js/helpers/hs.hamburgers.js')}}"></script>
    <script src="{{asset('/js/components/hs.scroll-nav.js')}}"></script>
    <script src="{{asset('/js/components/hs.tabs.js')}}"></script>
    <script src="{{asset('/js/components/hs.countdown.js')}}"></script>
    <script src="{{asset('/js/components/hs.carousel.js')}}"></script>
    <script src="{{asset('/js/components/gmap/hs.map.js')}}"></script>
    <script src="{{asset('/js/components/hs.go-to.js')}}"></script>

    <!-- JS Customization -->
    <script src="{{asset('/js/custom.js')}}"></script>

    <!-- JS Plugins Init. -->
    <script>
      // initialization of google map
      function initMap() {
        $.HSCore.components.HSGMap.init('.js-g-map');
      }

      $(document).on('ready', function () {
        // initialization of carousel
        $.HSCore.components.HSCarousel.init('.js-carousel');

        // initialization of header
        $.HSCore.components.HSHeader.init($('#js-header'));
        $.HSCore.helpers.HSHamburgers.init('.hamburger');

        // initialization of tabs
        $.HSCore.components.HSTabs.init('[role="tablist"]');

        // initialization of go to section
        $.HSCore.components.HSGoTo.init('.js-go-to');

        // initialization of countdowns
        var countdowns = $.HSCore.components.HSCountdown.init('.js-countdown', {
          yearsElSelector: '.js-cd-years',
          monthElSelector: '.js-cd-month',
          daysElSelector: '.js-cd-days',
          hoursElSelector: '.js-cd-hours',
          minutesElSelector: '.js-cd-minutes',
          secondsElSelector: '.js-cd-seconds'
        });

      
      });

      $(window).on('load', function() {
        // initialization of HSScrollNav
        $.HSCore.components.HSScrollNav.init($('#js-scroll-nav'), {
          duration: 700
        });

        // initialization of masonry.js
        $('.masonry-grid').imagesLoaded().then(function () {
          $('.masonry-grid').masonry({
            columnWidth: '.masonry-grid-sizer',
            itemSelector: '.masonry-grid-item',
            percentPosition: true
          });
        });
      });

      $(window).on('resize', function () {
        setTimeout(function () {
          $.HSCore.components.HSTabs.init('[role="tablist"]');
        }, 200);
      });
    </script>

     
      <!-- JS Plugins Init. -->
      <script >
        // initialization of google map
        function initMap() {
          $.HSCore.components.HSGMap.init('.js-g-map');
        }
      </script>

           <!-- JS Implementing Plugins -->
           <script  src="{{asset('/vendor/dzsparallaxer/dzsparallaxer.js')}}"></script>
           <script  src="{{asset('/vendor/dzsparallaxer/dzsscroller/scroller.js')}}"></script>
           <script  src="{{asset('/vendor/dzsparallaxer/advancedscroller/plugin.js')}}"></script>

        

         
  </body>
</html>
