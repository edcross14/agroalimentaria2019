<?php

namespace App\Http\Controllers;

use App\Participante;
use Illuminate\Http\Request;

class ParticipanteController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $participantes = Participante::latest()->paginate(5);
  
        return view('participantes.index',compact('participantes'))
            ->with('i', (request()->input('page', 1) - 1) * 5);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('participantes.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'nombre' => 'required', 
            'apellidoPaterno' => 'required', 
            'apellidoMaterno' => 'required',
            'email' => 'required',
            'genero' => 'required',
            'edad' => 'required',
            'celular' => 'required',
            'ocupacion' => 'required',
            'pais' => 'required', 
            'estado' => 'required', 
            'ciudad' => 'required',
            'codigoPostal' => 'required',
            'direccion' => 'required',
            'nombreEmpresa' => 'required',
        ]);
  
        Participante::create($request->all());
   
        return redirect()->route('participantes.index')
                        ->with('success','Participante creado correctamente.');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Participante  $participante
     * @return \Illuminate\Http\Response
     */
    public function show(Participante $participante)
    {
        return view('participantes.show',compact('participante'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Participante  $participante
     * @return \Illuminate\Http\Response
     */
    public function edit(Participante $participante)
    {
        return view('participantes.edit',compact('participante'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Participante  $participante
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Participante $participante)
    {
        $request->validate([
            'nombre' => 'required', 
            'apellidoPaterno' => 'required', 
            'apellidoMaterno' => 'required',
            'email' => 'required',
            'genero' => 'required',
            'edad' => 'required',
            'celular' => 'required',
            'ocupacion' => 'required',
            'pais' => 'required', 
            'estado' => 'required', 
            'ciudad' => 'required',
            'codigoPostal' => 'required',
            'direccion' => 'required',
            'nombreEmpresa' => 'required',
        ]);
  
        $participante->update($request->all());
  
        return redirect()->route('participantes.index')
                        ->with('success','Participante actualizado correctamente');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Participante  $participante
     * @return \Illuminate\Http\Response
     */
    public function destroy(Participante $participante)
    {
        $participante->delete();
  
        return redirect()->route('participantes.index')
                        ->with('success','Participant3 eliminado correctamente');
    }

    public function pdf(){
      
       
    }
}
