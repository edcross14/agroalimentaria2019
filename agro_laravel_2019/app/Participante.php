<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Participante extends Model
{
    protected $fillable = ['nombre', 'apellidoPaterno', 'apellidoMaterno',
    'email','genero','edad','celular','ocupacion', 'pais', 'estado', 'ciudad',
     'codigoPostal', 'direccion', 'nombreEmpresa'];
}
